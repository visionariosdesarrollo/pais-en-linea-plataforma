var app = {
	modalConfirmUserPassword : {
		id : '#modalPasswordUserConfirm',
		buttonSuccesslId : '#modalConfirmUserPassword-success',
		validate : {
			required : {
				messageId : '#requiredPasswordUserConfirm'
			},
			length : {
				minimumPasswordLength : 5,
				messageId : '#minLengthPasswordUserConfirm'
			}
		},
		show : function() {
			var _modal = $(app.modalConfirmUserPassword.id);
			var _passwordInput = $(app.modalConfirmUserPassword.id + ' input')
					.first();
			var _form = $(_passwordInput).parents('form:first');
			var _validate = app.modalConfirmUserPassword.validate;

			_form.submit(function(event) {
				_modal.removeClass('has-error');
				$(_validate.required.messageId).css('display', 'none');
				$(_validate.length.messageId).css('display', 'none');

				_passwordInput.focus();

				if (_modal.hasClass('in')) {
					if (!_passwordInput.val()) {
						_modal.addClass('has-error');
						$(_validate.required.messageId).css('display', 'block');
						return false;
					} else
						_modal.removeClass('has-error');

					if (_passwordInput.val().length < _validate.length.minimumPasswordLength) {
						_modal.addClass('has-error');
						$(_validate.length.messageId).css('display', 'block');
						return false;
					} else
						_modal.removeClass('has-error');	

;
					return true;
				} else {
					_passwordInput.val('');
					_modal.modal("show");
				}

				event.preventDefault();

				return false;
			});

			$(app.modalConfirmUserPassword.buttonSuccesslId).click(
					function(event) {
						_form.submit();
						event.preventDefault();
					})
		},
	},
	startTooltip : function() {
		$('[data-toggle=\"tooltip\"]').tooltip();
	},
	startPopover : function() {
		$('[data-toggle=\"popover\"]').tooltip();
	},
	startDatepicker : function() {
		$.fn.datepicker.defaults.autoclose = true;
		$.fn.datepicker.defaults.format = 'dd/mm/yyyy';
		$.fn.datepicker.defaults.language = 'es';
		var $elements = $('[data-role=\"date\"]');
		$elements.datepicker();
	},
	startTimepicker : function() {
		var $elements = $('[data-role=\"time\"]');
		$elements.timepicker();
	},
	startConfigEvent : function() {
		var buttonEventConfirmAnimate = {
			addClass : function() {
				$("#confirmEvent").addClass("btn-success", 1000,
						this.removeClass);
			},
			removeClass : function() {
				$("#confirmEvent").removeClass("btn-success", 1000);
				setTimeout(function() {
					buttonEventConfirmAnimate.addClass();
				}, 1000);
			},
			init : function() {
				this.addClass();
			}
		};
		buttonEventConfirmAnimate.init();

		$('#bodyEvent, #nameEvent').spellAsYouType({
			defaultDictionary : 'Espanol',
			userInterfaceTranslation : 'es',
			popUpStyle : 'fancybox',
			theme : 'bright',
			serverModel : 'php'
		});

		app.modalConfirmUserPassword.show();
	},
	init : function() {
		this.startTooltip();
		this.startPopover();
		this.startDatepicker();
		this.startTimepicker();
		this.startConfigEvent();
	}
};

jQuery(function($) {
	$(document).ajaxStop(function() {
		app.init();
	});
	app.init();
});
