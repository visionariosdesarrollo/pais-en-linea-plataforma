/*
SQLyog Ultimate v9.63 
MySQL - 5.5.45-cll-lve : Database - nethaco1_quickly
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`nethaco1_quickly` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `nethaco1_quickly`;

/*Table structure for table `tbl_mod_entity` */

DROP TABLE IF EXISTS `tbl_mod_entity`;

CREATE TABLE `tbl_mod_entity` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `cityId` tinyint(3) unsigned NOT NULL COMMENT 'Ciudad',
  `name` varchar(50) NOT NULL COMMENT 'Nombre de entidad',
  `description` text COMMENT 'Descripción',
  `email` varchar(50) NOT NULL COMMENT 'Correo electrónico',
  `phoneOne` varchar(20) NOT NULL COMMENT 'Teléfono',
  `phoneTwo` varchar(20) DEFAULT NULL COMMENT 'Teléfono',
  `image` varchar(250) DEFAULT NULL COMMENT 'Imagen',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cityId_-_name` (`cityId`,`name`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_mod_event` */

DROP TABLE IF EXISTS `tbl_mod_event`;

CREATE TABLE `tbl_mod_event` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `entityId` smallint(5) unsigned DEFAULT NULL COMMENT 'Entidad',
  `name` varchar(250) NOT NULL COMMENT 'Nombre del evento',
  `type` varchar(125) NOT NULL COMMENT 'Tipo de evento',
  `eventDateStart` date NOT NULL COMMENT 'Fecha inicio del evento',
  `eventTimeStart` time NOT NULL COMMENT 'Hora inicio del evento',
  `eventDateEnd` date NOT NULL COMMENT 'Fecha finalización del evento',
  `eventTimeEnd` time NOT NULL COMMENT 'Hora finalización del evento',
  `site` varchar(250) NOT NULL COMMENT 'Lugar y dirección del evento',
  `body` text NOT NULL COMMENT 'Descripción del evento',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `entityId` (`entityId`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_mod_event_quickly` */

DROP TABLE IF EXISTS `tbl_mod_event_quickly`;

CREATE TABLE `tbl_mod_event_quickly` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `userId` smallint(5) unsigned NOT NULL COMMENT 'Usuario',
  `eventId` smallint(5) unsigned NOT NULL COMMENT 'Evento',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userId_-_entityId` (`eventId`,`userId`),
  KEY `userId` (`userId`),
  KEY `eventId` (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_mod_event_type` */

DROP TABLE IF EXISTS `tbl_mod_event_type`;

CREATE TABLE `tbl_mod_event_type` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `name` varchar(100) NOT NULL COMMENT 'Nombre de tipo',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_mod_multimedia` */

DROP TABLE IF EXISTS `tbl_mod_multimedia`;

CREATE TABLE `tbl_mod_multimedia` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `table` varchar(50) NOT NULL COMMENT 'Nombre de tabla',
  `tableId` smallint(5) unsigned NOT NULL COMMENT 'Identificador de tabla',
  `type` enum('video','imagen','sonido','documento','enlace') NOT NULL COMMENT 'Tipo',
  `mimeType` varchar(20) DEFAULT NULL COMMENT 'Tipo de medio',
  `title` varchar(100) NOT NULL COMMENT 'Título',
  `path` varchar(250) NOT NULL COMMENT 'Ubicación',
  `description` text COMMENT 'Descripción',
  PRIMARY KEY (`id`),
  KEY `table` (`table`),
  KEY `tableId` (`tableId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_mod_release` */

DROP TABLE IF EXISTS `tbl_mod_release`;

CREATE TABLE `tbl_mod_release` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `userId` smallint(5) unsigned NOT NULL COMMENT 'Usuario',
  `name` varchar(250) NOT NULL COMMENT 'Nombre del comunicado',
  `releaseDateStart` date NOT NULL COMMENT 'Fecha inicio del comunicado',
  `releaseDateEnd` date NOT NULL COMMENT 'Fecha finalización del comunicado',
  `body` text NOT NULL COMMENT 'Descripción del comunicado',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_mod_user` */

DROP TABLE IF EXISTS `tbl_mod_user`;

CREATE TABLE `tbl_mod_user` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `entityId` smallint(5) unsigned DEFAULT NULL COMMENT 'Entidad',
  `role` enum('root','presidente','gobernador','alcalde','secretario de despacho','secretaria','redactor','usuario') NOT NULL COMMENT 'Perfil',
  `roleCountryId` tinyint(3) unsigned DEFAULT NULL COMMENT 'País',
  `roleDepartmentId` tinyint(3) unsigned DEFAULT NULL COMMENT 'Departamento',
  `roleCityId` tinyint(3) unsigned DEFAULT NULL COMMENT 'Ciudad',
  `name` varchar(50) NOT NULL COMMENT 'Nombre de usuario',
  `documentType` enum('cc','nit','pasaporte') DEFAULT NULL COMMENT 'Tipo de documento',
  `document` varchar(50) DEFAULT NULL COMMENT 'Documento de identidad',
  `dependence` varchar(250) DEFAULT NULL COMMENT 'Dependencia donde trabaja',
  `email` varchar(50) DEFAULT NULL COMMENT 'Correo electrónico',
  `username` varchar(20) DEFAULT NULL COMMENT 'Usuario',
  `password` varchar(80) DEFAULT NULL COMMENT 'Clave',
  `dataLastAccess` datetime DEFAULT NULL COMMENT 'Fecha de último acceso',
  `position` varchar(250) DEFAULT NULL COMMENT 'Cargo',
  `sex` enum('man','female') DEFAULT NULL COMMENT 'Sexo',
  `territory` enum('rural','urban') DEFAULT NULL COMMENT 'Territorio',
  `documentDeliveryDate` date DEFAULT NULL COMMENT 'Fecha de expedición',
  `image` varchar(250) DEFAULT NULL COMMENT 'Imagen',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `entityId` (`entityId`,`id`),
  UNIQUE KEY `document_-_email` (`document`,`role`),
  KEY `name` (`name`),
  KEY `role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_redundant_data` */

DROP TABLE IF EXISTS `tbl_redundant_data`;

CREATE TABLE `tbl_redundant_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `table` enum('tbl_sys_config_country','tbl_sys_config_country_department','tbl_sys_config_country_department_city','tbl_mod_event','tbl_mod_multimedia','tbl_mod_user','tbl_mod_entity','tbl_mod_event_type','tbl_mod_release','tbl_event_quickly','tbl_mod_event_quickly') NOT NULL COMMENT 'Nombre de tabla',
  `tableId` smallint(5) unsigned NOT NULL COMMENT 'Identificador de tabla',
  `status` enum('inactivo','pendiente','activo','cancelado','eliminado','cumplido','agendada','desagendada') NOT NULL DEFAULT 'inactivo' COMMENT 'Estado',
  `order` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Orden',
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de creación',
  `modifiedDate` timestamp NULL DEFAULT NULL COMMENT 'Fecha de modificación',
  `creationUserId` smallint(5) unsigned DEFAULT NULL COMMENT 'Usuario de creación',
  `modifiedUserId` smallint(5) unsigned DEFAULT NULL COMMENT 'Usuario de modificación',
  PRIMARY KEY (`id`),
  UNIQUE KEY `table_-_tableId` (`tableId`,`table`),
  KEY `status` (`status`),
  KEY `creationUserId` (`creationUserId`),
  KEY `modifiedUserId` (`modifiedUserId`),
  CONSTRAINT `tbl_redundant_data_ibfk_1` FOREIGN KEY (`creationUserId`) REFERENCES `tbl_mod_user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `tbl_redundant_data_ibfk_2` FOREIGN KEY (`modifiedUserId`) REFERENCES `tbl_mod_user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_sys_audit` */

DROP TABLE IF EXISTS `tbl_sys_audit`;

CREATE TABLE `tbl_sys_audit` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `username` varchar(20) NOT NULL COMMENT 'Usuario',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha',
  `accessType` enum('escritorio','tableta','celular','otro') NOT NULL DEFAULT 'escritorio' COMMENT 'Tipo de acceso',
  `ip` varchar(15) NOT NULL COMMENT 'Dirección IP de acceso',
  `table` varchar(50) NOT NULL COMMENT 'Nombre de tabla',
  `tableId` smallint(5) unsigned DEFAULT NULL COMMENT 'Identificador de tabla',
  `type` enum('insercion','actualizacion','eliminacion','consulta','otro') NOT NULL DEFAULT 'otro' COMMENT 'Tipo',
  `data` blob COMMENT 'Datos',
  PRIMARY KEY (`id`),
  KEY `id_user` (`table`,`tableId`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_sys_config_country` */

DROP TABLE IF EXISTS `tbl_sys_config_country`;

CREATE TABLE `tbl_sys_config_country` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `name` varchar(100) NOT NULL COMMENT 'Nombre de país',
  `code` char(2) NOT NULL COMMENT 'Código de 2 letras',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_sys_config_country_department` */

DROP TABLE IF EXISTS `tbl_sys_config_country_department`;

CREATE TABLE `tbl_sys_config_country_department` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `countryId` tinyint(3) unsigned NOT NULL COMMENT 'País',
  `name` varchar(100) NOT NULL COMMENT 'Nombre de departamento',
  PRIMARY KEY (`id`),
  UNIQUE KEY `countryId_-_name` (`countryId`,`name`),
  CONSTRAINT `tbl_sys_config_country_department_ibfk_1` FOREIGN KEY (`countryId`) REFERENCES `tbl_sys_config_country` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_sys_config_country_department_city` */

DROP TABLE IF EXISTS `tbl_sys_config_country_department_city`;

CREATE TABLE `tbl_sys_config_country_department_city` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `departmentId` tinyint(3) unsigned NOT NULL COMMENT 'Departamento',
  `name` varchar(100) NOT NULL COMMENT 'Nombre de municipio',
  `image` varchar(250) DEFAULT NULL COMMENT 'Imagen',
  PRIMARY KEY (`id`),
  UNIQUE KEY `departmentId_-_name` (`departmentId`,`name`),
  CONSTRAINT `tbl_sys_config_country_department_city_ibfk_1` FOREIGN KEY (`departmentId`) REFERENCES `tbl_sys_config_country_department` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
