<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Configuración')	.	' / '	. Yii::t('configuration','Departamentos');  ?>
<?php $this->beginContent('/configuration/index'); ?>
<div class="row">
	<div class="col-lg-3">
		<?php $this->renderPartial('../department/_formConfiguration', array('model'=>$model)); ?>
	</div>
  <div class="col-lg-9">
  	<?php $this->departmentAdmin(); ?>
	</div>
</div>
<?php $this->endContent(); ?>