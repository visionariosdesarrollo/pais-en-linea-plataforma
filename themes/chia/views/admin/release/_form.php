<div class="panel panel-orange">
	<div class="panel-heading">
		<i class="fa fa-edit fa-fw"></i> <?php echo Yii::t('configuration','Formulario'); ?>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<?php
$form = $this->beginWidget ( 'bootstrap.widgets.BsActiveForm', array (
				'id' => 'release-form',
		'htmlOptions' => array(
				'enctype' => 'multipart/form-data',
		),
				// Please note: When you enable ajax validation, make sure the corresponding
				// controller action is handling ajax validation correctly.
				// There is a call to performAjaxValidation() commented in generated controller code.
				// See class documentation of CActiveForm for details on this.
				'enableAjaxValidation' => false 
		) );
		?>
			<?php $this->renderPartial('../modal/modalPasswordUserConfirm', array('modalPasswordUserConfirm'=>$modalPasswordUserConfirm)); ?>		
			<?php echo Yii::t('configuration','<p class="text-info">Campos marcados con <span class="text-danger">*</span> son requeridos.</p>'); ?>
			<?php echo $form->errorSummary($model); ?>
			<div class="row">
			<div class="col-lg-6">
					<?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>50)); ?>
				</div>
			<div class="col-lg-3">
					<?php echo $form->textFieldControlGroup($model,'releaseDateStart',array('data-role'=>'date','prepend' =>BsHtml::icon(BsHtml::GLYPHICON_CALENDAR),'append' =>BsHtml::icon(BsHtml::GLYPHICON_INFO_SIGN,array('data-toggle'=>'tooltip','data-container'=>'body','data-original-title'=>Yii::t('configuration', 'El comunicado debe ser creado con {{limitCreation}} horas de anticipación a la fecha de la misma.', array('{{limitCreation}}' => ReleaseAbstract::LIMIT_CREATION)))))); ?>
				</div>
								<div class="col-lg-3">
								<?php echo $form->textFieldControlGroup($model,'releaseDateEnd',array('data-role'=>'date','prepend' =>BsHtml::icon(BsHtml::GLYPHICON_CALENDAR),'append' =>BsHtml::icon(BsHtml::GLYPHICON_INFO_SIGN,array('data-toggle'=>'tooltip','data-container'=>'body','data-original-title'=>Yii::t('configuration', 'La fecha de finalización del comunicado debe ser mayor que la fecha de inicio.'))))); ?>
				</div>	
		</div>
		<div class="row">
			<div class="col-lg-12">
			  	<?php echo $form->textAreaControlGroup($model,'body',array('rows'=>6)); ?>
			  </div>
		</div>
		<?php echo BsHtml::submitButton(Yii::t('configuration','Confirmar'), array('color'=>BsHtml::BUTTON_COLOR_SUCCESS)); ?>
			<?php echo !$model->isNewRecord ? BsHtml::linkButton(Yii::t('configuration','Cancelar'),array('url'=>$this->createUrl('create'),'color'=>BsHtml::BUTTON_COLOR_DANGER)) : ''; ?>
		<?php $this->endWidget(); ?>
	</div>
	<!-- /.panel-body -->
</div>
<!-- /.panel -->