<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Entidades')	.	' / '	. Yii::t('configuration','Listado');  ?>
<?php $this->beginContent('index'); ?>
<div class="panel panel-orange">
	<div class="panel-heading">
		<i class="fa fa-th-list fa-fw"></i> <?php echo Yii::t('configuration','Listado'); ?></div>
	<div class="panel-body">
		<?php
		$this->widget ( 'bootstrap.widgets.BsGridView', array (
				'id' => 'entity-grid',
				'htmlOptions' => array (
						'class' => 'table-responsive',
				),
				'dataProvider' => $model->search (),
				'pager' => array (
						'class' => 'bootstrap.widgets.BsPager',
						'size' => BsHtml::PAGINATION_SIZE_DEFAULT,
						'nextPageLabel' => Yii::t ( 'configuration', 'Siguiente' ),
						'prevPageLabel' => Yii::t ( 'configuration', 'Anterior' ),
						'firstPageLabel' => Yii::t ( 'configuration', 'Primero' ),
						'lastPageLabel' => Yii::t ( 'configuration', 'Último' ) 
				),
				'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
				'ajaxUrl' => $this->createUrl ( 'admin' ),
				'filter' => $model,
				'type' => BsHtml::GRID_TYPE_CONDENSED . ' ' . BsHtml::GRID_TYPE_STRIPED,
				'columns' => array (
						array (
								'name' => 'name',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus' 
								),
								'htmlOptions' => array (
										'id' => 'release-grid-name'
								)
						),
						array (
								'name' => 'status',
								'value' => 'RedundantDataAbstract::getStatusLabel($data->status)',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus'
								),
								'filter' => CHtml::activeDropDownList ( $model, 'status', Release::model()->getStatus ( ), array (
										'empty' => Yii::t ( 'configuration', 'Seleccione' ),
										'class' => 'form-control'
								) ),
								'type' => 'raw'
						),				
						array (
								'name' => 'releaseDateStart',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus' 
								),
								'value' => array (
										$this,
										'formatLocaleTimestampToDatetime' 
								),
								'filter' => CHtml::activeTextField ( $model, 'releaseDateStart', array (
										'class' => 'form-control',
										'data-role' => 'date' 
								) ),
								'type' => 'raw' 
						),
						array (
								'name' => 'creationDate',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus' 
								),
								'value' => array (
										$this,
										'formatLocaleTimestampToDatetime' 
								),
								'filter' => CHtml::activeTextField ( $model, 'creationDate', array (
										'class' => 'form-control',
										'data-role' => 'date' 
								) ),
								'type' => 'raw' 
						),
						array (
								'class' => 'nh.zii.widgets.grid.NHBsButtonColumn',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus' 
								),
								'deleteConfirmation' => true,
								'deleteColumnIdLabelConfirmation' => 'release-grid-name',
								'template' => '{view} {update} {delete}',
								'buttons' => array (
										'view' => array (
												'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->primaryKey))',
												'options' => array (
														'class' => 'btn btn-info btn-circle'
												),
												'visible' => '"' . UserAbstract::checkAccess ( $this->id, 'view' ) . '"'
										),										
										'update' => array (
												'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey))',
												'options' => array (
														'class' => 'btn btn-warning btn-circle' 
												),
												'visible' => '"' . UserAbstract::checkAccess ( $this->id, 'update' ) . '"' 
										),
										'delete' => array (
												'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
												'options' => array (
														'class' => 'btn btn-danger btn-circle' 
												),
												'visible' => '"' . UserAbstract::checkAccess ( $this->id, 'delete' ) . '"' 
										) 
								) 
						) 
				) 
		) );
		?>
		</div>
</div>
<?php $this->endContent(); ?>