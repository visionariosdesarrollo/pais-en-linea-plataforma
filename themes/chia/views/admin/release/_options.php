<?php
$options = array ();

if (UserAbstract::checkAccess ( $this->id, 'admin' ))
	$options [$this->createUrl ( 'admin' )] = Yii::t ( 'configuration', 'Listado' );

if (UserAbstract::checkAccess ( $this->id, 'create' ))
	$options [$this->createUrl ( 'create' )] = Yii::t ( 'configuration', 'Crear' );

$requestUri = Yii::app ()->request->requestUri;
?>

<!-- Nav tabs -->
<div class="row">
	<div class="col-lg-12">
		<ul class="nav nav-pills">
			<?php foreach($options as $url=>$label): ?>
				<li <?php echo $requestUri===$url?' class="active"':''; ?>><a
				href="<?php echo $url; ?>"><?php echo $label; ?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-12">
		<?php if($this->messageSuccess): ?>
			<div class="alert alert-success alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close"
				type="button">x</button>
		    <?php echo $this->messageSuccess; ?>
			</div>
		<?php endif; ?>
		<?php if($this->messageError): ?>
			<div class="alert alert-danger alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close"
				type="button">x</button>
		    <?php echo $this->messageSuccess; ?>
			</div>				
		<?php endif; ?>
		<br />
	</div>
	<!-- /.col-lg-12 -->
</div>
