<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Comunicados')	.	' / '	. Yii::t('configuration','Vista');  ?>
<?php $this->beginContent('index'); ?>
<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-orange">
					<div class="panel-heading">
						<?php echo BsHtml::icon(BsHtml::GLYPHICON_EDIT); ?>
						<?php echo $model->name; ?>
			</div>
					<!-- /.panel-heading -->
					<div class="panel-body table-responsive">

<?php
$this->widget ( 'zii.widgets.CDetailView', array (
		'htmlOptions' => array (
				'class' => 'table table-striped table-condensed table-hover' 
		),
		'data' => $model,
		'attributes' => array (
				array (
						'name' => 'redundantDataOfRelease.status',
						'value' => RedundantDataAbstract::getStatusLabel ( $model->redundantDataOfRelease->status ),
						'cssClass' => $this->cssClassStatus ( null, $model->redundantDataOfRelease ) 
				),
				array (
						'name' => 'redundantDataOfRelease.creationDate',
						'value' => $this->formatLocaleTimestampToDatetimeGeneric ( $model->redundantDataOfRelease->creationDate ),
						'cssClass' => 'bg-info' 
				),
				array (
						'name' => 'redundantDataOfRelease.modifiedDate',
						'value' => $this->formatLocaleTimestampToDatetimeGeneric ( $model->redundantDataOfRelease->modifiedDate ),
						'cssClass' => 'bg-info' 
				),
				'name',			
				'releaseDateStart',
				'releaseDateEnd',
				'body'
		) 
) );
?>
	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->endContent(); ?>