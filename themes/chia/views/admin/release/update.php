<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Comunicados')	.	' / '	. Yii::t('configuration','Actualizar');  ?>
<?php $this->beginContent('index'); ?>
<div class="row">
	<div class="col-lg-12">
		<?php $this->renderPartial('_form', array('model'=>$model,'modalPasswordUserConfirm'=>$modalPasswordUserConfirm)); ?>
	</div>
</div>
<?php $this->endContent(); ?>