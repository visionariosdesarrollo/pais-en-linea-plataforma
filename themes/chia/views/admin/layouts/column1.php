<?php $this->beginContent('/layouts/main'); ?>
	<!-- Page Content -->
	<div id="wrapper">
  		<div class="container-fluid">
  			<?php if(!Yii::app ()->user->isGuest): ?>
	    		<div class="row">
	      			<div class="col-lg-12">
	        			<?php $this->renderPartial('/layouts/menu'); ?>
					</div>
					<!-- /.col-lg-12 -->
				</div>		
  			<?php endif; ?>				
    		<div class="row">
      			<div class="col-lg-12">
        			<?php echo $content; ?>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /#wrapper -->
<?php $this->endContent(); ?>