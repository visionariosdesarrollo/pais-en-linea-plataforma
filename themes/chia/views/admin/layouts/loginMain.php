<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language; ?>">
	<head>
		<?php $this->renderPartial('../layouts/_header'); ?>
	</head>
	<body id="login">
		<div id="main">
			<?php echo $content; ?>
    </div>
		<?php $this->renderPartial('../layouts/_footerScripts'); ?>
	</body>
</html>