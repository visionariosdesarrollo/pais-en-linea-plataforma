<meta charset="<?php echo Yii::app()->charset; ?>">
<meta name="language" content="<?php echo Yii::app()->language; ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">    	
<meta name="application-name" content="<?php echo Yii::app()->name; ?>">
<meta name="author" content="<?php echo Yii::app()->params['author']; ?>">
<meta name="description" content="<?php echo Yii::app()->params['description']; ?>">
    	
<!-- Define a title for your HTML document. -->
<title><?php echo CHtml::encode($this->pageTitle .	' - '	.	Yii::app()->name); ?></title>
    	
<!-- Specify a default URL and a default target for all links on a page. -->
<base href="<?php echo Yii::app()->request->baseUrl; ?>" />

<!-- jQuery UI Core CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css" rel="stylesheet">    	
    	
<!-- Bootstrap Core CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">   	

<!-- Bootstrap Datepicker Core CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">

<!-- Bootstrap Timepicker Core CSS -->
<link rel="stylesheet/less" href="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/bootstrap-timepicker/less/timepicker.less" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">
    
<!-- DataTables Responsive CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">	    
    
<!-- Custom CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    	
<!-- Admin App CSS -->
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/styles/main.css" rel="stylesheet" type="text/css">

<!-- Fancy Box CSS -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->			
<!--[if lt IE 9]>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/html5shiv/dist/html5shiv.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/respond/dest/respond.min.js"></script>
<![endif]-->