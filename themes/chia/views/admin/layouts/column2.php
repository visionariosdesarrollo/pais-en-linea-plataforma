<?php $this->beginContent('/layouts/main'); ?>
		<div id="wrapper">
				<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only"><?php echo Yii::t('configuration', 'Navegación'); ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $this->createUrl('/admin'); ?>"><img alt="<?php echo Yii::app()->params['applicationName']; ?>" src="<?php echo Yii::app()->theme->baseUrl; ?>/styles/images/<?php echo Yii::app()->params['applicationLogoOrangeSmall']; ?>" /></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                	<a class="btn btn-link red" href="<?php echo $this->createUrl('/admin/default/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> <?php echo Yii::t('configuration','Cerrar sesión'); ?></a>
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Buscar...">
                                <span class="input-group-btn">
                                    <button class="btn btn-orange" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="<?php echo $this->createUrl('/admin'); ?>"><span class="fa fa-dashboard fa-fw"></span> <?php echo Yii::t('configuration','Escritorio'); ?></a>
                        </li>
                        <li>
                            <a href="#"><span class="fa fa-home fa-fw"></span> <?php echo Yii::t('configuration','Entidades'); ?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                            		<li>
                                    <a href="<?php echo $this->createUrl('/admin/entity/admin'); ?>"><?php echo Yii::t('configuration','Listado'); ?></a>
                                </li>                            
                            		<li>
                                    <a href="<?php echo $this->createUrl('/admin/entity/create'); ?>"><?php echo Yii::t('configuration','Crear'); ?></a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>                        
                        <li>
                            <a href="#"><span class="fa fa-users fa-fw"></span> <?php echo Yii::t('configuration','Servidores públicos'); ?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                            		<li>
                                    <a href="<?php echo $this->createUrl('/admin/user/admin'); ?>"><?php echo Yii::t('configuration','Listado'); ?></a>
                                </li>                            
                            		<li>
                                    <a href="<?php echo $this->createUrl('/admin/user/create'); ?>"><?php echo Yii::t('configuration','Crear'); ?></a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>                        
                        <li>
                            <a href="#"><span class="fa fa-calendar-o fa-fw"></span> <?php echo Yii::t('configuration','Actividades'); ?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                            
                                <li>
                                    <a href="<?php echo $this->createUrl('/admin/event/admin'); ?>"><?php echo Yii::t('configuration','Listado'); ?></a>
                                </li>	
                            		<li>
                                    <a href="<?php echo $this->createUrl('/admin/event/create'); ?>"><?php echo Yii::t('configuration','Crear'); ?></a>
                                </li>                     
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                        	<a href="#"><span class="fa fa-cog fa-fw"></span> <?php echo Yii::t('configuration','Configuración'); ?><span class="fa arrow"></a>
                            <ul class="nav nav-second-level">
                            	<li>
                                    <a href="<?php echo $this->createUrl('/admin/configuration/createCountry'); ?>"><i class="fa fa-globe fa-fw"></i> <?php echo Yii::t('configuration','Países'); ?> <span class="glyphicon glyphicon-info-sign" data-original-title="<?php echo Yii::t('configuration','Denominación del país o territorio según la norma ISO 3166-1.'); ?>" data-container="body" data-toggle="tooltip"></span></a> 
                                </li>
                            	<li>
                                    <a href="<?php echo $this->createUrl('/admin/configuration/createDepartment'); ?>"><i class="fa fa-globe fa-fw"></i> <?php echo Yii::t('configuration','Departamentos'); ?></a>
                                </li>
							     <li>
                                    <a href="<?php echo $this->createUrl('/admin/configuration/createCity'); ?>"><i class="fa fa-globe fa-fw"></i> <?php echo Yii::t('configuration','Ciudades'); ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo $this->createUrl('/admin/configuration/createEventType'); ?>"><i class="fa fa-globe fa-ellipsis-v"></i> <?php echo Yii::t('configuration','Tipos de actividades'); ?> <span class="glyphicon glyphicon-info-sign" data-original-title="<?php echo Yii::t('configuration','Tipos/Categorías de actividades por ejemplo: Teatro, Musica, Cultura, etc.'); ?>" data-container="body" data-toggle="tooltip"></span></a>
                                </li>                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $content; ?>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
		</div>
    <!-- /#wrapper -->
<?php $this->endContent(); ?>