<!-- JQuery JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/jquery/dist/jquery.min.js"></script>

<!-- JQuery Migrate JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/jquery-migrate/jquery-migrate-1.2.0.min.js"></script>

<!-- JQuery UI JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/jquery-ui/jquery-ui.min.js"></script>

<!-- jQuery Mouse Wheel Plugin JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/jquery-mousewheel/jquery.mousewheel.min.js"></script>

<!-- JQuery BBQ JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/jquery-bbq/jquery.ba-bbq.min.js"></script>

<!-- Moment JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/moment/min/moment.min.js"></script>

<!-- Bootstrap Core JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- LESS Core JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/less.js/dist/less.min.js"></script>

<!-- Bootstrap Datepicker Core JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js"></script>

<!-- Bootstrap Timepicker Core JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<!-- Metis Menu Plugin JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- DataTables JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<!-- Custom Theme JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/startbootstrap-sb-admin-2/dist/js/sb-admin-2.js"></script>

<!-- JQuery SpellChecker -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/JavaScriptSpellCheck/include.js"></script>

<!-- Fancy Box JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/bower_components/fancybox/source/jquery.fancybox.js"></script>

<!-- Admin App JS -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/javascripts/modules/admin/AdminModule.js"></script>
