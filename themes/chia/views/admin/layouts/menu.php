<?php
$user = Yii::app ()->user->getState ( 'user' );
$_access = UserAbstract::getAccess ();

$territory = null;

switch ($user->role) {
	case UserAbstract::ADMINISTRADOR_SUPER_USUARIO :
		$territory = sprintf ( '%s', BsHtml::tag ( 'b', array (
				'class' => 'text-orange' 
		), $user->roleCountryOfUser->name ) );
		break;
	case UserAbstract::ADMINISTRADOR_PRESIDENTE :
		$territory = sprintf ( '%s', BsHtml::tag ( 'b', array (
				'class' => 'text-orange' 
		), $user->roleCountryOfUser->name ) );
		break;
	case UserAbstract::ADMINISTRADOR_GOBERNADOR :
		$territory = sprintf ( '%s / %s', $user->roleCountryOfUser->name, BsHtml::tag ( 'b', array (
				'class' => 'text-orange' 
		), $user->roleDepartmentOfUser->name ) );
		break;
	case UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO :
		$territory = sprintf ( '%s / %s / %s', $user->roleCountryOfUser->name, $user->roleDepartmentOfUser->name, BsHtml::tag ( 'b', array (
				'class' => 'text-orange' 
		), $user->roleCityOfUser->name ) );
		break;
	case UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES :
		$territory = sprintf ( '%s / %s / %s', $user->roleCountryOfUser->name, $user->roleDepartmentOfUser->name, BsHtml::tag ( 'b', array (
			'class' => 'text-orange'
		), $user->roleCityOfUser->name ) );
		break;		
	default :
		$territory = sprintf ( '%s / %s / %s', $user->roleCountryOfUser->name, $user->roleDepartmentOfUser->name, BsHtml::tag ( 'b', array (
				'class' => 'text-orange' 
		), $user->entityOfUser->cityOfEntity->name ) );
		break;
}

$imageTemplate = "";
if (isset ( $user->entityOfUser->image ) && ! empty ( $user->entityOfUser->image )) {
	$imageTemplate = BsHtml::tag ( 'span', array (
			'id' => 'entity-image-menu' 
	), BsHtml::imageCircle ( EntityAbstract::getImagePath () . $user->entityOfUser->image, $user->entityOfUser->name ) ); // $imageTemplate = BsHtml::tag('span', array('id'=>'entity-image-menu'), BsHtml::imageCircle( EntityAbstract::getImagePath() . 'default.png'));
}

$this->widget ( 'bootstrap.widgets.BsNavbar', array (
		'collapse' => true,
		'brandLabel' => BsHtml::image ( Yii::app ()->theme->baseUrl . '/styles/images/' . Yii::app ()->params ['applicationLogoOrangeSmall'] ),
		'brandUrl' => $this->createUrl ( $_access ['url'] ['default'] ),
		'position' => BsHtml::NAVBAR_POSITION_STATIC_TOP,
		'htmlOptions' => array (
				'containerOptions' => array (
						'fluid' => true 
				) 
		),
		'items' => array (
				array (
						'class' => 'bootstrap.widgets.BsNav',
						'type' => 'navbar',
						'activateParents' => true,
						'encodeLabel' => false,
						'items' => array (
								array (
										'label' => Yii::t ( 'configuration', 'Escritorio' ),
										'url' => array (
												'default/dashboard' 
										),
										'icon' => BsHtml::GLYPHICON_DASHBOARD,
										'visible' => UserAbstract::checkAccess ( 'default', 'dashboard' ) 
								),
								array (
										'label' => Yii::t ( 'configuration', 'Entidades' ),
										'icon' => BsHtml::GLYPHICON_HOME,
										'visible' => UserAbstract::checkAccess ( 'entity' ),
										'items' => array (
												array (
														'label' => Yii::t ( 'configuration', 'Listado' ),
														'url' => array (
																'/admin/entity/admin' 
														),
														'icon' => BsHtml::GLYPHICON_LIST_ALT,
														'visible' => UserAbstract::checkAccess ( 'entity', 'admin' ) 
												),
												array (
														'label' => Yii::t ( 'configuration', 'Crear' ),
														'url' => array (
																'/admin/entity/create' 
														),
														'icon' => BsHtml::GLYPHICON_EDIT,
														'visible' => UserAbstract::checkAccess ( 'entity', 'create' ) 
												) 
										) 
								),
								array (
										'label' => Yii::t ( 'configuration', 'Servidores públicos' ),
										'icon' => BsHtml::GLYPHICON_USER,
										'visible' => UserAbstract::checkAccess ( 'user' ),
										'items' => array (
												array (
														'label' => Yii::t ( 'configuration', 'Listado' ),
														'url' => array (
																'/admin/user/admin' 
														),
														'icon' => BsHtml::GLYPHICON_LIST_ALT,
														'visible' => UserAbstract::checkAccess ( 'user', 'admin' ) 
												),
												array (
														'label' => Yii::t ( 'configuration', 'Crear' ),
														'url' => array (
																'/admin/user/create' 
														),
														'icon' => BsHtml::GLYPHICON_EDIT,
														'visible' => UserAbstract::checkAccess ( 'user', 'create' ) 
												) 
										) 
								),
								array (
										'label' => Yii::t ( 'configuration', 'Actividades' ),
										'icon' => BsHtml::GLYPHICON_CALENDAR,
										'visible' => UserAbstract::checkAccess ( 'event' ),
										'items' => array (
												array (
														'label' => Yii::t ( 'configuration', 'Listado' ),
														'url' => array (
																'/admin/event/admin' 
														),
														'icon' => BsHtml::GLYPHICON_LIST_ALT,
														'visible' => UserAbstract::checkAccess ( 'event', 'admin' ) 
												),
												array (
														'label' => Yii::t ( 'configuration', 'Crear' ),
														'url' => array (
																'/admin/event/create' 
														),
														'icon' => BsHtml::GLYPHICON_EDIT,
														'visible' => UserAbstract::checkAccess ( 'event', 'create' ) 
												) 
										) 
								),
								
								array (
										'label' => Yii::t ( 'configuration', 'Comunicados' ),
										'icon' => BsHtml::GLYPHICON_EDIT,
										'visible' => UserAbstract::checkAccess ( 'release' ),
										'items' => array (
												array (
														'label' => Yii::t ( 'configuration', 'Listado' ),
														'url' => array (
																'/admin/release/admin'
														),
														'icon' => BsHtml::GLYPHICON_LIST_ALT,
														'visible' => UserAbstract::checkAccess ( 'release', 'admin' )
												),
												array (
														'label' => Yii::t ( 'configuration', 'Crear' ),
														'url' => array (
																'/admin/release/create'
														),
														'icon' => BsHtml::GLYPHICON_EDIT,
														'visible' => UserAbstract::checkAccess ( 'release', 'create' )
												)
										)
								),								
								
								array (
										'label' => Yii::t ( 'configuration', 'Configuración' ),
										'icon' => BsHtml::GLYPHICON_COG,
										'visible' => UserAbstract::checkAccess ( 'configuration' ),
										'items' => array (
												array (
														'label' => Yii::t ( 'configuration', 'Países' ) . ' ' . BsHtml::icon ( BsHtml::GLYPHICON_INFO_SIGN, array (
																'data-original-title' => Yii::t ( 'configuration', 'Denominación del país o territorio según la norma ISO 3166-1.' ),
																'data-container' => 'body',
																'data-toggle' => 'tooltip' 
														) ),
														'url' => array (
																'/admin/configuration/createCountry' 
														),
														'icon' => BsHtml::GLYPHICON_GLOBE,
														'visible' => UserAbstract::checkAccess ( 'configuration', 'createCountry' ) 
												),
												array (
														'label' => Yii::t ( 'configuration', 'Departamentos' ),
														'url' => array (
																'/admin/configuration/createDepartment' 
														),
														'icon' => BsHtml::GLYPHICON_FLAG,
														'visible' => UserAbstract::checkAccess ( 'configuration', 'createDepartment' ) 
												),
												array (
														'label' => Yii::t ( 'configuration', 'Ciudades' ),
														'url' => array (
																'/admin/configuration/createCity' 
														),
														'icon' => BsHtml::GLYPHICON_MAP_MARKER,
														'visible' => UserAbstract::checkAccess ( 'configuration', 'createCity' ) 
												),
												array (
														'label' => Yii::t ( 'configuration', 'Tipos de actividades' ) . ' ' . BsHtml::icon ( BsHtml::GLYPHICON_INFO_SIGN, array (
																'data-original-title' => Yii::t ( 'configuration', 'Tipos/Categorías de actividades por ejemplo: Teatro, Musica, Cultura, etc.' ),
																'data-container' => 'body',
																'data-toggle' => 'tooltip' 
														) ),
														'url' => array (
																'/admin/configuration/createEventType' 
														),
														'icon' => BsHtml::GLYPHICON_WRENCH,
														'visible' => UserAbstract::checkAccess ( 'configuration', 'createEventType' ) 
												) 
										) 
								),
								BsHtml::menuDivider ( array (
										'class' => 'visible-xs-block' 
								) ),
								array (
										'style' => 'font-size: 40px',
										'class' => 'visible-xs-block text-center',
										'icon' => empty ( $imageTemplate ) ? BsHtml::GLYPHICON_USER : null 
								),
								
								array (
										'label' => BsHtml::tag ( 'h4', array (), UserAbstract::getRoleLabel ( $user->role ) ),
										'class' => 'visible-xs-block text-center' 
								),
								array (
										'label' => BsHtml::tag ( 'i', array (), $territory ),
										'class' => 'visible-xs-block text-center' 
								),
								array (
										'label' => BsHtml::tag ( 'h6', array (), UserAbstract::getRoleLabel ( isset ( $user->entityOfUser->name ) ? $user->entityOfUser->name : null ) ),
										'visible' => isset ( $user->entityOfUser->name ),
										'class' => 'visible-xs-block text-center' 
								),
								array (
										'label' => BsHtml::tag ( 'strong', array (), Yii::app ()->user->getState ( 'user' )->name ),
										'class' => 'visible-xs-block text-center' 
								),
								array (
										'label' => Yii::t ( 'user', 'Ultimo acceso: {dataLastAccess}', array (
												'{dataLastAccess}' => $this->formatLocaleTimestampToDatetimeGeneric ( $user->dataLastAccessAux ) 
										) ),
										'class' => 'visible-xs-block text-center bg-info' 
								),
								BsHtml::menuDivider ( array (
										'class' => 'visible-xs-block' 
								) ),
								array (
										'label' => Yii::t ( 'configuration', 'Cerrar sesión ({userName})', array (
												'{userName}' => Yii::app ()->user->name 
										) ),
										'icon' => BsHtml::GLYPHICON_LOG_OUT,
										'url' => array (
												'/admin/default/logout' 
										),
										'linkOptions' => array (
												'class' => 'logout-two' 
										),
										'class' => 'visible-xs-block text-center' 
								) 
						) 
				),
				array (
						'class' => 'bootstrap.widgets.BsNav',
						'type' => 'navbar',
						'activateParents' => true,
						'encodeLabel' => false,
						'items' => array (
								array (
										'label' => $imageTemplate . Yii::app ()->user->getState ( 'user' )->name,
										'icon' => empty ( $imageTemplate ) ? BsHtml::GLYPHICON_USER : null,
										'class' => 'visible-lg-block',
										'items' => array (
												array (
														'label' => BsHtml::tag ( 'h4', array (), UserAbstract::getRoleLabel ( $user->role ) ) 
												),
												array (
														'label' => BsHtml::tag ( 'i', array (), $territory ) 
												),
												array (
														'label' => BsHtml::tag ( 'h6', array (), UserAbstract::getRoleLabel ( isset ( $user->entityOfUser->name ) ? $user->entityOfUser->name : null ) ),
														'visible' => isset ( $user->entityOfUser->name ) 
												),
												array (
														'label' => Yii::t ( 'user', 'Ultimo acceso: {dataLastAccess}', array (
																'{dataLastAccess}' => $this->formatLocaleTimestampToDatetimeGeneric ( $user->dataLastAccessAux ) 
														) ),
														'class' => 'bg-info text-center' 
												),
												BsHtml::menuDivider (),
												array (
														'label' => Yii::t ( 'configuration', 'Cerrar sesión ({userName})', array (
																'{userName}' => Yii::app ()->user->name 
														) ),
														'icon' => BsHtml::GLYPHICON_LOG_OUT,
														'url' => array (
																'/admin/default/logout' 
														),
														'linkOptions' => array (
																'class' => 'logout-two' 
														),
														'class' => 'text-center' 
												) 
										) 
								),
								array (
										'url' => array (
												'/admin/default/logout' 
										),
										'icon' => BsHtml::GLYPHICON_LOG_OUT,
										'htmlOptions' => array (
												'data-original-title' => Yii::t ( 'configuration', 'Cerrar sesión ({userName})', array (
														'{userName}' => Yii::app ()->user->name 
												) ),
												'data-container' => 'body',
												'data-toggle' => 'tooltip',
												'data-placement' => 'bottom',
												'class' => 'visible-lg-block' 
										),
										'linkOptions' => array (
												'class' => 'logout-one' 
										) 
								) 
						),
						'htmlOptions' => array (
								'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT 
						) 
				) 
		) 
) );