<div class="panel panel-orange">
	<div class="panel-heading">
  	<i class="fa fa-edit fa-fw"></i> <?php echo Yii::t('configuration','Formulario'); ?>
	</div>
  <!-- /.panel-heading -->
  <div class="panel-body">
  		<?php 
		$form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
			'id'=>'city-form',
				'htmlOptions' => array(
						'enctype' => 'multipart/form-data',
				),
	    // Please note: When you enable ajax validation, make sure the corresponding
	    // controller action is handling ajax validation correctly.
	    // There is a call to performAjaxValidation() commented in generated controller code.
	    // See class documentation of CActiveForm for details on this.
	    'enableAjaxValidation'=>false,
		));
		
		if (!$model->isNewRecord && isset ( $model->image ) && ! empty ($model->image )) {
			echo $imageTemplate = BsHtml::tag('div', array('class'=>'text-center margin-bottom'), BsHtml::imageResponsive( CityAbstract::getImagePath () . $model->image, $model->name )); // $imageTemplate = BsHtml::tag('span', array('id'=>'entity-image-menu'), BsHtml::imageCircle( EntityAbstract::getImagePath() . 'default.png');
		}
		?>
		<?php echo Yii::t('configuration','<p class="text-info">Campos marcados con <span class="text-danger">*</span> son requeridos.</p>'); ?>
	    <?php //echo $form->errorSummary($model); ?>
	    <?php echo $form->dropDownListControlGroup($model,'departmentId',Department::model()->getDepartmentsOfCountry(),array('empty'=>Yii::t('configuration','Seleccione'))); ?>    
	    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>100)); ?>
	    <?php echo $form->dropDownListControlGroup($model,'status',City::model()->getStatus()); ?>
		<?php echo $form->fileFieldControlGroup($model,'image',array('maxlength'=>250)); ?>
	    <?php echo BsHtml::submitButton($model->isNewRecord ? Yii::t('configuration','Crear') : Yii::t('configuration','Actualizar'), array('color'=>BsHtml::BUTTON_COLOR_SUCCESS)); ?>
	    <?php echo !$model->isNewRecord ? BsHtml::linkButton(Yii::t('configuration','Cancelar'),array('url'=>$this->createUrl('createCountry'),'color'=>BsHtml::BUTTON_COLOR_DANGER)) : ''; ?>	    
		<?php $this->endWidget(); ?>
	</div>
	<!-- /.panel-body -->
</div>
<!-- /.panel -->
