<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Configuración')	.	' / '	. Yii::t('configuration','Ciudades');  ?>
<?php $this->beginContent('/configuration/index'); ?>
<div class="row">
	<div class="col-lg-3">
		<?php $this->renderPartial('../city/_formConfiguration', array('model'=>$model)); ?>
	</div>
  <div class="col-lg-9">
  	<?php $this->cityAdmin(); ?>
	</div>
</div>
<?php $this->endContent(); ?>