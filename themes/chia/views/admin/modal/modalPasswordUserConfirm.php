<?php
$html = array();

if($modalPasswordUserConfirm == NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT)
	array_push($html, BsHtml::blockAlert(BsHtml::ALERT_COLOR_ERROR, Yii::t('user','Contraseña incorrecta.')));

array_push($html, Yii::t ( 'configuration', '<p>Por razones de seguridad por favor ingrese nuevamente su contraseña de usuario para continuar.</p>' ));
array_push($html, '<div class="form-group">');
array_push($html, BsHtml::label( Yii::t ( 'configuration', 'Contraseña <span class="required">*</span>' ), 'passwordUserConfirm', array('class' => 'control-label error required')));
array_push($html, '<div>');
array_push($html, BsHtml::passwordField('passwordUserConfirm'));
array_push($html, '<p id="requiredPasswordUserConfirm" class="help-block" style="display: none;">' . Yii::t ( 'yii', '{attribute} cannot be blank.', array('{attribute}' => Yii::t ( 'configuration', 'Contraseña' ))) . '</p>');
array_push($html, '<p id="minLengthPasswordUserConfirm" class="help-block" style="display: none;">' . Yii::t ( 'yii', '{attribute} is too short (minimum is {min} characters).', array('{attribute}' => Yii::t ( 'configuration', 'Contraseña'), '{min}' => UserAbstract::MINIMUM_PASSWORD_LENGTH)) . '</p>');
array_push($html, '</div>');
array_push($html, '</div>');
$this->widget ( 'bootstrap.widgets.BsModal', array (
		'id' => 'modalPasswordUserConfirm',
		'show' => $modalPasswordUserConfirm == NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT,
		'header' => Yii::t ( 'configuration', 'Confirmación de clave de usuario' ),
		'content' => implode('', $html),
		'footer' => array (
				BsHtml::button ( Yii::t ( 'configuration', 'Confirmar' ), array (
						'id' => 'modalConfirmUserPassword-success',
						'color' => BsHtml::BUTTON_COLOR_SUCCESS
				) ),
				BsHtml::button ( Yii::t ( 'configuration', 'Cancelar' ), array (
						'data-dismiss' => 'modal',
						'color' => BsHtml::BUTTON_COLOR_DANGER
				) )
		)
) );

/*
<div class="form-group">
	<label for="Event_site" class="control-label required">Lugar y dirección de actividad <span class="required">*</span></label>
	<div>
		<input type="text" placeholder="Lugar y dirección de actividad" class="form-control" id="Event_site" name="Event[site]" maxlength="250">
	</div>
</div> 
 
<div class="form-group has-error">
	<label for="Event_site" class="control-label error required">Lugar y dirección de actividad <span class="required">*</span></label>
	<div>
		<input type="text" value="" placeholder="Lugar y dirección de actividad" class="form-control error" id="Event_site" name="Event[site]" maxlength="250">
		<p class="help-block">Lugar y dirección de actividad no puede ser nulo.</p>
	</div>
</div>
*/