<?php
$options = array ();

if (UserAbstract::checkAccess ( $this->id, 'createCountry' ))
	$options [$this->createUrl ( 'createCountry' )] = Yii::t('configuration','Países')
;

if (UserAbstract::checkAccess ( $this->id, 'createDepartment' ))
	$options [$this->createUrl ( 'createDepartment' )] = Yii::t ( 'configuration', 'Departamentos' );

if (UserAbstract::checkAccess ( $this->id, 'createCity' ))
	$options [$this->createUrl ( 'createCity' )] = Yii::t ( 'configuration', 'Ciudades' );

if (UserAbstract::checkAccess ( $this->id, 'createEventType' ))
	$options [$this->createUrl ( 'createEventType' )] = Yii::t ( 'configuration', 'Tipos de actividad' );

$requestUri = Yii::app ()->request->requestUri;
?>

<!-- Nav tabs -->
<div class="row">
	<div class="col-lg-12">
		<ul class="nav nav-pills">
			<?php foreach($options as $url=>$label): ?>
				<li <?php echo $requestUri===$url?' class="active"':''; ?>><a
				href="<?php echo $url; ?>"><?php echo $label; ?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-12">
		<?php if($this->messageSuccess): ?>
			<div class="alert alert-success alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close"
				type="button">x</button>
		    <?php echo $this->messageSuccess; ?>
			</div>
		<?php endif; ?>
		<?php if($this->messageError): ?>
			<div class="alert alert-danger alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close"
				type="button">x</button>
		    <?php echo $this->messageSuccess; ?>
			</div>				
		<?php endif; ?>
		<br />
	</div>
	<!-- /.col-lg-12 -->
</div>
