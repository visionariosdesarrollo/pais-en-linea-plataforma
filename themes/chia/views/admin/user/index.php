<?php echo BsHtml::pageHeader(Yii::t('configuration','Usuarios')); ?>
<?php $this->renderPartial('_options'); ?>

<div class="row">
	<div class="col-lg-12">
		<?php echo $content; ?>
	</div>
  <!-- /.col-lg-12 -->                
</div>