<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Usuarios')	.	' / '	. Yii::t('configuration','Vista');  ?>
<?php $this->beginContent('index'); ?>
<div class="row">
	<div class="col-lg-12">

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-orange">
					<div class="panel-heading">
						<?php echo BsHtml::icon(BsHtml::GLYPHICON_USER); ?>
						<?php echo $model->name; ?>
			</div>
					<!-- /.panel-heading -->
					<div class="panel-body table-responsive">
	<?php
	$this->widget ( 'zii.widgets.CDetailView', array (
			'htmlOptions' => array (
					'class' => 'table table-striped table-condensed table-hover' 
			),
			'data' => $model,
			'attributes' => array (
					array (
							'name' => 'redundantDataOfUser.status',
							'value' => RedundantDataAbstract::getStatusLabel ( $model->redundantDataOfUser->status ),
							'cssClass' => $this->cssClassStatus ( null, $model->redundantDataOfUser ) 
					),
					array (
							'name' => 'dataLastAccess',
							'value' => $this->formatLocaleTimestampToDatetimeGeneric ( $model->dataLastAccess ),
							'cssClass' => 'bg-warning' 
					),
					array (
							'name' => 'redundantDataOfUser.creationDate',
							'value' => $this->formatLocaleTimestampToDatetimeGeneric ( $model->redundantDataOfUser->creationDate ),
							'cssClass' => 'bg-info' 
					),
					array (
							'name' => 'redundantDataOfUser.modifiedDate',
							'value' => $this->formatLocaleTimestampToDatetimeGeneric ( $model->redundantDataOfUser->modifiedDate ),
							'cssClass' => 'bg-info' 
					),
					array (
							'name' => 'entityOfUser.name',
							'visible' => isset ( $model->entityOfUser->name ) 
					),
					array (
							'name' => 'role',
							'value' => UserAbstract::getRoleLabel ( $model->role ),
							'cssClass' => 'bg-primary' 
					),
					'name',
					array (
							'name' => Yii::t ( 'user', 'Documento' ),
							'value' => strtoupper ( $model->documentType ) . ' ' . $model->document 
					),
					array (
							'name' => Yii::t ( 'user', 'Departamento / Municipio' ),
							'value' => $model->roleDepartmentOfUser->name . ' / ' . $model->roleCityOfUser->name 
					),
					'email',
					'username',
					array (
							'name' => 'password',
							'value' => Yii::t ( 'user', 'Oculto' ),
							'template' => '<tr class=\"{class}\"><th>{label}</th><td class="text-danger">{value}</td></tr>' 
					) 
			) 
	) );
	?>
	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->endContent(); ?>