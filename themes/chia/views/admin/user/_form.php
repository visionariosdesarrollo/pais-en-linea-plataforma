<?php
$form = $this->beginWidget ( 'bootstrap.widgets.BsActiveForm', array (
		'id' => 'user-form',
		'htmlOptions' => array(
				'enctype' => 'multipart/form-data',
		),
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation' => false 
) );

echo $form->hiddenField($model,'resetPassword', array('id'=>'hiddenResetPassword'));

if (!$model->isNewRecord && isset ( $model->image ) && ! empty ($model->image )) {
	echo $imageTemplate = BsHtml::tag('div', array('class'=>'text-center margin-bottom'), BsHtml::imageCircle ( UserAbstract::getImagePath () . $model->image, $model->name )); // $imageTemplate = BsHtml::tag('span', array('id'=>'entity-image-menu'), BsHtml::imageCircle( EntityAbstract::getImagePath() . 'default.png');
}
?>
<div class="row">
	<div class="col-lg-9">
		<div class="panel panel-orange">
			<div class="panel-heading">
				<i class="fa fa-edit fa-fw"></i> <?php echo Yii::t('configuration','Formulario'); ?>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
		  		<?php $this->renderPartial('../modal/modalPasswordUserConfirm', array('modalPasswordUserConfirm'=>$modalPasswordUserConfirm)); ?>
				<?php echo Yii::t('configuration','<p class="text-info">Campos marcados con <span class="text-danger">*</span> son requeridos.</p>'); ?>
		    	<?php //echo $form->errorSummary($model); ?>
			    <div class="row">
					<div class="col-lg-2">
					    <?php echo $form->dropDownListControlGroup($model,'documentType',$model->getDocumentTypes()); ?>
					  </div>
					<div class="col-lg-3">
					    <?php echo $form->textFieldControlGroup($model,'document',array('maxlength'=>50)); ?>
					  </div>
					<div class="col-lg-4">
					    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>50)); ?>
					  </div>
					<div class="col-lg-3">
							<?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>50)); ?>
						</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<?php echo $form->fileFieldControlGroup($model,'image',array('maxlength'=>250)); ?>
					</div>
				</div>					
				<?php if($model->role == UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES): ?>
				<div class="row">
 						<div class="col-lg-4">
					   		 <?php echo $form->textFieldControlGroup($model,'dependence',array('maxlength'=>250)); ?>
						</div>
						<div class="col-lg-4">			  
							 <?php echo $form->textFieldControlGroup($model,'position',array('maxlength'=>250)); ?>
						</div>
						<div class="col-lg-4">
							<?php echo $form->fileFieldControlGroup($model,'image',array('maxlength'=>250)); ?>
						</div>
				</div>				
				<?php endif ?>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>

	<div class="col-lg-3">
		<div class="panel panel-orange">
			<div class="panel-heading">
				<i class="fa fa-edit fa-fw"></i> <?php echo Yii::t('configuration','Datos de acceso'); ?>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
		  		<div class="row">
					<div class="col-lg-12">			  
							<?php echo $form->dropDownListControlGroup($model,'status', $model->status==RedundantDataAbstract::STATUS_PENDIENTE ? $model->getStatusPending() : $model->getStatus(), array('empty'=>Yii::t('configuration','Seleccione'),'disabled'=>$model->status==RedundantDataAbstract::STATUS_PENDIENTE?true:false) ); ?>
						</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						   <?php echo $form->dropDownListControlGroup($model,'role', RoleAbstract::getRolesDropDownListControlGroup(),array('empty'=>Yii::t('configuration','Seleccione'), 'onchange'=>'js:document.getElementById("user-form").submit();')); ?>
						</div>
				</div>

				<?php 
					$user = Yii::app ()->user->getState ( 'user' );					
					$roles = $user->selectsRole($model->role);
				?>
					<?php if(in_array('roleCountryId', $roles)): ?>
					<div class="row">
						<div class="col-lg-12">
						   	<?php echo $form->dropDownListControlGroup($model,'roleCountryId',Country::model()->getCountries(RedundantDataAbstract::STATUS_ACTIVO, false),array('empty'=>Yii::t('configuration','Seleccione'), 'onchange'=>'js:document.getElementById("user-form").submit();')); ?>
						  </div>
					</div>
					<?php endif ?>

					<?php if(in_array('roleDepartmentId', $roles)): ?>
					<div class="row">
						<div class="col-lg-12">
								<?php echo $form->dropDownListControlGroup($model,'roleDepartmentId',Department::model()->getDepartmentsOfCountry(RedundantDataAbstract::STATUS_ACTIVO, (int)$model->roleCountryId, false),array('empty'=>Yii::t('configuration','Seleccione'), 'onchange'=>'js:document.getElementById("user-form").submit();')); ?>
							</div>
					</div>
					<?php endif ?>
				
					<?php if(in_array('roleCityId', $roles)): ?>
					<div class="row">
						<div class="col-lg-12">			  
								<?php echo $form->dropDownListControlGroup($model,'roleCityId',City::model()->getCityOfDepartments(RedundantDataAbstract::STATUS_ACTIVO, (int)$model->roleDepartmentId, false),array('empty'=>Yii::t('configuration','Seleccione'), 'onchange'=>'js:document.getElementById("user-form").submit();')); ?>
							</div>
					</div>
					<?php endif ?>

					<?php if(in_array('entityId', $roles)): ?>
					<div class="row">
						<div class="col-lg-12">  
								<?php echo $form->dropDownListControlGroup($model,'entityId',Entity::model()->getEntities(RedundantDataAbstract::STATUS_ACTIVO, (int)$model->roleCityId),array('empty'=>Yii::t('configuration','Seleccione'))); ?>
							</div>
					</div>
					<?php endif ?>				

				<div class="row">
					<div class="col-lg-12">
						<?php echo $form->textFieldControlGroup($model,'username',array('maxlength'=>20)); ?>
					</div>
				</div>
				<div class="row margin-bottom">
					<div class="col-lg-12">	
				<?php echo BsHtml::submitButton(Yii::t('configuration','Confirmar'), array('color'=>BsHtml::BUTTON_COLOR_SUCCESS)); ?>
				<?php echo !$model->isNewRecord ? BsHtml::linkButton(Yii::t('configuration','Cancelar'),array('url'=>$this->createUrl('admin'),'color'=>BsHtml::BUTTON_COLOR_DANGER)) : ''; ?>
									<?php echo !$model->isNewRecord ? BsHtml::linkButton(Yii::t('configuration','Resetear clave'),array('url'=>$this->createUrl('reset', array('id'=>$model->id)),'color'=>BsHtml::BUTTON_COLOR_INFO)) : ''; ?>
				</div>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>
<?php $this->endWidget(); ?>