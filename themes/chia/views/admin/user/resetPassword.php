<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('user','Cambio de contraseña');  ?>
<div class="row">
		<div class="col-md-4 col-md-offset-4">
	  	<div class="login-panel panel panel-orange">
	    	<div class="panel-heading">
	  	    <img alt="<?php echo Yii::app()->params['applicationName']; ?>" src="<?php echo Yii::app()->theme->baseUrl; ?>/styles/images/<?php echo Yii::app()->params['applicationLogoWhiteSmall']; ?>" />
	      </div>
	      <div class="panel-body">
					<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
						'id' => 'user-form',
						'enableClientValidation'=>false,
						'htmlOptions'=>array(
							'role'=>'form',
						),
					)); ?>
						<p class="lead"><?php echo Yii::t('user','Cambio de contraseña'); ?></p>
	        	<fieldset>

	        				<?php echo $form->passwordFieldControlGroup($model,'password'); ?>
							
							<?php echo CHtml::submitButton(Yii::t('user','Cambiar clave'),array('class'=>'btn btn-lg btn-success btn-block')); ?>
						</fieldset>
					<?php $this->endWidget(); ?>					
				</div>
			</div>
		</div>
</div>