<?php
/* @var $this UserController */
/* @var $data User */
?>


<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-orange">
			<div class="panel-heading">
				<i class="fa fa-edit fa-fw"></i> <?php echo Yii::t('configuration','Formulario'); ?>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">

				<div class="view">

					<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br /> <b><?php echo CHtml::encode($data->getAttributeLabel('entityId')); ?>:</b>
	<?php echo CHtml::encode($data->entityId); ?>
	<br /> <b><?php echo CHtml::encode($data->getAttributeLabel('role')); ?>:</b>
	<?php echo CHtml::encode($data->role); ?>
	<br /> <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br /> <b><?php echo CHtml::encode($data->getAttributeLabel('documentType')); ?>:</b>
	<?php echo CHtml::encode($data->documentType); ?>
	<br /> <b><?php echo CHtml::encode($data->getAttributeLabel('document')); ?>:</b>
	<?php echo CHtml::encode($data->document); ?>
	<br /> <b><?php echo CHtml::encode($data->getAttributeLabel('cityId')); ?>:</b>
	<?php echo CHtml::encode($data->cityId); ?>
	<br />

	<?php 
/*
	       * <b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	       * <?php echo CHtml::encode($data->email); ?>
	       * <br />
	       *
	       * <b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	       * <?php echo CHtml::encode($data->username); ?>
	       * <br />
	       *
	       * <b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	       * <?php echo CHtml::encode($data->password); ?>
	       * <br />
	       *
	       * <b><?php echo CHtml::encode($data->getAttributeLabel('dataLastAccess')); ?>:</b>
	       * <?php echo CHtml::encode($data->dataLastAccess); ?>
	       * <br />
	       *
	       */
	?>

</div>
			</div>
		</div>
	</div>
</div>
