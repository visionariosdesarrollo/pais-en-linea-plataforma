<div class="panel panel-orange">
	<div class="panel-heading">
  	<i class="fa fa-edit fa-fw"></i> <?php echo Yii::t('configuration','Formulario'); ?>
	</div>
  <!-- /.panel-heading -->
  <div class="panel-body">
		<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
		    'id'=>'event-form',
		    // Please note: When you enable ajax validation, make sure the corresponding
		    // controller action is handling ajax validation correctly.
		    // There is a call to performAjaxValidation() commented in generated controller code.
		    // See class documentation of CActiveForm for details on this.
		    'enableAjaxValidation'=>true,
		)); ?>
			<?php $this->renderPartial('../modal/modalPasswordUserConfirm', array('modalPasswordUserConfirm'=>$modalPasswordUserConfirm)); ?>
			<?php echo Yii::t('configuration','<p class="text-info">Campos marcados con <span class="text-danger">*</span> son requeridos.</p>'); ?>
			<?php //echo $form->errorSummary($model); ?>			
			<div class="row">
		    	<div class="col-lg-9">
					<?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>250, 'id' => 'nameEvent', 'labelOptions' => array('label' => $model->getAttributeLabel('name') . ' ' . BsHtml::icon(BsHtml::GLYPHICON_EXCLAMATION_SIGN, array('data-toggle'=>'tooltip','data-container'=>'body','data-original-title'=>Yii::t('configuration', 'Para utilizar el corrector ortográfico por favor presione el botón derecho del ratón sobre la palabra subrayada con el color amarillo y seleccione la palabra correspondiente en el campo "{attribute}".', array('{attribute}' => $model->getAttributeLabel('name')))))))); ?>
				</div>
				<div class="col-lg-3">
					<?php
						if($model->isNewRecord || $model->status == RedundantDataAbstract::STATUS_INACTIVO)
							echo $form->dropDownListControlGroup($model,'status',Event::model()->getStatusMinimum());
						else {
							echo BsHtml::activeLabel($model, 'status');
							echo BsHtml::tag('div', array(), BsHtml::bsLabel(RedundantData::getStatusLabel($model->status), array('color' => BsHtml::LABEL_COLOR_INFO)));
						}
					?>
				</div>			
			</div>
			<div class="row">
			  <div class="col-lg-3">
	    		<?php echo $form->textFieldControlGroup($model,'eventDateStart',array('data-role'=>'date','prepend' =>BsHtml::icon(BsHtml::GLYPHICON_CALENDAR),'append' =>BsHtml::icon(BsHtml::GLYPHICON_INFO_SIGN,array('data-toggle'=>'tooltip','data-container'=>'body','data-original-title'=>Yii::t('configuration', 'La actividad debe ser creada con {{limitCreation}} horas de anticipación a la fecha y hora de inicio de la misma.', array('{{limitCreation}}' => EventAbstract::LIMIT_CREATION)))))); ?>			  	
			  </div>
			  <div class="col-lg-3">
			  	<?php echo $form->textFieldControlGroup($model,'eventTimeStart',array('data-role'=>'time','prepend' =>BsHtml::icon(BsHtml::GLYPHICON_TIME),'groupOptions'=>array('class'=>'bootstrap-timepicker timepicker'))); ?>
			  </div>
			  <div class="col-lg-3">
			  	<?php echo $form->textFieldControlGroup($model,'eventDateEnd',array('data-role'=>'date','prepend' =>BsHtml::icon(BsHtml::GLYPHICON_CALENDAR),'append' =>BsHtml::icon(BsHtml::GLYPHICON_INFO_SIGN,array('data-toggle'=>'tooltip','data-container'=>'body','data-original-title'=>Yii::t('configuration', 'La fecha y hora de finalización de la actividad debe ser mayor que la fecha y hora de inicio.'))))); ?>
			  </div>
			  <div class="col-lg-3">
			  	<?php echo $form->textFieldControlGroup($model,'eventTimeEnd',array('data-role'=>'time','prepend' =>BsHtml::icon(BsHtml::GLYPHICON_TIME),'groupOptions'=>array('class'=>'bootstrap-timepicker timepicker'))); ?>
			  </div>			  			
			</div>		
			<div class="row">
				<div class="col-lg-12">
			  	<?php echo $form->textFieldControlGroup($model,'site',array('maxlength'=>250)); ?>
			  </div>
			</div>
			<div class="row">
				<div class="col-lg-12">
			  	<?php echo $form->inlineCheckBoxListControlGroup($model, 'type', EventType::model()->getEventsTypesForName(), array('labelOptions' => array('label' => $model->getAttributeLabel('type') . ' ' . BsHtml::icon(BsHtml::GLYPHICON_EXCLAMATION_SIGN, array('data-toggle'=>'tooltip','data-container'=>'body','data-original-title'=>Yii::t('configuration', 'Por favor contacte con el administrador del sistema para solicitar un nuevo "{attribute}".', array('{attribute}' => $model->getAttributeLabel('type')))))))); ?>
			  </div>
			</div>			
			<div class="row">
			  <div class="col-lg-12">
				  <?php echo $form->textAreaControlGroup($model,'body',array('id' => 'bodyEvent', 'rows'=>6, 'labelOptions' => array('label' => $model->getAttributeLabel('body') . ' ' . BsHtml::icon(BsHtml::GLYPHICON_EXCLAMATION_SIGN, array('data-toggle'=>'tooltip','data-container'=>'body','data-original-title'=>Yii::t('configuration', 'Para utilizar el corrector ortográfico por favor presione el botón derecho del ratón sobre la palabra subrayada con el color amarillo y seleccione la palabra correspondiente en el campo "{attribute}".', array('{attribute}' => $model->getAttributeLabel('body')))))))); ?>
				</div>
			</div>
			<div class="row">
			  <div class="col-lg-12">
					<?php echo BsHtml::submitButton(Yii::t('configuration','Confirmar'), array('id' => 'confirmEvent', 'size' => BsHtml::BUTTON_SIZE_DEFAULT)); ?>
					<?php echo BsHtml::linkButton(Yii::t('configuration','Cancelar'),array('url'=>$this->createUrl('admin'),'color'=>BsHtml::BUTTON_COLOR_DANGER)); ?>
				</div>
			</div>
		<?php $this->endWidget(); ?>
	</div>
	<!-- /.panel-body -->
</div>
<!-- /.panel -->