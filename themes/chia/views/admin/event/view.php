<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Actividades')	.	' / '	. Yii::t('configuration','Vista');  ?>
<?php $this->beginContent('index'); ?>
<div class="row">
	<div class="col-lg-12">

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-orange">
					<div class="panel-heading">
						<?php echo BsHtml::icon(BsHtml::GLYPHICON_CALENDAR); ?>
						<?php echo $model->name; ?>
			</div>
					<!-- /.panel-heading -->
					<div class="panel-body table-responsive">
<?php
$user = Yii::app ()->user->getState ( 'user' );
$this->widget ( 'zii.widgets.CDetailView', array (
		'htmlOptions' => array (
				'class' => 'table table-striped table-condensed table-hover' 
		),
		'data' => $model,
		'attributes' => array (
				array (
						'name' => 'redundantDataOfEvent.creationUserId',
						'value' => EventController::getUsersEntity( $model->redundantDataOfEvent->creationUserId ),
						'visible' => $user->role != UserAbstract::USUARIO_FINAl
				),
				array (
						'name' => 'redundantDataOfEvent.status',
						'value' => RedundantDataAbstract::getStatusLabel ( $model->redundantDataOfEvent->status ),
						'cssClass' => $this->cssClassStatus ( null, $model->redundantDataOfEvent ) 
				),
				array (
						'name' => 'redundantDataOfEvent.creationDate',
						'value' => $this->formatLocaleTimestampToDatetimeGeneric ( $model->redundantDataOfEvent->creationDate ),
						'cssClass' => 'bg-info' 
				),
				array (
						'name' => 'redundantDataOfEvent.modifiedDate',
						'value' => $this->formatLocaleTimestampToDatetimeGeneric ( $model->redundantDataOfEvent->modifiedDate ),
						'cssClass' => 'bg-info' 
				),
				'entityEvent.name',
				'name',
				array (
						'name' => 'type',
						'value' => implode(', ', $model->type ),
				),
				'eventDateStart',
				'eventTimeStart',
				'eventDateEnd',
				'eventTimeEnd',
				'site',
				'body' 
		)
) );
?>
			<div class="row">
			 	<div class="col-lg-12">
					<?php
						$user = Yii::app ()->user->getState ( 'user' );
						if($user->role == UserAbstract::USUARIO_FINAl  && $model->status != RedundantDataAbstract::STATUS_CANCELADO) {
							echo '<hr />';
							echo BsHtml::linkButton(Yii::t('configuration','Cancelar actividad'),array('url'=>$this->createUrl('cancel',array('id' => $model->id)),'color'=>BsHtml::BUTTON_COLOR_WARNING));
						} else if($user->role == UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS && $model->status == RedundantDataAbstract::STATUS_PENDIENTE) {
							echo '<hr />';
							echo BsHtml::linkButton(Yii::t('configuration','Activar actividad'),array('url'=>$this->createUrl('activate',array('id' => $model->id)),'color'=>BsHtml::BUTTON_COLOR_WARNING));
						}
					?>
				</div>
			</div>
	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->endContent(); ?>