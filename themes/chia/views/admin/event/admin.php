<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Actividades')	.	' / '	. Yii::t('configuration','Listado');  ?>
<?php $this->beginContent('index'); ?>
<?php $this->renderPartial('../default/_eventsStatus');  ?>
<div class="panel panel-orange">
	<div class="panel-heading">
		<i class="fa fa-th-list fa-fw"></i> <?php echo Yii::t('configuration','Listado'); ?></div>
	<div class="panel-body">
		<?php
		$user = Yii::app ()->user->getState ( 'user' );
		$this->widget ( 'bootstrap.widgets.BsGridView', array (
				'id' => 'event-grid',
				'htmlOptions' => array (
						'class' => 'table-responsive',
				),
				'dataProvider' => $model->search (),
				'pager' => array (
						'class' => 'bootstrap.widgets.BsPager',
						'size' => BsHtml::PAGINATION_SIZE_DEFAULT,
						'nextPageLabel' => Yii::t ( 'configuration', 'Siguiente' ),
						'prevPageLabel' => Yii::t ( 'configuration', 'Anterior' ),
						'firstPageLabel' => Yii::t ( 'configuration', 'Primero' ),
						'lastPageLabel' => Yii::t ( 'configuration', '�ltimo' ) 
				),
				'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
				'ajaxUrl' => $this->createUrl ( 'admin' ),
				'filter' => $model,
				'type' => BsHtml::GRID_TYPE_CONDENSED . ' ' . BsHtml::GRID_TYPE_STRIPED,
				'columns' => array (
						array (
								'name' => 'name',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus' 
								),
								'htmlOptions' => array (
										'id' => 'event-grid-name' 
								) 
						),
						
						array (
								'name' => 'entityId',
								'value' => '$data->entityEvent->name',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus'
								),
								'filter' => CHtml::activeDropDownList ( $model, 'entityId', Entity::model ()->getEntities (null, $user->roleCityId), array (
										'empty' => Yii::t ( 'configuration', 'Seleccione' ),
										'class' => 'form-control'
								) ),
								'visible' => $user->role === UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO
						),
						array (
								'name' => 'creationUserId',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus'
								),
								'value' => array (
										$this,
										'creationUser'
								),
								'filter' => CHtml::activeDropDownList ( $model, 'creationUserId', User::model ()->getUsersEditorsOfEntities (), array (
										'empty' => Yii::t ( 'configuration', 'Seleccione' ),
										'class' => 'form-control'
								) ),
								'type' => 'raw',
								'visible' => $user->role === UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO || $user->role === UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES
						),
						array (
								'name' => 'creationUserId',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus'
								),
								'value' => array (
										$this,
										'creationUser'
								),
								'filter' => CHtml::activeDropDownList ( $model, 'creationUserId', User::model ()->getUsersEditorsOfEntities (), array (
										'empty' => Yii::t ( 'configuration', 'Seleccione' ),
										'class' => 'form-control'
								) ),
								'type' => 'raw',
								'visible' => $user->role === UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS
						),
						array (
								'name' => 'type',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus'
								),
								'value' => array (
										$this,
										'arrayToString'
								),
								'filter' => CHtml::activeDropDownList ( $model, 'type', EventType::model ()->getEventsTypesForName (), array (
										'empty' => Yii::t ( 'configuration', 'Seleccione' ),
										'class' => 'form-control'
								) ),
								'type' => 'raw'
						),						
						array (
								'name' => 'status',
								'value' => 'RedundantDataAbstract::getStatusLabel($data->status)',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus' 
								),
								'filter' => CHtml::activeDropDownList ( $model, 'status', EventAbstract::getStatusList ( ), array (
										'empty' => Yii::t ( 'configuration', 'Seleccione' ),
										'class' => 'form-control' 
								) ),
								'type' => 'raw' 
						),
						array (
								'name' => 'eventDateStart',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus' 
								),
								'value' => array (
										$this,
										'formatLocaleTimestampToDate' 
								),
								'filter' => CHtml::activeTextField ( $model, 'eventDateStart', array (
										'class' => 'form-control',
										'data-role' => 'date' 
								) ),
								'type' => 'raw' 
						),
						array (
								'name' => 'creationDate',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus' 
								),
								'value' => array (
										$this,
										'formatLocaleTimestampToDatetime' 
								),
								'filter' => CHtml::activeTextField ( $model, 'creationDate', array (
										'class' => 'form-control',
										'data-role' => 'date' 
								) ),
								'type' => 'raw' 
						),
						array (
								'class' => 'nh.zii.widgets.grid.NHBsButtonColumn',
								'cssClassExpression' => array (
										$this,
										'cssClassStatus' 
								),
								'template' => '{update} {delete}',
								'deleteConfirmation' => true,
								'deleteColumnIdLabelConfirmation' => 'event-grid-name',
								'template' => '{view} {update} {delete}',
								'buttons' => array (
										'view' => array (
												'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->primaryKey))',
												'options' => array (
														'class' => 'btn btn-info btn-circle'
												),
												'visible' => '"' . UserAbstract::checkAccess ( $this->id, 'view' ) . '"'
										),							
										'update' => array (
												'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey))',
												'options' => array (
														'class' => 'btn btn-warning btn-circle' 
												),
												'visible' => 'EventController::checkAccessEventUpdate ( "'	.	$this->id	.	'", $data )' 
										),
										'delete' => array (
												'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
												'options' => array (
														'class' => 'btn btn-danger btn-circle' 
												),
												'visible' => 'EventController::checkAccessEventDelete ( "'	.	$this->id	.	'", $data )'
										) 
								) 
						) 
				) 
		) );
		?>
		</div>
</div>
<?php $this->endContent(); ?>	