<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Entidades')	.	' / '	. Yii::t('configuration','Vista');  ?>
<?php $this->beginContent('index'); ?>
<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-orange">
					<div class="panel-heading">
						<?php echo BsHtml::icon(BsHtml::GLYPHICON_HOME); ?>
						<?php echo $model->name; ?>
			</div>
					<!-- /.panel-heading -->
					<div class="panel-body table-responsive">

<?php
if (isset ( $model->image ) && ! empty ($model->image )) {
	echo $imageTemplate = BsHtml::tag('div', array('class'=>'text-center margin-bottom'), BsHtml::imageCircle ( EntityAbstract::getImagePath () . $model->image, $model->name )); // $imageTemplate = BsHtml::tag('span', array('id'=>'entity-image-menu'), BsHtml::imageCircle( EntityAbstract::getImagePath() . 'default.png');
}

$this->widget ( 'zii.widgets.CDetailView', array (
		'htmlOptions' => array (
				'class' => 'table table-striped table-condensed table-hover' 
		),
		'data' => $model,
		'attributes' => array (
				array (
						'name' => 'redundantDataOfEntity.status',
						'value' => RedundantDataAbstract::getStatusLabel ( $model->redundantDataOfEntity->status ),
						'cssClass' => $this->cssClassStatus ( null, $model->redundantDataOfEntity ) 
				),
				array (
						'name' => 'redundantDataOfEntity.creationDate',
						'value' => $this->formatLocaleTimestampToDatetimeGeneric ( $model->redundantDataOfEntity->creationDate ),
						'cssClass' => 'bg-info' 
				),
				array (
						'name' => 'redundantDataOfEntity.modifiedDate',
						'value' => $this->formatLocaleTimestampToDatetimeGeneric ( $model->redundantDataOfEntity->modifiedDate ),
						'cssClass' => 'bg-info' 
				),
				'cityOfEntity.name',
				'name',
				'description',
				'email',
				'phoneOne',
				'phoneTwo'
		) 
) );
?>
	</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->endContent(); ?>