<div class="panel panel-orange">
	<div class="panel-heading">
		<i class="fa fa-edit fa-fw"></i> <?php echo Yii::t('configuration','Formulario'); ?>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<?php
		$user = Yii::app ()->user->getState ( 'user' );
		
		if (!$model->isNewRecord && isset ( $model->image ) && ! empty ($model->image )) {
			echo $imageTemplate = BsHtml::tag('div', array('class'=>'text-center margin-bottom'), BsHtml::imageCircle ( EntityAbstract::getImagePath () . $model->image, $model->name )); // $imageTemplate = BsHtml::tag('span', array('id'=>'entity-image-menu'), BsHtml::imageCircle( EntityAbstract::getImagePath() . 'default.png');
		}
		
$form = $this->beginWidget ( 'bootstrap.widgets.BsActiveForm', array (
				'id' => 'entity-form',
		'htmlOptions' => array(
				'enctype' => 'multipart/form-data',
		),
				// Please note: When you enable ajax validation, make sure the corresponding
				// controller action is handling ajax validation correctly.
				// There is a call to performAjaxValidation() commented in generated controller code.
				// See class documentation of CActiveForm for details on this.
				'enableAjaxValidation' => false 
		) );
		?>
			<?php $this->renderPartial('../modal/modalPasswordUserConfirm', array('modalPasswordUserConfirm'=>$modalPasswordUserConfirm)); ?>		
			<?php echo Yii::t('configuration','<p class="text-info">Campos marcados con <span class="text-danger">*</span> son requeridos.</p>'); ?>
			<?php //echo $form->errorSummary($model); ?>
			<div class="row">
			<div class="col-lg-6">
					<?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>50)); ?>
				</div>
			<div class="col-lg-2">			  
					<?php echo $form->dropDownListControlGroup($model,'status',$model->getStatus(), array('empty'=>Yii::t('configuration','Seleccione')) ); ?>
				</div>
			<div class="col-lg-4">
					<?php echo $form->fileFieldControlGroup($model,'image',array('maxlength'=>250)); ?>prestashop
				</div>				
		</div>
		<div class="row">
			<?php if($user->role === UserAbstract::ADMINISTRADOR_SUPER_USUARIO): ?>
					<div class="col-lg-3">
					<?php echo $form->dropDownListControlGroup($model,'cityId',City::model()->getCityOfDepartments(null, null, true),array('empty'=>Yii::t('configuration','Seleccione'))); ?>
				</div>
			<?php endif; ?>
				
			<div class="col-lg-3">
					<?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>50)); ?>
				</div>
			<div class="col-lg-3">
					<?php echo $form->textFieldControlGroup($model,'phoneOne',array('maxlength'=>20)); ?>
				</div>
			<div class="col-lg-3">
					<?php echo $form->textFieldControlGroup($model,'phoneTwo',array('maxlength'=>20)); ?>
				</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
			  	<?php echo $form->textAreaControlGroup($model,'description',array('rows'=>6)); ?>
			  </div>
		</div>
		  <?php echo BsHtml::submitButton(Yii::t('configuration','Confirmar'), array('color'=>BsHtml::BUTTON_COLOR_SUCCESS)); ?>
			<?php echo !$model->isNewRecord ? BsHtml::linkButton(Yii::t('configuration','Cancelar'),array('url'=>$this->createUrl('admin'),'color'=>BsHtml::BUTTON_COLOR_DANGER)) : ''; ?>
		<?php $this->endWidget(); ?>
	</div>
	<!-- /.panel-body -->
</div>
<!-- /.panel -->