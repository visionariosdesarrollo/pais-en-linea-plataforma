<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Escritorio');  ?>
<?php echo BsHtml::pageHeader(Yii::t('system','Escritorio', array('headerOptions' => '' ))); ?>
<?php $this->renderPartial('_eventsStatus');  ?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-orange">
			<div class="panel-heading">
            	<?php echo BsHtml::icon(BsHtml::GLYPHICON_STATS)	.	' '	.	Yii::t ( 'configuration', 'Estadística de actividades' ); ?>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-3"><?php $this->renderPartial('_formStatisticsOfEventsForm', array('model' => $modelStatisticsOfEventsForm)); ?></div>
					<div class="col-lg-9">
						<?php
						$this->widget ( 'application.extensions.jqplot.JqplotGraphWidget', array (
								'data' => array (
									array_values($events),
								),
								'options' => array (
										'animate' => 'js:!$.jqplot.use_excanvas',
										//'resetAxes' => true,
										'seriesDefaults' => array (
												'renderer' => 'js:$.jqplot.BarRenderer',
												'pointLabels' => array (
													'show' => true,
														'stackedValue' => true,
												),
												'rendererOptions' => array(
													// Set the varyBarColor option to true to use different colors for each bar.
													// The default series colors are used.
													'varyBarColor' => true
												),
										),
										'legend' => array (
												//'show' => 'true',
												//'location' => 'nw'
										),
										'title' => Yii::t ( 'configuration', 'Actividades' ),
										'axes' => array (
												'xaxis' => array (
														'renderer' => 'js:$.jqplot.CategoryAxisRenderer',
														'label' =>  Yii::t ( 'configuration', 'Mes y año de creación' ),
														'ticks' => array_keys($events),
														'tickOptions' => array(
															'formatString' =>  '%s',
														),
												),
												'yaxis' => array (
													'min' => 0,
													'max' => 100,
													'label' =>  Yii::t ( 'configuration', 'Número de actividades' ),
													'tickOptions' => array(
														'formatString' =>  '%d',
													),
												)

										),
										'highlighter' => array (
											'show' => false
										)
								),
								'htmlOptions' => array (),
								'pluginScriptFile' => array (
									'jqplot.dateAxisRenderer.js',
									'jqplot.barRenderer.js',
									'jqplot.categoryAxisRenderer.js'
								)
						) );
						?>
 					</div>
				</div>
			</div>
		</div>
	</div>
</div>
