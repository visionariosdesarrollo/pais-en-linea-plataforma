<?php 
Yii::import('application.extensions.widgets.*'); 
$user = Yii::app ()->user->getState ( 'user' );
?>
<div class="row">
	<div class="col-lg-2">
		<?php
		$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
				'url' => Yii::app ()->createUrl ( 'admin/event/admin' ),
				'color' => EventsCountWidget::COLOR_DEFAULT,
				'label' => Yii::t ( "system", 'Ver actividades' ),
				'icon' => BsHtml::GLYPHICON_CALENDAR 
		) );
		?>
	</div>
	<div class="col-lg-2">
		<?php
		$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
				'url' => Yii::app ()->createUrl ( 'admin/event/admin', array (
						'status' => RedundantData::STATUS_ACTIVO 
				) ),
				'color' => EventsCountWidget::COLOR_GREEN,
				'status' => RedundantData::STATUS_ACTIVO,
				'label' => Yii::t ( "system", 'Ver actividades' ),
				'icon' => BsHtml::GLYPHICON_CALENDAR 
		) );
		?>
	</div>
	<div class="col-lg-2">
		<?php
		$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
				'url' => Yii::app ()->createUrl ( 'admin/event/admin', array (
						'status' => RedundantData::STATUS_CUMPLIDO 
				) ),
				'color' => EventsCountWidget::COLOR_BLUE,
				'status' => RedundantData::STATUS_CUMPLIDO,
				'label' => Yii::t ( "system", 'Ver actividades' ),
				'icon' => BsHtml::GLYPHICON_CALENDAR 
		) );
		?>
	</div>
	<div class="col-lg-2">
		<?php
		$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
				'url' => Yii::app ()->createUrl ( 'admin/event/admin', array (
						'status' => RedundantData::STATUS_PENDIENTE
				) ),
				'color' => EventsCountWidget::COLOR_YELLOW,
				'status' => RedundantData::STATUS_PENDIENTE,
				'label' => Yii::t ( "system", 'Ver actividades' ),
				'icon' => BsHtml::GLYPHICON_CALENDAR 
		) );
		?>
	</div>
	<div class="col-lg-2">
		<?php
		$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
				'url' => Yii::app ()->createUrl ( 'admin/event/admin', array (
						'status' => RedundantData::STATUS_CANCELADO 
				) ),
				'color' => EventsCountWidget::COLOR_RED_TWO,
				'status' => RedundantData::STATUS_CANCELADO,
				'label' => Yii::t ( "system", 'Ver actividades' ),
				'icon' => BsHtml::GLYPHICON_CALENDAR 
		) );
		?>
	</div>	
	<?php if($user->role ==  UserAbstract::ADMINISTRADOR_SUPER_USUARIO): ?>
			<div class="col-lg-2">
			<?php
			$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
						'url' => Yii::app ()->createUrl ( 'admin/event/admin', array (
							'status' => RedundantData::STATUS_INACTIVO
					) ),
					'color' => EventsCountWidget::COLOR_YELLOW_TWO,
					'status' => RedundantData::STATUS_INACTIVO,
					'label' => Yii::t ( "system", 'Ver actividades' ),
					'icon' => BsHtml::GLYPHICON_CALENDAR 
			) );
			?>
		</div>
		<div class="col-lg-2">
			<?php
			$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
					'url' => Yii::app ()->createUrl ( 'admin/event/admin', array (
							'status' => RedundantData::STATUS_ELIMINADO 
					) ),
					'color' => EventsCountWidget::COLOR_RED,
					'status' => RedundantData::STATUS_ELIMINADO,
					'label' => Yii::t ( "system", 'Ver actividades' ),
					'icon' => BsHtml::GLYPHICON_CALENDAR 
			) );
			?>
		</div>
	<?php elseif($user->role ==  UserAbstract::ADMINISTRADOR_PRESIDENTE || $user->role ==  UserAbstract::ADMINISTRADOR_GOBERNADOR ||  $user->role ==  UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO): ?>
		<div class="col-lg-2">
			<?php
			$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
						'url' => Yii::app ()->createUrl ( 'admin/event/admin', array (
							'status' => RedundantData::STATUS_INACTIVO
					) ),
					'color' => EventsCountWidget::COLOR_YELLOW_TWO,
					'status' => RedundantData::STATUS_INACTIVO,
					'label' => Yii::t ( "system", 'Ver actividades' ),
					'icon' => BsHtml::GLYPHICON_CALENDAR 
			) );
			?>
		</div>
		<div class="col-lg-2">
			<?php
			$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
					'url' => Yii::app ()->createUrl ( 'admin/default/dashboard'),
					'color' => EventsCountWidget::COLOR_RED,
					'status' => RedundantData::STATUS_ELIMINADO,
					'label' => Yii::t ( "system", 'Ver actividades' ),
					'icon' => BsHtml::GLYPHICON_CALENDAR 
			) );
			?>
		</div>
	<?php elseif($user->role ==  UserAbstract::USUARIO_FINAl): ?>
		<div class="col-lg-2">
			<?php
			$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
						'url' => Yii::app ()->createUrl ( 'admin/event/admin', array (
							'status' => RedundantData::STATUS_INACTIVO
					) ),
					'color' => EventsCountWidget::COLOR_YELLOW_TWO,
					'status' => RedundantData::STATUS_INACTIVO,
					'label' => Yii::t ( "system", 'Ver actividades' ),
					'icon' => BsHtml::GLYPHICON_CALENDAR 
			) );
			?>
		</div>
		<div class="col-lg-2">
			<?php
			$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
					'url' => Yii::app ()->createUrl ( 'admin/event/admin'),
					'color' => EventsCountWidget::COLOR_RED,
					'status' => RedundantData::STATUS_ELIMINADO,
					'label' => Yii::t ( "system", 'Ver actividades' ),
					'icon' => BsHtml::GLYPHICON_CALENDAR 
			) );
			?>
		</div>
	<?php else: ?>
		<div class="col-lg-2">
			<?php
			$this->widget ( 'application.extensions.widgets.EventsCountWidget', array (
					'url' => Yii::app ()->createUrl ( 'admin/default/dashboard'),
					'color' => EventsCountWidget::COLOR_RED,
					'status' => RedundantData::STATUS_ELIMINADO,
					'label' => Yii::t ( "system", 'Ver actividades' ),
					'icon' => BsHtml::GLYPHICON_CALENDAR 
			) );
			?>
		</div>	
	<?php endif ?>
</div>