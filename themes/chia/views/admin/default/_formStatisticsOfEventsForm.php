  <div class="well well-sm">
  	<h4><i class="fa fa-edit fa-fw"></i> <?php echo Yii::t('configuration','Parametros de búsqueda'); ?></h4>
  
  		<?php 
			$user = Yii::app ()->user->getState ( 'user' );
			$roles = $user->selectsRole($user->role);
		?>
		<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
			'id'=>'model-statistics-of-events-form',
	    // Please note: When you enable ajax validation, make sure the corresponding
	    // controller action is handling ajax validation correctly.
	    // There is a call to performAjaxValidation() commented in generated controller code.
	    // See class documentation of CActiveForm for details on this.
	    'enableAjaxValidation'=>false,
		)); ?>
		<?php if(in_array('roleCountryId', $roles)): ?>
		<div class="row margin-bottom">
			<div class="col-lg-12">
				<?php echo CHtml::activeDropDownList ( $model, 'roleCountryId', Country::model()->getCountries(RedundantDataAbstract::STATUS_ACTIVO, false), array (
											'empty' => Yii::t ( 'configuration', 'Seleccione país' ),
											'class' => 'form-control',
											'onchange'=>'js:document.getElementById("model-statistics-of-events-form").submit();'
									) ); ?>
			</div>
		</div>
		<?php endif ?>
		
		<?php if(in_array('roleDepartmentId', $roles)): ?>
		<div class="row margin-bottom">
			<div class="col-lg-12">
				<?php echo CHtml::activeDropDownList ( $model, 'roleDepartmentId', Department::model()->getDepartmentsOfCountry(RedundantDataAbstract::STATUS_ACTIVO, (int)$model->roleCountryId, false), array (
											'empty' => Yii::t ( 'configuration', 'Seleccione departamento' ),
											'class' => 'form-control',
											'onchange'=>'js:document.getElementById("model-statistics-of-events-form").submit();'
									) ); ?>
			</div>
		</div>
		<?php endif ?>
		
		<?php if(in_array('roleCityId', $roles)): ?>
		<div class="row margin-bottom">
			<div class="col-lg-12">
				<?php echo CHtml::activeDropDownList ( $model, 'roleCityId', City::model()->getCityOfDepartments(RedundantDataAbstract::STATUS_ACTIVO, (int)$model->roleDepartmentId, false), array (
											'empty' => Yii::t ( 'configuration', 'Seleccione ciudad' ),
											'class' => 'form-control',
											'onchange'=>'js:document.getElementById("model-statistics-of-events-form").submit();'
									) ); ?>
			</div>
		</div>
		<?php endif ?>

		<?php if(in_array('entityId', $roles)): ?>
		<div class="row margin-bottom">
			<div class="col-lg-12">
				<?php echo CHtml::activeDropDownList ( $model, 'entityId', Entity::model()->getEntities(RedundantDataAbstract::STATUS_ACTIVO, (int)$model->roleCityId), array (
											'empty' => Yii::t ( 'configuration', 'Seleccione entidad' ),
											'class' => 'form-control',
											'onchange'=>'js:document.getElementById("model-statistics-of-events-form").submit();'
									) ); ?>
			</div>
		</div>
		<?php endif ?>
		
		<div class="row margin-bottom">
			<div class="col-lg-12">
				<?php echo CHtml::activeDropDownList ( $model, 'userId', User::model()->getListUsersStatistics($model->roleCountryId, $model->roleDepartmentId, $model->roleCityId, $model->entityId), array (
											'empty' => Yii::t ( 'configuration', 'Seleccione redactor' ),
											'class' => 'form-control',
											'onchange'=>'js:document.getElementById("model-statistics-of-events-form").submit();'
									) ); ?>
			</div>
		</div>		
		<div class="row margin-bottom">
			<div class="col-lg-12">
				<?php echo CHtml::activeDropDownList ( $model, 'type', EventType::model ()->getEventsTypesForName (), array (
											'empty' => Yii::t ( 'configuration', 'Seleccione tipo de actividad' ),
											'class' => 'form-control',
											'onchange'=>'js:document.getElementById("model-statistics-of-events-form").submit();'
									) ); ?>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>