<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Inicio de sesión');  ?>
<div class="row">
		<div class="col-md-4 col-md-offset-4">
	  	<div class="login-panel panel panel-orange">
	    	<div class="panel-heading">
	  	    <img alt="<?php echo Yii::app()->params['applicationName']; ?>" src="<?php echo Yii::app()->theme->baseUrl; ?>/styles/images/<?php echo Yii::app()->params['applicationLogoWhiteSmall']; ?>" />
	      </div>
	      <div class="panel-body">
					<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
						'id'=>'login-form',
						'enableClientValidation'=>true,
						'htmlOptions'=>array(
							'role'=>'form',
						),							
						'clientOptions'=>array(
							'validateOnSubmit'=>true,
						),
					)); ?>
						<p class="lead"><?php echo Yii::t('user','Inicio de sesión'); ?></p>
	        	<fieldset>
							<?php echo $form->textFieldControlGroup($model,'username',array('placeholder'=>$model->getAttributeLabel('username'),'autofocus'=>true)); ?>
	        	
	        		<?php echo $form->passwordFieldControlGroup($model,'password',array('placeholder'=>$model->getAttributeLabel('password'))); ?>
							
							<div class="checkbox">
								<label>
									<?php echo $form->checkBox($model,'rememberMe'); ?><?php echo $model->getAttributeLabel('rememberMe'); ?>
								</label>
								<br/>
								<?php echo $form->error($model,'rememberMe'); ?>
							</div>
							<?php echo CHtml::submitButton(Yii::t('user','Ingresar'),array('class'=>'btn btn-lg btn-success btn-block')); ?>
						</fieldset>
					<?php $this->endWidget(); ?>					
				</div>
			</div>
		</div>	
</div>