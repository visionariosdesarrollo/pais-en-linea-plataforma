<div class="panel panel-orange">
	<div class="panel-heading">
		<i class="fa fa-th-list fa-fw"></i> <?php echo Yii::t('configuration','Listado'); ?></div>
	<div class="panel-body">
	<?php
	$this->widget ( 'bootstrap.widgets.BsGridView', array (
			'id' => 'country-grid',
			'htmlOptions' => array (
					'class' => 'table-responsive',
			),
			'dataProvider' => $model->search (),
			'pager' => array (
					'class' => 'bootstrap.widgets.BsPager',
					'size' => BsHtml::PAGINATION_SIZE_DEFAULT,
					'nextPageLabel' => Yii::t ( 'configuration', 'Siguiente' ),
					'prevPageLabel' => Yii::t ( 'configuration', 'Anterior' ),
					'firstPageLabel' => Yii::t ( 'configuration', 'Primero' ),
					'lastPageLabel' => Yii::t ( 'configuration', 'Último' ) 
			),
			'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
			'ajaxUrl' => $this->createUrl ( 'adminCountry' ),
			'filter' => $model,
			'type' => BsHtml::GRID_TYPE_CONDENSED . ' ' . BsHtml::GRID_TYPE_STRIPED,
			'columns' => array (
					array (
							'name' => 'code',
							'cssClassExpression' => array (
									$this,
									'cssClassStatus' 
							) 
					),
					array (
							'name' => 'name',
							'cssClassExpression' => array (
									$this,
									'cssClassStatus' 
							),
							'htmlOptions' => array (
									'id' => 'country-grid-name' 
							) 
					),
					array (
							'name' => 'status',
							'value' => 'RedundantDataAbstract::getStatusLabel($data->status)',
							'cssClassExpression' => array (
									$this,
									'cssClassStatus'
							),
							'filter' => CHtml::activeDropDownList ( $model, 'status', Country::model()->getStatus(), array (
									'empty' => Yii::t ( 'configuration', 'Seleccione' ),
									'class' => 'form-control'
							) ),
							'type' => 'raw'
					),					
					array (
							'name' => 'creationDate',
							'cssClassExpression' => array (
									$this,
									'cssClassStatus' 
							),
							'value' => array (
									$this,
									'formatLocaleTimestampToDatetime' 
							),
							'filter' => CHtml::activeTextField ( $model, 'creationDate', array (
									'class' => 'form-control',
									'data-role' => 'date' 
							) ),
							'type' => 'raw' 
					),
					array (
							'class' => 'nh.zii.widgets.grid.NHBsButtonColumn',
							'cssClassExpression' => array (
									$this,
									'cssClassStatus' 
							),
							'template' => '{update} {delete}',
							'deleteConfirmation' => true,
							'deleteColumnIdLabelConfirmation' => 'country-grid-name',
							'template' => '{update} {delete}',
							'buttons' => array (
									'update' => array (
											'url' => 'Yii::app()->controller->createUrl("updateCountry",array("id"=>$data->primaryKey))',
											'options' => array (
													'class' => 'btn btn-warning btn-circle' 
											),
											'visible' => '"' . UserAbstract::checkAccess ( $this->id, 'updateCountry' ) . '"' 
									),
									'delete' => array (
											'url' => 'Yii::app()->controller->createUrl("deleteCountry",array("id"=>$data->primaryKey))',
											'options' => array (
													'class' => 'btn btn-danger btn-circle' 
											),
											'visible' => '"' . UserAbstract::checkAccess ( $this->id, 'deleteCountry' ) . '"' 
									) 
							) 
					) 
			) 
	) );
	?>
	</div>
</div>