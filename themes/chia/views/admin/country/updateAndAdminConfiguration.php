<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Configuración')	.	' / '	. Yii::t('configuration','Países');  ?>
<?php $this->beginContent('/configuration/index'); ?>
<div class="row">
	<div class="col-lg-3">
		<?php $this->renderPartial('../country/_formConfiguration', array('model'=>$model)); ?>
	</div>
  <div class="col-lg-9">
  	<?php $this->countryAdmin(); ?>
	</div>
</div>
<?php $this->endContent(); ?>