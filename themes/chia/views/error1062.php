<?php $this->pageTitle = Yii::t('configuration','Adminsitración')	.	' / '	. Yii::t('configuration','Error'); ?>
<div class="errorPage">dasdsad
	<?php echo BsHtml::pageHeader(Yii::t('configuration','Error {{code}}',array('{{code}}'=>$code))); ?>
	<div class="row">
		<div class="col-lg-12">
			<?php echo BsHtml::image ( Yii::app ()->theme->baseUrl . '/styles/images/' . Yii::app ()->params ['applicationLogoOrangeMedium'] ); ?>
			<p><?php echo BsHtml::icon(BsHtml::GLYPHICON_THUMBS_DOWN, array('style'=>'font-size:10em;' )); ?></p>
			<p><?php echo CHtml::encode($message); ?></p>
		</div>
	</div>
</div>