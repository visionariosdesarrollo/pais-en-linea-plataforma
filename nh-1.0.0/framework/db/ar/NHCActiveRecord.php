<?php
/**
 * NHCActiveRecord class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

/**
 * NHCActiveRecord is the base class for classes representing relational data. 
 * All model classes for the application should extend from this base class.
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @package system.web.auth
 * @since 1.0
 */
abstract class NHCActiveRecord extends CActiveRecord
{
	const MYSQL_DATE_FORMAT = 'yyyy-MM-dd';
	const MYSQL_TIME_FORMAT = 'HH:mm:ss';
	
	public $passwordUserConfirm;
	public $status;
	public $order = 0;
	public $creationUser;
	public $modifiedUser;
	public $creationUserId;
	public $modifiedUserId;	
	public $creationDate;
	public $modifiedDate;
	
	public function changeStatus($status)
	{
		$this->status = $status;
		return $this->save(false);
	}
}