<?php
/**
 * NHCActiveRecordBehavior class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

/**
 * NHCActiveRecordBehavior is the base class for behaviors that can be attached to {@link CActiveRecord}.
 * All behavior classes for the application should extend from this base class.
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @package system.web.auth
 * @since 1.0
 */
class NHCActiveRecordBehavior extends CActiveRecordBehavior
{
}