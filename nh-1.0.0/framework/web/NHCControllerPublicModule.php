<?php
/**
 * NHCControllerPublicModule class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

/**
 * NHCControllerPublicModule is the customized base controller class for module "public" extends of NHController.
 * All controller classes for module "public" should extend from this base class.
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @package system.web.auth
 * @since 1.0 
 */
class NHCControllerPublicModule extends NHCController
{
		/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='/admin/layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	/**
	 * Initializes the controller.
	 * This method is called by the application before the controller starts to execute .
	 */
	public function init()
	{
		parent::init();
	}
}