CREATE TABLE tbl_mod_user_auth_item
(
   `id`                 SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
   `name`               VARCHAR(64) NOT NULL COMMENT 'Nombre',
   `type`               INTEGER NOT NULL COMMENT 'Tipo',
   `description`        TEXT COMMENT 'Descripción',
   `bizRule`            TEXT COMMENT 'Regla de validaciíon',
   `data`               TEXT COMMENT 'Datos',
   PRIMARY KEY (`id`),
   UNIQUE KEY `name` (`name`)
);

CREATE TABLE tbl_mod_user_auth_item_child
(
   `id`                   SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
   `parent`               VARCHAR(64) NOT NULL COMMENT 'Padre',
   `child`                VARCHAR(64) NOT NULL COMMENT 'Hijo',
   PRIMARY KEY (`id`),
   UNIQUE KEY `parent_-_child` (`parent`, `child`),
   FOREIGN KEY (`parent`) REFERENCES `tbl_mod_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
   FOREIGN KEY (`child`) REFERENCES `tbl_mod_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
);
 
CREATE TABLE tbl_mod_user_auth_assignment
(
   `id`                   SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
   `itemName`             VARCHAR(64) NOT NULL COMMENT 'Nombre',
   `userId`               SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Usuario',
   `bizRule`              TEXT COMMENT 'Regla de validaciíon',
   `data`                 TEXT COMMENT 'Datos',
   PRIMARY KEY (`id`),
   UNIQUE KEY `itemName_-_userId` (`itemName`, `userId`),
   FOREIGN KEY (`itemName`) REFERENCES `tbl_mod_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
   FOREIGN KEY (`userId`) REFERENCES `tbl_mod_user` (`id`)
);