<?php
/**
 * NHCUserIdentity class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

/**
 * NHCDbAuthManager represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided data can identity the user.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @package system.web.auth
 * @since 1.0
 */
class NHCUserIdentity extends CUserIdentity {
	const ERROR_STATUS_PENDING = 3;
	const ERROR_STATUS_INACTIVE = 4;
	
	/**
	 *
	 * @var integer the user's ID.
	 */
	private $_id;
	private $_userName;
	private $_role;
	public function getId() {
		return $this->_id;
	}
	public function getRole() {
		return $this->_role;
	}
	
	/**
	 * Authenticates a user.
	 * The implementation makes sure if the username and password exists in data base.
	 * 
	 * @return integet code of error.
	 */
	public function authenticate() {
		$user = User::model ()->findByAttributes ( array (
				'username' => $this->username 
		) );

		if ($user === null) // No user found!
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if (! $user->validatePassword ( $this->password )) // Invalid password!
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else {
			if ($user->type === UserAbstract::TYPE_ENTITY) { // User of entity
				$status = array (
						$user->entityOfUser->cityOfEntity->departmentOfCity->countryOfDepartment->status,
						$user->entityOfUser->cityOfEntity->departmentOfCity->status,
						$user->entityOfUser->cityOfEntity->status,
						$user->entityOfUser->status
						//$user->status 
				);
			} else {
				switch ($user->role) {
					case UserAbstract::ADMINISTRADOR_SUPER_USUARIO :
					case UserAbstract::ADMINISTRADOR_PRESIDENTE :
						$status = array (
								$user->roleCountryOfUser->status,
						);
						break;
					case UserAbstract::ADMINISTRADOR_GOBERNADOR :
						$status = array (
								$user->roleCountryOfUser->status,
								$user->roleDepartmentOfUser->status,
						);
						break;
					default:
						$status = array (
								$user->roleCountryOfUser->status,
								$user->roleDepartmentOfUser->status,
								$user->roleCityOfUser->status,
						);
						break;
				}
			}

			//if (in_array ( RedundantDataAbstract::STATUS_PENDIENTE, $status )) // Status pending!
				//$this->errorCode = self::ERROR_STATUS_PENDING;
			//else if (in_array ( RedundantDataAbstract::STATUS_INACTIVO, $status )) // Status inactive!
			if (in_array ( RedundantDataAbstract::STATUS_INACTIVO, $status )) // Status inactive!
				$this->errorCode = self::ERROR_STATUS_INACTIVE;
			else {
				$this->_id = $user->id;
				$this->_role = $user->role;
				$user->dataLastAccessAux = $user->dataLastAccess; 
				$this->setState ( 'user', $user );
				$user->dataLastAccess = new CDbExpression ( 'NOW()' );
				$user->save ();
				$this->errorCode = self::ERROR_NONE;
			}
		}
		return $this->errorCode;
	}
}