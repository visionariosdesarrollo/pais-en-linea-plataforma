<?php
/**
 * NHHttpSession class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

/**
 * NHHttpSession is the customized base provides session-level data management and the related configurations extends of CHttpSession.
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @package system.web
 * @since 1.0
 */
class NHHttpSession extends CHttpSession
{
	/**
	 * Updates the current session id with a newly generated one .
	 * Please refer to {@link http://php.net/session_regenerate_id} for more details.
	 * @param boolean $deleteOldSession Whether to delete the old associated session file or not.
	 * @since 1.1.8
	 */
	public function regenerateID($deleteOldSession=false)
	{
		if($this->getIsStarted())
			session_regenerate_id($deleteOldSession);
	}
}
