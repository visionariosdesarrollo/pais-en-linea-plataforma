<?php
/**
 * NHCControllerAdminModule class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

/**
 * NHCControllerAdminModule is the customized base controller class for module "admin" extends of NHController. 
 * All controller classes for module "admin" should extend from this base class.
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @package system.web.auth
 * @since 1.0 
 */

Yii::import('application.models.behaviors.*');

class NHCControllerAdminModule extends NHCController
{
	const MODAL_PASSWORD_USER_CONFIRM_INIT = 0;
	const MODAL_PASSWORD_USER_CONFIRM_CORRECT = 1;
	const MODAL_PASSWORD_USER_CONFIRM_INCORRECT = 2;

	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='/layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	/**
	 * @var string prompts user 
	 */
	public $messageSuccess=null;
	public $messageError=null;	
	
	/**
	 * Initializes the controller.
	 * This method is called by the application before the controller starts to execute .
	 */
	public function init()
	{
		parent::init();
		Yii::app()->errorHandler->errorAction='admin/default/error';		
		$clientScript=Yii::app()->clientScript;
		$clientScript->scriptMap=array(
			'jquery.js'=>false,
			'jquery.min.js'=>false,
			'jquery.ba-bbq.min.js'=>false,
			'jquery.ui'=>false,
		);		
	}

	/**
	 * Select css class of status.
	 */
	public function cssClassStatus($column,$row)
	{
		$cssClass='success';
		switch ($row->status)
		{
			case RedundantDataAbstract::STATUS_PENDIENTE:
				$cssClass='info';
				break;
			case RedundantDataAbstract::STATUS_INACTIVO:
				$cssClass='warning';
				break;
			case RedundantDataAbstract::STATUS_ELIMINADO:
				$cssClass='danger';
				break;
		}
		return $cssClass;
	}

	/**
	 * Select css class of status.
	 */
	public function cssClassStatusImage($column,$row)
	{
		$cssClass='success';
		switch ($row->status)
		{
			case RedundantDataAbstract::STATUS_PENDIENTE:
				$cssClass='info';
				break;
			case RedundantDataAbstract::STATUS_INACTIVO:
				$cssClass='warning';
				break;
			case RedundantDataAbstract::STATUS_ELIMINADO:
				$cssClass='danger';
				break;
		}
		return $cssClass	.	' text-center';
	}
	
	/**
	 * Format locale timestamp to datetime.
	 */
	public function formatLocaleTimestampToDatetime($row) {
		if ($row->creationDate) {
			$dateFormatter = new CDateFormatter ( Yii::app ()->locale->id );
			return $dateFormatter->formatDateTime ( $row->creationDate );
		}
	}

	/**
	 * Format locale timestamp to datetime generic.
	 */
	public function formatLocaleTimestampToDatetimeGeneric($date) {
		if ($date) {
			$dateFormatter = new CDateFormatter ( Yii::app ()->locale->id );
			return $dateFormatter->formatDateTime ( $date );
		}
	}
	
	/**
	 * Array to string.
	 */
	public function arrayToString($row) {
		if (is_array ( $row->type )) {
			return implode ( ',', $row->type );
		}
	}	
	
	/**
	 * Filter access control
	 * @return array a list of filter configurations.
	 * @see CFilter
	 */	
	public function filters()
	{
		return array(
			'accessControl',
			'checkAccess'
		);
	}
	
	public function filterCheckAccess($filterChain){
		$actionPermitive = array('index','error','login','logout');
		if(!is_numeric(array_search($filterChain->action->id, $actionPermitive)))
		{
			$user = Yii::app ()->user->getState ( 'user' );
			if($user->status==RedundantDataAbstract::STATUS_PENDIENTE) {
				if($this->action->id != 'resetPassword') {
					$this->forward('default/resetPassword');
				}
			}else {
				if(!UserAbstract::checkAccess($filterChain->controller->id, $filterChain->action->id))
					throw new CHttpException(404,Yii::t('configuration','La página solicitada no existe.'));
			}
		}
		$filterChain->run();
	}
	
	/*
	 * Validate password user confirm
	 * @return bool password correct
	 */
	public function validateModalPasswordUserConfirm() {
		$request = Yii::app()->request;
		$password = $request->getParam('passwordUserConfirm');
		return Yii::app()->user->getState('user')->validatePassword($password);
	}	
	
	/**
	 * Access rules for default for this controller .
	 * @return array list of access rules.
	 */
	public function accessRules()
	{
		return array(
				array('allow',
						'actions'=>array('index', 'login','error'),
						'users'=>array('?'),
				),
				array('deny',
						'actions'=>array('index', 'login'),
						'users'=>array('@'),
				),
				array('allow',
						'users'=>array('@'),
				),				
				/*
				array('deny',
					'actions'=>array('login'),
					'users'=>array('@'),
				),
				*/
    		array('deny', // deny all users
        	'users' => array('?'),
				),
		);
	}	
}