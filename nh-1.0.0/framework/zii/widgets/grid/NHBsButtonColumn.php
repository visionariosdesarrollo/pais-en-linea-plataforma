<?php
/**
 * NHBsButtonColumn class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

Yii::import('bootstrap.widgets.BsButtonColumn');

/**
 * NHBsButtonColumn It is the custom class for grid buttons {@link BsButtonColumn}.
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @package nh.zii.widgets.grid
 * @since 1.0
 */
class NHBsButtonColumn extends BsButtonColumn
{
	public $deleteColumnIdLabelConfirmation;
	
	/**
	 * Initializes the default buttons (view, update and delete).
	 */
	protected function initDefaultButtons()
	{
		if(!isset($this->buttons['delete']['click']))
		{
			$id=str_replace('-','',$this->grid->id);
			if($this->deleteConfirmation)
			{
				$this->grid->widget('bootstrap.widgets.BsModal',array(
						'id'=>$id.'errorDelete',
						'header'=>Yii::t('configuration','<i class="glyphicon glyphicon-alert"> Error</i>'),
						'content'=>Yii::t('configuration','Error al eliminar el registro <span class="'.$id.'nameDeleteRowGrid">{{name}}</span> seleccionado.'),
						'footer'=>array(
								BsHtml::button(Yii::t('configuration','Aceptar'),array(
									'data-dismiss'=>'modal',
									'color'=>BsHtml::BUTTON_COLOR_PRIMARY,
								)),
						)
				));				
				
				$this->grid->widget('bootstrap.widgets.BsModal',array(
					'id'=>$id.'confirmationDelete',
					'header'=>Yii::t('configuration','<i class="glyphicon glyphicon-alert"> Confirmación</i>'),
					'content'=>Yii::t('configuration','¿Desea eliminar el registro <span class="'.$id.'nameDeleteRowGrid">{{name}}</span> seleccionado?'),
					'footer'=>array(
						BsHtml::button(Yii::t('configuration','Aceptar'),array(
							'data-dismiss'=>'modal',
							'color'=>BsHtml::BUTTON_COLOR_PRIMARY,
							'id'=>$id.'confirmationDeleteOk',
						)),
						BsHtml::button(Yii::t('configuration','Cancelar'),array(
							'data-dismiss' => 'modal',
							'color'=>BsHtml::BUTTON_COLOR_DANGER
						))
					)
				));

				Yii::app()->getClientScript()->registerScript(__CLASS__.'#'.$id.'confirmationDelete',<<<EOD
jQuery("#{$id}confirmationDeleteOk").on("click", function(event) {
	{$id}deleteGrid();
});
EOD
);

				if($this->deleteColumnIdLabelConfirmation)
					$confirmation=<<<EOD
var text=$($(th).parent().parent().children("#{$this->deleteColumnIdLabelConfirmation}")).html();
if(text)
	$(".{$id}nameDeleteRowGrid").html(' <strong>"' + text + '"</strong> '); 
EOD
;
				$confirmation.='jQuery("#'.$id.'confirmationDelete").modal("show");';
			}
			else
				$confirmation=$id.'deleteGrid();';

			if(Yii::app()->request->enableCsrfValidation)
			{
				$csrfTokenName = Yii::app()->request->csrfTokenName;
				$csrfToken = Yii::app()->request->csrfToken;
				$csrf = "\n\t\tdata:{ '$csrfTokenName':'$csrfToken' },";
			}
			else
				$csrf = '';

			if($this->afterDelete===null)
				$this->afterDelete=<<<EOD
function(th, error, XHR) {
	if(!error)
		jQuery("#{$id}errorDelete").modal("show"); 
}
EOD
;
			Yii::app()->getClientScript()->registerScript(__CLASS__.'#'.$id.'deleteGrid',<<<EOD
		var th = null,
				afterDelete = $this->afterDelete;
function {$id}deleteGrid() {
	jQuery('#{$this->grid->id}').yiiGridView('update', {
		type: 'POST',
		dataType: 'json',
		url: jQuery(th).attr('href'),$csrf
		success: function(data) {
			if(data.error)
				jQuery('#{$this->grid->id}').yiiGridView('update');
			afterDelete(th, data.error, data.data);
		},
		error: function(XHR) {
			return afterDelete(th, false, XHR);
		}
	});
};
EOD
			);			

			$this->buttons['delete']['click']=<<<EOD
function() {
	th = this;
	$confirmation						
	return false;
}
EOD;
		}

		parent::initDefaultButtons();
	}

	/*
   * Expresions in options.
	 */
	protected function renderButton($id, $button, $row, $data)
	{
		if(isset($button['options']) && is_array($button['options']))
		{
			$expresions=array();
			foreach($button['options'] as $option=>$value)
			{
				if(preg_match('/\$(data|row)+/',$value))
				{
					$button['options'][$option] = $this->evaluateExpression($value, array('data'=>$data, 'row'=>$row));
				}
			}
		}

		parent::renderButton($id, $button, $row, $data);
	}
}