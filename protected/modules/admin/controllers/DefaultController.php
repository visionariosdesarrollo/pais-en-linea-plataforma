<?php
class DefaultController extends NHCControllerAdminModule {
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$this->forward ( 'login' );
	}
	public function actionDashboard() {
		$modelStatisticsOfEventsForm = new StatisticsOfEventsForm();
		if (isset ( $_POST ['StatisticsOfEventsForm'] )) {
			$modelStatisticsOfEventsForm->attributes = $_POST ['StatisticsOfEventsForm'];
		}

		$events = $modelStatisticsOfEventsForm->search();
		
		$this->render ( 'dashboard', array (
				'modelStatisticsOfEventsForm' => $modelStatisticsOfEventsForm,
				'events' => $events
		) );
	}

	/**
	 * Displays the login page
	 */
	public function actionResetPassword() {
		$this->layout = '/layouts/loginColumn';

		$user = Yii::app ()->user->getState ( 'user' );
		$model = User::model()->findByPk($user->id);

		if($user->status!=RedundantDataAbstract::STATUS_PENDIENTE)
			throw new CHttpException(404,Yii::t('configuration','La p�gina solicitada no existe.'));

		if (isset ( $_POST ['User'] )) {
			$model->password = $_POST ['User']['password'];
			if($model->validate()) {
				$model->password = $model->hashPassword($model->password);
				$model->status = RedundantDataAbstract::STATUS_ACTIVO;
				if($model->save(false)) {
					$user->password = $model->password;
					$user->redundantDataOfUser->status = $user->status = $model->status;
					$_access = UserAbstract::getAccess ();
					$this->redirect ( $this->createUrl ( $_access ['url'] ['default'] ) );
				}
			}
		}else
			$model->password = '';

		$this->render ( '../user/resetPassword', array (
				'model' => $model
		) );
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app ()->errorHandler->error) {
			if (Yii::app ()->request->isAjaxRequest)
				echo $error ['message'];
			else
				$this->render ( 'error', $error );
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin() {
		$this->layout = '/layouts/loginColumn';

		$model = new LoginForm ();

		if (isset ( $_POST ['ajax'] ) && $_POST ['ajax'] === 'login-form') {
			echo CActiveForm::validate ( $model );
			Yii::app ()->end ();
		}

		if (isset ( $_POST ['LoginForm'] )) {
			$model->attributes = $_POST ['LoginForm'];
			if ($model->validate () && $model->login ()) {
				$_access = UserAbstract::getAccess ();
				$this->redirect ( $this->createUrl ( $_access ['url'] ['default'] ) );
			}
		}

		$this->render ( 'login', array (
				'model' => $model
		) );
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app ()->user->logout ();
		$this->refresh ();
	}
}
