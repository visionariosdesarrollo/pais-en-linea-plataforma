<?php
Yii::import ( 'application.modules.admin.controllers.behaviors.entity.*' );

class EntityController extends NHCControllerAdminModule {
	public function behaviors() {
		return array (
				'EntityBehavior' => array (
						'class' => 'EntityBehavior' 
				) 
		);
	}
	public function actions() {
		return array (
				'view' => 'application.modules.admin.controllers.actions.entity.ViewAction',
				'create' => 'application.modules.admin.controllers.actions.entity.CreateAction',
				'update' => 'application.modules.admin.controllers.actions.entity.UpdateAction',
				'delete' => 'application.modules.admin.controllers.actions.entity.DeleteAction',
				'admin' => 'application.modules.admin.controllers.actions.entity.AdminAction' 
		);
	}
}