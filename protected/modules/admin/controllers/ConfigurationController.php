<?php
Yii::import('application.modules.admin.controllers.behaviors.configuration.*');

class ConfigurationController extends NHCControllerAdminModule
{		
	public function behaviors() {
    return array(
    	'ConfigurationCountryBehavior'=>array(
    		'class'=>'CountryBehavior',
      ),
    	'ConfigurationDepartmentBehavior'=>array(
    		'class'=>'DepartmentBehavior',
			),
    	'ConfigurationCityBehavior'=>array(
    		'class'=>'CityBehavior',
    	),
			'ConfigurationEventTypeBehavior'=>array(
				'class'=>'EventTypeBehavior',
    	),    		  		
		);
	}
	
	public function actions()
	{
		return array(
			'createCountry'=>'application.modules.admin.controllers.actions.configuration.country.CreateAction',
			'updateCountry'=>'application.modules.admin.controllers.actions.configuration.country.UpdateAction',
			'deleteCountry'=>'application.modules.admin.controllers.actions.configuration.country.DeleteAction',
			'adminCountry'=>'application.modules.admin.controllers.actions.configuration.country.AdminAction',
			'createDepartment'=>'application.modules.admin.controllers.actions.configuration.department.CreateAction',
			'updateDepartment'=>'application.modules.admin.controllers.actions.configuration.department.UpdateAction',
			'deleteDepartment'=>'application.modules.admin.controllers.actions.configuration.department.DeleteAction',
			'adminDepartment'=>'application.modules.admin.controllers.actions.configuration.department.AdminAction',
			'createCity'=>'application.modules.admin.controllers.actions.configuration.city.CreateAction',
			'updateCity'=>'application.modules.admin.controllers.actions.configuration.city.UpdateAction',
			'deleteCity'=>'application.modules.admin.controllers.actions.configuration.city.DeleteAction',
			'adminCity'=>'application.modules.admin.controllers.actions.configuration.city.AdminAction',
			'createEventType'=>'application.modules.admin.controllers.actions.configuration.eventType.CreateAction',
			'updateEventType'=>'application.modules.admin.controllers.actions.configuration.eventType.UpdateAction',
			'deleteEventType'=>'application.modules.admin.controllers.actions.configuration.eventType.DeleteAction',
			'adminEventType'=>'application.modules.admin.controllers.actions.configuration.eventType.AdminAction',
		);
	}

	/**
	* Models.
	*/
	public function actionIndex($id=null)
	{
		$this->render('index',array(
			'id'=>$id
		));
	}
}