<?php
Yii::import('application.modules.admin.controllers.behaviors.eventType.*');

class EventTypeController extends NHCControllerAdminModule
{
	public function behaviors() {
		return array(
			'EventBehavior'=>array(
				'class'=>'EventTypeBehavior',
			),
		);
	}

	public function actions()
	{
		return array(
			'create'=>'application.modules.admin.controllers.actions.eventType.CreateAction',
			'update'=>'application.modules.admin.controllers.actions.eventType.UpdateAction',
			'delete'=>'application.modules.admin.controllers.actions.eventType.DeleteAction',
			'admin'=>'application.modules.admin.controllers.actions.event.AdminAction',
		);
	}
}