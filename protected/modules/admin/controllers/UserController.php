<?php
Yii::import ( 'application.modules.admin.controllers.behaviors.user.*' );
class UserController extends NHCControllerAdminModule {
	public function behaviors() {
		return array (
				'UserBehavior' => array (
						'class' => 'UserBehavior' 
				)
		);
	}

	public function actions() {
		return array (
				'view' => 'application.modules.admin.controllers.actions.user.ViewAction',
				'create' => 'application.modules.admin.controllers.actions.user.CreateAction',
				'update' => 'application.modules.admin.controllers.actions.user.UpdateAction',
				'reset' => 'application.modules.admin.controllers.actions.user.ResetAction',
				'delete' => 'application.modules.admin.controllers.actions.user.DeleteAction',
				'admin' => 'application.modules.admin.controllers.actions.user.AdminAction' 
		);
	}

	public function roleCity($data, $row) {
		return isset($data->roleCityOfUser) ? $data->roleCityOfUser->name : Yii::t('configuration', '(No definido)'); 		
	}
	
	public function setLocation() {
		$user = Yii::app ()->user->getState ( 'user' );
		$attributes = array();
		
		$roles = array(UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO, UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES);
		
		if(in_array($user->role, $roles)) {
			$attributes['roleCountryId'] = $user->roleCountryId;
			$attributes['roleDepartmentId'] = $user->roleDepartmentId;
			$attributes['roleCityId'] = $user->roleCityId;
		}
		
		return $attributes;
	}
}