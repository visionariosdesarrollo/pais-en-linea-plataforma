<?php

class EntityBehavior extends CBehavior
{	
		/**
		 * Returns the data model based on the primary key given in the GET variable.
		 * If the data model is not found, an HTTP exception will be raised.
		 * @param integer $id the ID of the model to be loaded
		 * @return Entity the loaded model
		 * @throws CHttpException
		 */
		public function loadModelEntity($id)
		{
			$model=Entity::model()->findByPk($id);
			if($model===null && !isset($_GET['ajax']))
				throw new CHttpException(404,Yii::t('configuration','La p�gina solicitada no existe.'));
			return $model;
		}

		/**
		 * Performs the AJAX validation.
		 * @param Entity $model the model to be validated
		 */
		protected function performAjaxValidationEntity($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='entity-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}		
}