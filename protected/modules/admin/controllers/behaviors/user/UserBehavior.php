<?php

class UserBehavior extends CBehavior
{			 			
		/**
		 * Returns the data model based on the primary key given in the GET variable.
		 * If the data model is not found, an HTTP exception will be raised.
		 * @param integer $id the ID of the model to be loaded
		 * @return User the loaded model
		 * @throws CHttpException
		 */
		public function loadModelUser($id)
		{
			$model=User::model()->findByPk($id);
			if($model===null && !isset($_GET['ajax']))
				throw new CHttpException(404,Yii::t('configuration','La p�gina solicitada no existe.'));
			return $model;
		}

		/**
		 * Performs the AJAX validation.
		 * @param User $model the model to be validated
		 */
		protected function performAjaxValidationUser($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}		
}