<?php

class EventTypeBehavior extends CBehavior
{	
		/**
		 * Manages all models.
		 */
		public function eventTypeAdmin()
		{
			$model=new EventType('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['EventType']))
				$model->attributes=$_GET['EventType'];
		
			$this->owner->renderPartial('../eventType/adminConfiguration',array(
					'model'=>$model,
			));
		}
	
		/**
		 * Returns the data model based on the primary key given in the GET variable.
		 * If the data model is not found, an HTTP exception will be raised.
		 * @param integer $id the ID of the model to be loaded
		 * @return EventType the loaded model
		 * @throws CHttpException
		 */
		public function loadModelEventType($id)
		{
			$model=EventType::model()->findByPk($id);
			if($model===null && !isset($_GET['ajax']))
				throw new CHttpException(404,Yii::t('configuration','La p�gina solicitada no existe.'));
			return $model;
		}

		/**
		 * Performs the AJAX validation.
		 * @param Event $model the model to be validated
		 */
		protected function performAjaxValidationEvent($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='event-type-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}		
}