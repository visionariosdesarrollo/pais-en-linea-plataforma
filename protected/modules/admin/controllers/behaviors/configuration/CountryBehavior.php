<?php
class CountryBehavior extends CBehavior
{	
		/**
		 * Manages all models.
		 */
		public function countryAdmin()
		{
			$model=new Country('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['Country']))
				$model->attributes=$_GET['Country'];
		
			$this->owner->renderPartial('../country/adminConfiguration',array(
		 		'model'=>$model,
		 	));
		}
		 			
		/**
		 * Returns the data model based on the primary key given in the GET variable.
		 * If the data model is not found, an HTTP exception will be raised.
		 * @param integer $id the ID of the model to be loaded
		 * @return Country the loaded model
		 * @throws CHttpException
		 */
		public function loadModelCountry($id)
		{
			$model=Country::model()->findByPk($id);
			if($model===null && !isset($_GET['ajax']))
				throw new CHttpException(404,Yii::t('configuration','La p�gina solicitada no existe.'));
			return $model;
		}

		/**
		 * Performs the AJAX validation.
		 * @param Country $model the model to be validated
		 */
		protected function performAjaxValidationCountry($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='country-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}		
}