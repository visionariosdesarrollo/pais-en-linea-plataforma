<?php
class DepartmentBehavior extends CBehavior
{	
		/**
		 * Manages all models.
		 */
		public function departmentAdmin()
		{
			$model=new Department('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['Department']))
				$model->attributes=$_GET['Department'];
		
			$this->owner->renderPartial('../department/adminConfiguration',array(
		 		'model'=>$model,
		 	));
		}
		 			
		/**
		 * Returns the data model based on the primary key given in the GET variable.
		 * If the data model is not found, an HTTP exception will be raised.
		 * @param integer $id the ID of the model to be loaded
		 * @return Department the loaded model
		 * @throws CHttpException
		 */
		public function loadModelDepartment($id)
		{
			$model=Department::model()->findByPk($id);
			if($model===null && !isset($_GET['ajax']))
				throw new CHttpException(404,Yii::t('configuration','La p�gina solicitada no existe.'));
			return $model;
		}

		/**
		 * Performs the AJAX validation.
		 * @param Department $model the model to be validated
		 */
		protected function performAjaxValidationDepartment($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='department-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}		
}