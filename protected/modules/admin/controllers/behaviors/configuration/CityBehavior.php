<?php
class CityBehavior extends CBehavior
{	
		/**
		 * Manages all models.
		 */
		public function cityAdmin()
		{
			$model=new City('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['City']))
				$model->attributes=$_GET['City'];
		
			$this->owner->renderPartial('../city/adminConfiguration',array(
		 		'model'=>$model,
		 	));
		}
		 			
		/**
		 * Returns the data model based on the primary key given in the GET variable.
		 * If the data model is not found, an HTTP exception will be raised.
		 * @param integer $id the ID of the model to be loaded
		 * @return City the loaded model
		 * @throws CHttpException
		 */
		public function loadModelCity($id)
		{
			$model=City::model()->findByPk($id);
			if($model===null && !isset($_GET['ajax']))
				throw new CHttpException(404,Yii::t('configuration','La p�gina solicitada no existe.'));
			return $model;
		}

		/**
		 * Performs the AJAX validation.
		 * @param City $model the model to be validated
		 */
		protected function performAjaxValidationCity($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='city-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}		
}