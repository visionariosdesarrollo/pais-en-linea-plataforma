<?php

class ReleaseBehavior extends CBehavior
{	
		/**
		 * Returns the data model based on the primary key given in the GET variable.
		 * If the data model is not found, an HTTP exception will be raised.
		 * @param integer $id the ID of the model to be loaded
		 * @return Release the loaded model
		 * @throws CHttpException
		 */
		public function loadModelRelease($id)
		{
			$model=Release::model()->findByPk($id);
			if($model===null && !isset($_GET['ajax']))
				throw new CHttpException(404,Yii::t('configuration','La p�gina solicitada no existe.'));
			return $model;
		}

		/**
		 * Performs the AJAX validation.
		 * @param Release $model the model to be validated
		 */
		protected function performAjaxValidationRelease($model)
		{
			if(isset($_POST['ajax']) && $_POST['ajax']==='release-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}
		
		/**
		 * Format locale timestamp to date.
		 */
		public function formatLocaleTimestampToDate($row,$attribute)
		{
			$timestamp = CDateTimeParser::parse($row->releaseDateStart,NHCActiveRecord::MYSQL_DATE_FORMAT);
			$dateFormatter=new CDateFormatter(Yii::app()->locale->id);
			return $dateFormatter->formatDateTime($timestamp,'medium','');
		}
}