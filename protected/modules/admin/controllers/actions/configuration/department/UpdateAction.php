<?php
/**
 * Updates a particular model.
 * If update is successful, the browser will be redirected to the 'view' page.
 * @param integer $id the ID of the model to be updated
 */
class UpdateAction extends CAction
{
	public function run($id)
  {
		$model=$this->controller->loadModelDepartment($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationDepartment($model);

		if(isset($_POST['Department']))
		{
			$model->attributes=$_POST['Department'];
			if($model->save())
				$this->controller->redirect(array('createDepartment'));
		}

		$this->controller->render('../department/updateAndAdminConfiguration',array(
			'model'=>$model,
		));
  }
}