<?php
/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
class CreateAction extends CAction
{
	public function run()
  {
		$model=new Department;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationDepartment($model);

		if(isset($_POST['Department']))
		{
			$model->attributes=$_POST['Department'];
			if($model->save())
				$this->controller->refresh();
		}

		$this->controller->render('../department/createAndAdminConfiguration',array(
			'model'=>$model,
		));
  }
}