<?php
/**
 * Manages all models.
 */
class AdminAction extends CAction
{
	public function run()
  {
		$this->controller->eventTypeAdmin();
  }
}