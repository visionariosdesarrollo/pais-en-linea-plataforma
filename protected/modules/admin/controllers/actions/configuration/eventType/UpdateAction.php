<?php
/**
 * Updates a particular model.
 * If update is successful, the browser will be redirected to the 'view' page.
 * @param integer $id the ID of the model to be updated
 */
class UpdateAction extends CAction
{
	public function run($id)
  {
		$model=$this->controller->loadModelEventType($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationCity($model);

		if(isset($_POST['EventType']))
		{
			$model->attributes=$_POST['EventType'];
			if($model->save())
				$this->controller->redirect(array('createEventType'));
		}

		$this->controller->render('../eventType/updateAndAdminConfiguration',array(
			'model'=>$model,
		));
  }
}