<?php
/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
class CreateAction extends CAction
{
	public function run()
  {
		$model=new EventType;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationCity($model);

		if(isset($_POST['EventType']))
		{
			$model->attributes=$_POST['EventType'];
			if($model->save())
				$this->controller->refresh();
		}

		$this->controller->render('../eventType/createAndAdminConfiguration',array(
			'model'=>$model,
		));
  }
}