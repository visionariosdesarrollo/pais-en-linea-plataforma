<?php
/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
class CreateAction extends CAction
{
	public function run()
  {
		$model=new Country;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationCountry($model);

		if(isset($_POST['Country']))
		{
			$model->attributes=$_POST['Country'];
			if($model->save())
				$this->controller->refresh();
		}

		$this->controller->render('../country/createAndAdminConfiguration',array(
			'model'=>$model,
		));
  }
}