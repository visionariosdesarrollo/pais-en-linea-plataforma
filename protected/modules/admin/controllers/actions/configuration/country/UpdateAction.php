<?php
/**
 * Updates a particular model.
 * If update is successful, the browser will be redirected to the 'view' page.
 * @param integer $id the ID of the model to be updated
 */
class UpdateAction extends CAction
{
	public function run($id)
  {
		$model=$this->controller->loadModelCountry($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationCountry($model);

		if(isset($_POST['Country']))
		{
			$model->attributes=$_POST['Country'];
			if($model->save())
				$this->controller->redirect(array('createCountry'));
		}

		$this->controller->render('../country/updateAndAdminConfiguration',array(
			'model'=>$model,
		));
  }
}