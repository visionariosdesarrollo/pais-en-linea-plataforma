<?php
Yii::import ( 'application.extensions.class-upload-php.src.*' );
/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
class CreateAction extends CAction
{
	public function run()
  {
		$model=new City;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationCity($model);

		if(isset($_POST['City']))
		{
			$model->attributes=$_POST['City'];
		
			$image = $_FILES ['City'];
			if (isset ( $image ['name'] ['image'])  && !empty(  $image ['name'] ['image']) )
				$model->image = uniqid ( rand ( 0, 9999 ) );

			if($model->save()) {
				$dataFile = array (
						'name' => $image ['name'] ['image'],
						'type' => $image ['type'] ['image'],
						'tmp_name' => $image ['tmp_name'] ['image'],
						'error' => $image ['error'] ['image'],
						'size' => $image ['size'] ['image']
				);
				$newImage = new Upload ( $dataFile );
				$newImage->file_new_name_body = $model->image;
				$newImage->image_resize = true;
				$newImage->image_convert = CityAbstract::IMAGE_EXTENSION;
				$newImage->image_x = CityAbstract::IMAGE_WIDTH;
				$newImage->image_ratio_x = true;
				$newImage->Process ( dirname(dirname(dirname(dirname(dirname(dirname(dirname(dirname(realpath(__FILE__)))))))))	. CityAbstract::getImagePath ());
				if ($newImage->processed) {
					$model->image = $newImage->file_dst_name;
					$model->save(false);
					$newImage->Clean ();
				} else
					$newImage->error;
				
				$this->controller->refresh();
			}
		}

		$this->controller->render('../city/createAndAdminConfiguration',array(
			'model'=>$model,
		));
  }
}