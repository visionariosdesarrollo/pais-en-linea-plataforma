<?php
Yii::import ( 'application.extensions.class-upload-php.src.*' );
/**
 * Updates a particular model.
 * If update is successful, the browser will be redirected to the 'view' page.
 * @param integer $id the ID of the model to be updated
 */
class UpdateAction extends CAction
{
	public function run($id)
	{
		$model=$this->controller->loadModelCity($id);
		if(isset($_POST['City']))
		{
			$oldImageName = $model->image;
			$model->attributes = $_POST['City'];
			$newImage = false;
			$image = $_FILES ['City'];
			if (isset ( $image ['name'] ['image'])  && !empty(  $image ['name'] ['image']) ) {
				$model->image = uniqid ( $model->id );
				$newImage = true;
			} else 
				$model->image = $oldImageName;
			
			if($model->save()) {
				if ($newImage) {
					$dataFile = array (
						'name' => $image ['name'] ['image'],
						'type' => $image ['type'] ['image'],
						'tmp_name' => $image ['tmp_name'] ['image'],
						'error' => $image ['error'] ['image'],
						'size' => $image ['size'] ['image']
					);
					$newImage = new Upload ( $dataFile );
					$newImage->file_new_name_body = $model->image;
					$newImage->image_convert = CityAbstract::IMAGE_EXTENSION;
					$newImage->image_resize = true;
					$newImage->image_ratio = true;
					$newImage->image_x = CityAbstract::IMAGE_WIDTH;
					$newImage->image_y = CityAbstract::IMAGE_HEIGHT;
					$newImage->Process ( dirname(dirname(dirname(dirname(dirname(dirname(dirname(dirname(realpath(__FILE__)))))))))	. CityAbstract::getImagePath () );
					if ($newImage->processed) {
						$model->image = $newImage->file_dst_name;
						$model->save(false);
						$newImage->Clean ();
					} else {
						$newImage->error;
					}
				}
				$this->controller->redirect(array('createCity'));
			}
		}
		$this->controller->render('../city/updateAndAdminConfiguration',array(
			'model'=>$model,
		));
  }
}