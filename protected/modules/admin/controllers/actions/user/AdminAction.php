<?php
/**
 * Manages all models.
 */
class AdminAction extends CAction
{
	public function run()
  {
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->controller->render('admin',array(
	 		'model'=>$model,
	 	));
  }
}