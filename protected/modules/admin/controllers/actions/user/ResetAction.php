<?php
/**
 * Updates a particular model.
 * If update is successful, the browser will be redirected to the 'view' page.
 * @param integer $id the ID of the model to be updated
 */
class ResetAction extends CAction
{
	public function run($id)
  	{
		$model = $this->controller->loadModelUser($id);
		$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INIT;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationUser($model);

		$model->password = $model->hashPassword($model->document);
		$model->status = RedundantDataAbstract::STATUS_PENDIENTE;
		$model->save(false);
		$this->controller->redirect($this->controller->createUrl('user/update/',array('id' => $id)));
  	}
}