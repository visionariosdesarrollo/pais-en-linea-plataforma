<?php
Yii::import ( 'application.extensions.class-upload-php.src.*' );
/**
 * Updates a particular model.
 * If update is successful, the browser will be redirected to the 'view' page.
 * @param integer $id the ID of the model to be updated
 */
class UpdateAction extends CAction {
	public function run($id)
  	{
		$model = $this->controller->loadModelUser($id);
		$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INIT;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationUser($model);
		if(isset($_POST['User']))
		{
			$model->status == RedundantDataAbstract::STATUS_PENDIENTE ? $_POST['User']['status'] = $model->status : false;
			$_POST['User'] ['image']= $model->image;
			$model->attributes = $_POST['User'];
			$model->attributes = $this->controller->setLocation();
			$newImage = false;
			$image = $_FILES ['User'];
			if (isset ( $image ['name'] ['image'])  && !empty(  $image ['name'] ['image']) ) {
				$model->image = uniqid ( $model->id );
				$newImage = true;
			}
			if($model->validate())
			{
				$modalPasswordUserConfirm = $this->controller->validateModalPasswordUserConfirm() ? NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT : NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
				if($modalPasswordUserConfirm == NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT) {
					if($model->save(false)) {
						if ($newImage) {
							$dataFile = array (
								'name' => $image ['name'] ['image'],
								'type' => $image ['type'] ['image'],
								'tmp_name' => $image ['tmp_name'] ['image'],
								'error' => $image ['error'] ['image'],
								'size' => $image ['size'] ['image']
							);
							$newImage = new Upload ( $dataFile );
							$newImage->file_new_name_body = $model->image;
							$newImage->image_resize = true;
							$newImage->image_convert = UserAbstract::IMAGE_EXTENSION;
							$newImage->image_x = UserAbstract::IMAGE_WIDTH;
							$newImage->image_ratio_x = true;
							$newImage->Process ( dirname(dirname(dirname(dirname(dirname(dirname(dirname(realpath(__FILE__))))))))	. UserAbstract::getImagePath () );
							if ($newImage->processed) {
								$model->image = $newImage->file_dst_name;
								$model->save(false);
								$newImage->Clean ();
							} else {
								$newImage->error;
							}
						}
						$this->controller->redirect(array('admin'));
					}
				} else
					$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
			}
		}
		$this->controller->render('update',array(
				'model'=>$model,
				'modalPasswordUserConfirm'=>$modalPasswordUserConfirm,
		));
	}
}