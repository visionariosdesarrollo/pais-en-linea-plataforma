<?php
/**
 * Manages all models.
 */
class AdminAction extends CAction
{
	public function run()
	{
		$model=new Entity('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Entity']))
			$model->attributes=$_GET['Entity'];

		$this->controller->render('admin',array(
	 		'model'=>$model,
	 	));
	}
}