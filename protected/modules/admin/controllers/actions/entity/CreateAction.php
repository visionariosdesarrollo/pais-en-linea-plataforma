<?php
Yii::import ( 'application.extensions.class-upload-php.src.*' );
// echo Yii::getPathOfAlias( 'application.extensions.class-upload-php.src' );
// exit;

/**
 * Creates a new model.
 * If creation is successful, the browser will be redirected to the 'view' page.
 */
class CreateAction extends CAction {
	public function run() {
		$model = new Entity ();
		$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INIT;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationEvent($model);
		
		if (isset ( $_POST ['Entity'] )) {
			$model->attributes = $_POST ['Entity'];
			
			$user = Yii::app ()->user->getState ( 'user' );
			if($user->role === UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO || $user->role === UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES) {
				$model->cityId = $user->roleCityId;
			}
			
			$image = $_FILES ['Entity'];
			if (isset ( $image ['name'] ['image'])  && !empty(  $image ['name'] ['image']) )
				$model->image = uniqid ( rand ( 0, 9999 ) );
			
			if ($model->validate ()) {
				$modalPasswordUserConfirm = $this->controller->validateModalPasswordUserConfirm () ? NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT : NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
				if ($modalPasswordUserConfirm == NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT) {
					if ($model->save ( false )) {
						$dataFile = array (
								'name' => $image ['name'] ['image'],
								'type' => $image ['type'] ['image'],
								'tmp_name' => $image ['tmp_name'] ['image'],
								'error' => $image ['error'] ['image'],
								'size' => $image ['size'] ['image'] 
						);
						$newImage = new Upload ( $dataFile );
						$newImage->file_new_name_body = $model->image;
						$newImage->image_resize = true;
						$newImage->image_convert = EntityAbstract::IMAGE_EXTENSION;
						$newImage->image_x = EntityAbstract::IMAGE_WIDTH;
						$newImage->image_ratio_x = true;
						$newImage->Process ( dirname(dirname(dirname(dirname(dirname(dirname(dirname(realpath(__FILE__))))))))	. EntityAbstract::getImagePath () );
						if ($newImage->processed) {
							$model->image = $newImage->file_dst_name;
							$model->save(false);
							$newImage->Clean ();
						} else
							$newImage->error;
						
						$this->controller->redirect ( array (
								'admin' 
						) );
					}
				} else
					
					$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
			}
		} else {
			$model->status = RedundantDataAbstract::STATUS_ACTIVO;
		}
		
		$this->controller->render ( 'create', array (
				'model' => $model,
				'modalPasswordUserConfirm' => $modalPasswordUserConfirm 
		) );
	}
}