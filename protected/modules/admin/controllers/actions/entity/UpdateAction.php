<?php
Yii::import ( 'application.extensions.class-upload-php.src.*' );

/**
 * Updates a particular model.
 * If update is successful, the browser will be redirected to the 'view' page.
 * @param integer $id the ID of the model to be updated
 */
class UpdateAction extends CAction
{
	public function run($id)
  	{
		$model=$this->controller->loadModelEntity($id);
		$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INIT;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationEvent($model);

		if(isset($_POST['Entity']))
		{
			$_POST['Entity'] ['image']= $model->image;
			$model->attributes=$_POST['Entity'];

			$newImage = false;
			$image = $_FILES ['Entity'];
			$imageName = $model->image;
			
			if (isset ( $image ['name'] ['image'])  && !empty(  $image ['name'] ['image']) ) {
				$model->image = uniqid ( rand ( 0, 9999 ) );
				$newImage = true;
			}
			
			if($model->validate())
			{
				$model->image = $imageName;
				$modalPasswordUserConfirm = $this->controller->validateModalPasswordUserConfirm() ? NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT : NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
				if($modalPasswordUserConfirm == NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT)
					if($model->save(false)) {
						if ($newImage) {
							$dataFile = array (
									'name' => $image ['name'] ['image'],
									'type' => $image ['type'] ['image'],
									'tmp_name' => $image ['tmp_name'] ['image'],
									'error' => $image ['error'] ['image'],
									'size' => $image ['size'] ['image']
							);
							
							$newImage = new Upload ( $dataFile );
							
							$newImage->file_new_name_body = $model->image;
							$newImage->image_resize = true;
							$newImage->image_convert = EntityAbstract::IMAGE_EXTENSION;
							$newImage->image_x = EntityAbstract::IMAGE_WIDTH;
							$newImage->image_ratio_x = true;
							$newImage->Process ( dirname(dirname(dirname(dirname(dirname(dirname(dirname(realpath(__FILE__))))))))	. EntityAbstract::getImagePath () );
							if ($newImage->processed) {
								$model->image = $newImage->file_dst_name;
								$model->save(false);
								$newImage->Clean ();
							} else {
								$newImage->error;
							}
						}
						
						$this->controller->redirect(array('admin'));
					}
				else
					$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
			}
		}

		$this->controller->render('update',array(
			'model'=>$model,
			'modalPasswordUserConfirm'=>$modalPasswordUserConfirm,
		));
  }
}