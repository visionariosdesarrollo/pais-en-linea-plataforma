<?php
/**
 * Manages all models.
 */
class AdminAction extends CAction {
	public function run($status = null) {
		$model = new Release( 'search' );
		$model->unsetAttributes (); // clear any default values
		if (isset ( $_GET ['Release'] ))
			$model->attributes = $_GET ['Release'];

		if ($status)
			$model->status = $status;
			
		$this->controller->render ( 'admin', array (
				'model' => $model 
		) );
	}
}