<?php
/**
 * Updates a particular model.
 * If update is successful, the browser will be redirected to the 'view' page.
 * @param integer $id the ID of the model to be updated
 */
class UpdateAction extends CAction
{
	public function run($id)
  	{
		$model=$this->controller->loadModelRelease($id);
		$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INIT;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationRelease($model);

		if(isset($_POST['Release']))
		{
			$model->attributes=$_POST['Release'];
			$model->userId = Yii::app ()->user->getId ();
			
			if($model->validate())
			{
				$modalPasswordUserConfirm = $this->controller->validateModalPasswordUserConfirm() ? NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT : NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
				if($modalPasswordUserConfirm == NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT)
					if($model->save(false))
						$this->controller->redirect(array('admin'));
				else
					$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
			}				
		}

		$this->controller->render('update',array(
			'model'=>$model,
			'modalPasswordUserConfirm'=>$modalPasswordUserConfirm,				
		));
  	}
}