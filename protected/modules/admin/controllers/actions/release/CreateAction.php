<?php
/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
class CreateAction extends CAction
{
	public function run()
	{
		$model = new Release;
		$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INIT;
		if(isset($_POST['Release']))
		{
			$model->attributes = $_POST['Release'];
			$attributes = $model->attributes;
			$model->userId = Yii::app ()->user->getId ();

			if($model->validate())
			{
				$modalPasswordUserConfirm = $this->controller->validateModalPasswordUserConfirm() ? NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT : NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
				if($modalPasswordUserConfirm == NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT)
				{
					if($model->save(false))
						$this->controller->redirect(array('admin'));
				}else
					$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
			}
		}
		else{
			$locale = Yii::app()->locale;
			$startDateTime = strtotime('+' . (ReleaseAbstract::LIMIT_CREATION + 1) . ' hour', time());
			$endDateTime = $startDateTime;
			$dateFormat = $locale->getDateFormat();
			$model->releaseDateStart = $locale->getDateFormatter()->format($dateFormat, $startDateTime);
			$model->releaseDateEnd = $locale->getDateFormatter()->format($dateFormat, $endDateTime);
			$model->status = RedundantDataAbstract::STATUS_ACTIVO;
		}

		$this->controller->render('create',array(
			'model'=>$model,
			'modalPasswordUserConfirm'=>$modalPasswordUserConfirm,
		));
  }
}