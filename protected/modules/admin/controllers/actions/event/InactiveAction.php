<?php

/**
 * Inactive a particular model.
 * If inactivation is successful, the browser will be redirected to the 'admin' page.
 * @param integer $id the ID of the model to be deleted
 */
class InactiveAction extends CAction
{
	public function run($id)
	{
		if((int)$id)
		{
			if($model=$this->controller->loadModelEvent($id))
				$model->changeStatus(RedundantDataAbstract::STATUS_INACTIVO) ? 1 : 0; //$data['error']=$model->delete() ? 1 : 0;
		}
		$this->controller->redirect(array('admin'));
	}
}