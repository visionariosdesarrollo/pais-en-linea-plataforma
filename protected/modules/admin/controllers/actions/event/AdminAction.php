<?php
/**
 * Manages all models.
 */
class AdminAction extends CAction {
	public function run($status = null) {
		$model = new Event ( 'search' );
		$model->unsetAttributes (); // clear any default values
		if (isset ( $_GET ['Event'] ))
			$model->attributes = $_GET ['Event'];

		if ($status)
			$model->status = $status;
			
		$this->controller->render ( 'admin', array (
				'model' => $model 
		) );
	}
}