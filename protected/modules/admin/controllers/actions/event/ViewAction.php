<?php
/**
 * View a particular model.
 * If update is successful, the browser will be redirected to the 'view' page.
 * @param integer $id the ID of the model to be updated
 */
class ViewAction extends CAction
{
	public function run($id)
  	{
		$model = $this->controller->loadModelEvent($id);
		
		$this->controller->render('view',array(
			'model'=>$model,
		));
  	}
}