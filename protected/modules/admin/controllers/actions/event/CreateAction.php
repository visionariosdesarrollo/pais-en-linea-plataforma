<?php
/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
class CreateAction extends CAction
{
	public function run()
	{
		$model = new Event;
		$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INIT;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidationEvent($model);

		if(isset($_POST['Event']))
		{
			$model->attributes = $_POST['Event'];
			$user = Yii::app ()->user->getState ( 'user' );
			$model->entityId = $user->entityId;			

			$attributes = $model->attributes;
			
			if($model->validate())
			{
				
				$modalPasswordUserConfirm = $this->controller->validateModalPasswordUserConfirm() ? NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT : NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
				if($modalPasswordUserConfirm == NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_CORRECT)
				{
					$model->attributes = $attributes;
					if($model->save(false))
						$this->controller->redirect(array('admin'));
				}else
					$modalPasswordUserConfirm = NHCControllerAdminModule::MODAL_PASSWORD_USER_CONFIRM_INCORRECT;
			}
		}
		else{
			$locale = Yii::app()->locale;
			$startDateTime = strtotime('+' . (EventAbstract::LIMIT_CREATION + 1) . ' hour', time());
			$endDateTime = $startDateTime;
			$dateFormat = $locale->getDateFormat();
			$timeFormat = $locale->getTimeFormat ( 'short' ) . ' a';
			$model->eventDateStart = $locale->getDateFormatter()->format($dateFormat, $startDateTime);
			$model->eventTimeStart = date('g:m a', $startDateTime); //$model->eventTimeStart = $locale->getDateFormatter()->format($timeFormat, $startDateTime);
			$model->eventDateEnd = $locale->getDateFormatter()->format($dateFormat, $endDateTime);
			$model->eventTimeEnd = date('g:m a', strtotime('+1 hour', $endDateTime)); //$model->eventTimeEnd = $locale->getDateFormatter()->format($timeFormat, strtotime('+1 hour', $endDateTime));
			$model->status = RedundantDataAbstract::STATUS_ACTIVO;
		}

		$this->controller->render('create',array(
			'model'=>$model,
			'modalPasswordUserConfirm'=>$modalPasswordUserConfirm,
		));
  }
}