<?php

/**
 * Deletes a particular model.
 * If deletion is successful, the browser will be redirected to the 'admin' page.
 * @param integer $id the ID of the model to be deleted
 */
class DeleteAction extends CAction
{
	public function run($id)
	{
  		$data['data']=null;
  		$data['error']=false;
		if(Yii::app()->request->isPostRequest && (int)$id)
		{
			try {
				// we only allow deletion via POST request
				if($model=$this->controller->loadModelEvent($id))
					$data['error']=$model->changeStatus(RedundantDataAbstract::STATUS_ELIMINADO) ? 1 : 0; //$data['error']=$model->delete() ? 1 : 0;
			} catch (CDbException $e) {}

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		echo CJSON::encode($data);
		Yii::app()->end();
	}
}