<?php
Yii::import ( 'application.modules.admin.controllers.behaviors.event.*' );
class EventController extends NHCControllerAdminModule {
	private static $_usersEntity;
	public function init() {
		parent::init();
		self::$_usersEntity = User::model ()->getListUsersEditorsOfEntities ();
	}	
	public function behaviors() {
		return array (
			'EventBehavior' => array (
				'class' => 'EventBehavior' 
			) 
		);
	}
	public function actions() {
		return array (
				'view' => 'application.modules.admin.controllers.actions.event.ViewAction',
				'create' => 'application.modules.admin.controllers.actions.event.CreateAction',
				'update' => 'application.modules.admin.controllers.actions.event.UpdateAction',
				'delete' => 'application.modules.admin.controllers.actions.event.DeleteAction',
				'activate' => 'application.modules.admin.controllers.actions.event.ActivateAction',
				'inactive' => 'application.modules.admin.controllers.actions.event.InactiveAction',
				'cancel' => 'application.modules.admin.controllers.actions.event.CancelAction',
				'admin' => 'application.modules.admin.controllers.actions.event.AdminAction'
		);
	}
	
	public static function getUsersEntity($userId) {
		return isset(self::$_usersEntity[$userId]) ? self::$_usersEntity[$userId] : Yii::t('configuracion', '(No definido)');		
	}
	
	public static function checkAccessEventUpdate ( $controllerId, $model )  {
		//return ($model->status != RedundantDataAbstract::STATUS_INACTIVO && $model->status != RedundantDataAbstract::STATUS_PENDIENTE && $model->status != RedundantDataAbstract::STATUS_CANCELADO) && UserAbstract::checkAccess ( $controllerId, 'update' );
		return $model->status != RedundantDataAbstract::STATUS_CANCELADO && UserAbstract::checkAccess ( $controllerId, 'update' );
	}
	
	public static function checkAccessEventDelete ( $controllerId, $model )  {
		return $model->status != RedundantDataAbstract::STATUS_CANCELADO && UserAbstract::checkAccess ( $controllerId, 'delete' );
	}
	
	public static function creationUser ( $data, $row )  {
		return self::getUsersEntity($data->redundantDataOfEvent->creationUserId);
		//return $model->status != RedundantDataAbstract::STATUS_CANCELADO && UserAbstract::checkAccess ( $controllerId, 'delete' );
	}
}