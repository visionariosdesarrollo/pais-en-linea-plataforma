<?php
Yii::import ( 'application.modules.admin.controllers.behaviors.release.*' );
class ReleaseController extends NHCControllerAdminModule
{
	public function behaviors() {
		return array (
				'ReleaseBehavior' => array (
						'class' => 'ReleaseBehavior'
				)
		);
	}
	
	public function actions() {
		return array (
				'view' => 'application.modules.admin.controllers.actions.release.ViewAction',
				'create' => 'application.modules.admin.controllers.actions.release.CreateAction',
				'update' => 'application.modules.admin.controllers.actions.release.UpdateAction',
				'delete' => 'application.modules.admin.controllers.actions.release.DeleteAction',
				'admin' => 'application.modules.admin.controllers.actions.release.AdminAction'
		);
	}
}
