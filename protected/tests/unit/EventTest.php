<?php
class EventTest extends CDbTestCase
{
	/**
	 * @var array a list of fixtures that should be loaded before each test method executes.
	 * The array keys are fixture names, and the array values are either AR class names
	 * or table names. If table names, they must begin with a colon character (e.g. 'Post'
	 * means an AR class, while ':post' means a table name).
	 */	
	public $fixtures=array(
		'city'=>'City',
		'event'=>'Event',
	);
	
	/**
	 * @author dmontoya
	 * @group create 
	 * @group crud
	 * @small
	 * @test
	 */
	public function create()
	{
		$city=$this->city('City2');
		$this->assertTrue($city instanceof City);
		
		$name='Concierto Ricardo Arjona & Danza Hall';
		$event=new Event;
		$event->setAttributes(array(
			'name'=>$name,
			'cityId'=>$city->id,
			'type'=>'Musica,Danza,Cultura',
			'eventDate'=>new CDbExpression(sprintf('DATE_ADD(CURRENT_TIMESTAMP, INTERVAL %d HOUR)',rand(0, 12))),
			'site'=>'Estadio Palogrande',
			'body'=>'Uno de los artistas más influyentes del mundo Ricardo Arjona y el grupo Danza Hall de la ciudad.'
		));
		$this->assertTrue($event->save(false));

		//RECOVER
		$recoverEvent=Event::model()->findByPk($event->id);
		$this->assertTrue($recoverEvent instanceof Event);
		$this->assertEquals($name,$recoverEvent->name); 
	}
	
	/**
	 * @author dmontoya
	 * @group reade 
	 * @group crud
	 * @small
	 * @test
	 */
	public function read()
	{	
		$name='Concierto Santiagro Cruz';
		$event=$this->event('Event1');
		$this->assertTrue($event instanceof Event);
		$this->assertEquals($name,$event->name);
	}

	/**
	 * @author dmontoya
	 * @group update 
	 * @group crud
	 * @small
	 * @test
	 */
	public function update()
	{
		$name='Santigo Cruz en escena';
		$event=$this->event('Event1');
		$event->name=$name;
		$this->assertTrue($event->save(false));
		
		//RECOVER
		$recoverEvent=Event::model()->findByPk($event->id);
		$this->assertTrue($recoverEvent instanceof Event);
		$this->assertEquals($name,$recoverEvent->name);
	}

	/**
	 * @author dmontoya
	 * @group delete 
	 * @group crud
	 * @small
	 * @test
	 */
	public function delete()
	{
		$event=$this->event('Event4');
		$eventId=$event->id;
		$this->assertTrue($event->delete());

		//RECOVER
		$recoverEvent=Event::model()->findByPk($eventId);
		$this->assertEquals(NULL,$recoverEvent);
	}
}