<?php
class UserTest extends CDbTestCase
{
	/**
	 * @var array a list of fixtures that should be loaded before each test method executes.
	 * The array keys are fixture names, and the array values are either AR class names
	 * or table names. If table names, they must begin with a colon character (e.g. 'Post'
	 * means an AR class, while ':post' means a table name).
	 */	
	public $fixtures=array(
		'city'=>'City',
		'user'=>'User',
	);
	
	/**
	 * @author dmontoya
	 * @group create 
	 * @group crud
	 * @small
	 * @test
	 */
	public function create()
	{
		$city=$this->city('City2');
		$this->assertTrue($city instanceof City);
		
		$name="Álvaro Cárdenas";
		$user=new User;
		$user->setAttributes(array(
			'name'=>$name,
			'documentType'=>UserAbstract::DOCUMENT_TYPE_CC,				
			'document'=>7890681,
			'cityId'=>$city->id,
			'email'=>'alvarocardenas@qltar.co',
			'username'=>'acardenas',
			'password'=>$user->hashPassword('acardenasQL')
		));
		$this->assertTrue($user->save(false));

		//RECOVER
		$recoverUser=User::model()->findByPk($user->id);
		$this->assertTrue($recoverUser instanceof User);
		$this->assertEquals($name,$recoverUser->name); 
	}
	
	/**
	 * @author dmontoya
	 * @group reade 
	 * @group crud
	 * @small
	 * @test
	 */
	public function read()
	{	
		$name='Diego Montoya';
		$user=$this->user('User1');
		$this->assertTrue($user instanceof User);
		$this->assertEquals($name,$user->name);
	}

	/**
	 * @author dmontoya
	 * @group update 
	 * @group crud
	 * @small
	 * @test
	 */
	public function update()
	{
		$name='Diego Montoya Blandon';
		$user=$this->user('User1');
		$user->name=$name;
		$this->assertTrue($user->save(false));
		
		//RECOVER
		$recoverUser=User::model()->findByPk($user->id);
		$this->assertTrue($recoverUser instanceof User);
		$this->assertEquals($name,$recoverUser->name);
	}

	/**
	 * @author dmontoya
	 * @group delete 
	 * @group crud
	 * @small
	 * @test
	 */
	public function delete()
	{
		$user=$this->user('User1');
		$userId=$user->id;
		$this->assertTrue($user->delete());

		//RECOVER
		$recoverUser=User::model()->findByPk($userId);
		$this->assertEquals(NULL,$recoverUser);
	}
	
	/**
	 * @author dmontoya
	 * @group login
	 * @group logout
	 * @group session
	 * @small
	 * @test
	 */
	public function loginAndLogout()
	{
		Yii::app()->user->logout();
		Yii::app()->user->clearStates();

		$login=new LoginForm;
		$login->setAttributes(array(
			'username'=>'dmontoya',
			'password'=>'dmontoyaNH',
			'rememberMe'=>false
		));
		
		// Login
		$this->assertTrue($login->validate() && $login->login());
		$this->assertTrue(Yii::app()->user->getState('user') instanceof User);
		$this->assertEquals(Yii::app()->user->getState('user')->id,Yii::app()->user->getId());
		
		// Logout
		Yii::app()->user->logout();
		$_SESSION=array();
		$this->assertEquals(NULL,Yii::app()->user->getState('user'));
	}
}