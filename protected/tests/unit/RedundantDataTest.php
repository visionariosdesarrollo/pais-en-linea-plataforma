<?php
class RedundantDataTest extends CDbTestCase
{
	/**
	 * @var array a list of fixtures that should be loaded before each test method executes.
	 * The array keys are fixture names, and the array values are either AR class names
	 * or table names. If table names, they must begin with a colon character (e.g. 'Post'
	 * means an AR class, while ':post' means a table name).
	 */	
	public $fixtures=array(
		'country'=>'Country',
		'user'=>'User',
		'redundantData'=>'RedundantData',
	);
	
	/**
	 * @author dmontoya
	 * @group create 
	 * @group crud
	 * @small
	 * @test
	 */
	public function create()
	{
		$country=$this->country('Country2');
		$user=$this->user('User2');
		$this->assertTrue($country instanceof Country);

		$redundantData=new RedundantData;
		$redundantData->setAttributes(array(
			'table'=>$country->tableName(),
			'tableId'=>$country->id,
			'status'=>RedundantData::STATUS_ACTIVO,
			'order'=>rand(0,5),
			'creationUserId'=>$user->id,
		));
		$this->assertTrue($redundantData->save(false));

		//RECOVER
		$recoverRedundantData=RedundantData::model()->findByPk($redundantData->id);
		$this->assertTrue($recoverRedundantData instanceof RedundantData);
		$this->assertEquals($country->tableName(),$recoverRedundantData->table) 
			&& $this->assertEquals($country->id,$recoverRedundantData->tableId);
	}
	
	/**
	 * @author dmontoya
	 * @group reade 
	 * @group crud
	 * @small
	 * @test
	 */
	public function read()
	{	
		$country=$this->country('Country1');
		$this->assertTrue($country instanceof Country);

		//RECOVER
		$recoverRedundantData=RedundantData::model()->findByAttributes(array('table'=>$country->tableName(),'tableId'=>$country->id));
		$this->assertTrue($recoverRedundantData instanceof RedundantData);
		$this->assertEquals($country->tableName(),$recoverRedundantData->table)
			&& $this->assertEquals($country->id,$recoverRedundantData->tableId);		
	}

	/**
	 * @author dmontoya
	 * @group update 
	 * @group crud
	 * @small
	 * @test
	 */
	public function update()
	{
		$redundantData=$this->redundantData('RedundantData2');
		$redundantData->status=RedundantData::STATUS_ACTIVO;
		$this->assertTrue($redundantData->save(false));

		//RECOVER
		$recoverRedundantData=RedundantData::model()->findByPk($redundantData->id);
		$this->assertTrue($recoverRedundantData instanceof RedundantData);
		$this->assertEquals(RedundantData::STATUS_ACTIVO,$recoverRedundantData->status);
	}

	/**
	 * @author dmontoya
	 * @group delete 
	 * @group crud
	 * @small
	 * @test
	 */
	public function delete()
	{
		$redundantData=$this->redundantData('RedundantData3');
		$redundantDataId=$redundantData->id;
		$this->assertTrue($redundantData->delete());

		//RECOVER
		$recoverRedundantData=RedundantData::model()->findByPk($redundantDataId);
		$this->assertEquals(NULL,$recoverRedundantData);
	}
}