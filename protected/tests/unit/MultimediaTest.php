<?php
class MultimediaTest extends CDbTestCase
{
	/**
	 * @var array a list of fixtures that should be loaded before each test method executes.
	 * The array keys are fixture names, and the array values are either AR class names
	 * or table names. If table names, they must begin with a colon character (e.g. 'Post'
	 * means an AR class, while ':post' means a table name).
	 */	
	public $fixtures=array(
		'event'=>'Event',
		'multimedia'=>'Multimedia',
	);
	
	/**
	 * @author dmontoya
	 * @group create 
	 * @group crud
	 * @small
	 * @test
	 */
	public function create()
	{
		$event=$this->event('Event2');
		$this->assertTrue($event instanceof Event);
		
		$title='Danza Arabe';
		$multimedia=new Multimedia;
		$multimedia->setAttributes(array(
			'table'=>Event::model()->tableName(),
			'tableId'=>$event->id,
			'type'=>MultimediaAbstract::TYPE_SONIDO,
			'mimeType'=>CFileHelper::getMimeType('danza.mp3'),
			'title'=>$title,
			'path'=>'danza.mp3',
			'description'=>'La danza del vientre es una danza que combina elementos tradicionales de Oriente Medio junto con otros del Norte de África. ',
		));
		$this->assertTrue($multimedia->save(false));

		//RECOVER
		$recoverMultimedia=Multimedia::model()->findByPk($multimedia->id);
		$this->assertTrue($recoverMultimedia instanceof Multimedia);
		$this->assertEquals($title,$recoverMultimedia->title); 
	}
	
	/**
	 * @author dmontoya
	 * @group reade 
	 * @group crud
	 * @small
	 * @test
	 */
	public function read()
	{	
		$title='Walt Disney';
		$multimedia=$this->multimedia('Multimedia1');
		$this->assertTrue($multimedia instanceof Multimedia);
		$this->assertEquals($title,$multimedia->title);
	}

	/**
	 * @author dmontoya
	 * @group update 
	 * @group crud
	 * @small
	 * @test
	 */
	public function update()
	{
		$title='Walt Disney Pictures';
		$multimedia=$this->multimedia('Multimedia1');
		$multimedia->title=$title;
		$this->assertTrue($multimedia->save(false));
		
		//RECOVER
		$recoverMultimedia=Multimedia::model()->findByPk($multimedia->id);
		$this->assertTrue($recoverMultimedia instanceof Multimedia);
		$this->assertEquals($title,$recoverMultimedia->title);
	}

	/**
	 * @author dmontoya
	 * @group delete 
	 * @group crud
	 * @small
	 * @test
	 */
	public function delete()
	{
		$multimedia=$this->multimedia('Multimedia3');
		$multimediaId=$multimedia->id;
		$this->assertTrue($multimedia->delete());

		//RECOVER
		$recoverMultimedia=Multimedia::model()->findByPk($multimediaId);
		$this->assertEquals(NULL,$recoverMultimedia);
	}
}