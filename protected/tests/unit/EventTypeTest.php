<?php
class EventTypeTest extends CDbTestCase
{
	/**
	 * @var array a list of fixtures that should be loaded before each test method executes.
	 * The array keys are fixture names, and the array values are either AR class names
	 * or table names. If table names, they must begin with a colon character (e.g. 'Post'
	 * means an AR class, while ':post' means a table name).
	 */	
	public $fixtures=array(
		'eventType'=>'EventType',
	);
	
	/**
	 * @author dmontoya
	 * @group create 
	 * @group crud
	 * @small
	 * @test
	 */
	public function create()
	{
		$name='Moda';
		$eventType=new EventType;
		$eventType->setAttributes(array(
			'name'=>$name,
		));
		$this->assertTrue($eventType->save(false));

		//RECOVER
		$recoverEventType=EventType::model()->findByPk($eventType->id);
		$this->assertTrue($recoverEventType instanceof EventType);
		$this->assertEquals($name,$recoverEventType->name);
	}
	
	/**
	 * @author dmontoya
	 * @group read 
	 * @group crud
	 * @small
	 * @test
	 */
	public function read()
	{	
		$name='Musica';
		$eventType=$this->eventType('EventType1');
		$this->assertTrue($eventType instanceof EventType);
		$this->assertEquals($name,$eventType->name);
	}

	/**
	 * @author dmontoya
	 * @group update 
	 * @group crud
	 * @small
	 * @test
	 */
	public function update()
	{
		$name='Danza';
		$eventType=$this->eventType('EventType2');
		$eventType->name=$name;
		$this->assertTrue($eventType->save(false));

		//RECOVER
		$recoverEventType=EventType::model()->findByPk($eventType->id);
		$this->assertTrue($recoverEventType instanceof EventType);
		$this->assertEquals($name,$recoverEventType->name);
	}
		
	/**
	 * @author dmontoya
	 * @group delete 
	 * @group crud
	 * @small
	 * @test
	 */
	public function delete()
	{
		$eventType=$this->eventType('EventType3');
		$eventTypeId=$eventType->id;
		$this->assertTrue($eventType->delete());

		//RECOVER
		$recoverEventType=EventType::model()->findByPk($eventTypeId);
		$this->assertEquals(NULL,$recoverEventType);
	}
}