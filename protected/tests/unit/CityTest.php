<?php
class CityTest extends CDbTestCase
{
	/**
	 * @var array a list of fixtures that should be loaded before each test method executes.
	 * The array keys are fixture names, and the array values are either AR class names
	 * or table names. If table names, they must begin with a colon character (e.g. 'Post'
	 * means an AR class, while ':post' means a table name).
	 */	
	public $fixtures=array(
		'department'=>'Department',
		'city'=>'City',			
	);
	
	/**
	 * @author dmontoya
	 * @group create 
	 * @group crud
	 * @small
	 * @test
	 */
	public function create()
	{
		$department=$this->department('Department2');
		$this->assertTrue($department instanceof Department);
		
		$name='Villamaria';
		$city=new City;
		$city->setAttributes(array(
			'departmentId'=>$department->id,
			'name'=>$name
		));
		$this->assertTrue($city->save(false));

		//RECOVER
		$recoverCity=City::model()->findByPk($city->id);
		$this->assertTrue($recoverCity instanceof City);
		$this->assertEquals($name,$recoverCity->name);
	}
	
	/**
	 * @author dmontoya
	 * @group read 
	 * @group crud
	 * @small
	 * @test
	 */
	public function read()
	{	
		$name='Manizales';
		$city=$this->city('City1');
		$this->assertTrue($city instanceof City);
		$this->assertEquals($name,$city->name);
	}

	/**
	 * @author dmontoya
	 * @group update 
	 * @group crud
	 * @small
	 * @test
	 */
	public function update()
	{
		$name='Manzanares';
		$city=$this->city('City2');
		$city->name=$name;
		$this->assertTrue($city->save(false));

		//RECOVER
		$recoverCity=City::model()->findByPk($city->id);
		$this->assertTrue($recoverCity instanceof City);
		$this->assertEquals($name,$recoverCity->name);
	}
	
	/**
	 * @author dmontoya
	 * @group delete 
	 * @group crud
	 * @small
	 * @test
	 */
	public function delete()
	{
		$city=$this->city('City4');
		$cityId=$city->id;
		$this->assertTrue($city->delete());

		//RECOVER
		$recoverCity=City::model()->findByPk($cityId);
		$this->assertEquals(NULL,$recoverCity);
	}
}