<?php
class CountryTest extends CDbTestCase
{
	/**
	 * @var array a list of fixtures that should be loaded before each test method executes.
	 * The array keys are fixture names, and the array values are either AR class names
	 * or table names. If table names, they must begin with a colon character (e.g. 'Post'
	 * means an AR class, while ':post' means a table name).
	 */	
	public $fixtures=array(
		'country'=>'Country',
	);
	
	/**
	 * @author dmontoya
	 * @group create 
	 * @group crud
	 * @small
	 * @test
	 */
	public function create()
	{
		$name='España';
		$country=new Country;
		$country->setAttributes(array(
			'name'=>$name,
			'code'=>'ES',
		));
		$this->assertTrue($country->save(false));

		//RECOVER
		$recoverCountry=Country::model()->findByPk($country->id);
		$this->assertTrue($recoverCountry instanceof Country);
		$this->assertEquals($name,$recoverCountry->name);
	}
	
	/**
	 * @author dmontoya
	 * @group read 
	 * @group crud
	 * @small
	 * @test
	 */
	public function read()
	{	
		$name='Colombia';
		$country=$this->country('Country1');
		$this->assertTrue($country instanceof Country);
		$this->assertEquals($name,$country->name);
	}

	/**
	 * @author dmontoya
	 * @group update 
	 * @group crud
	 * @small
	 * @test
	 */
	public function update()
	{
		$code='CO';
		$country=$this->country('Country1');
		$country->code=$code;
		$this->assertTrue($country->save(false));

		//RECOVER
		$recoverCountry=Country::model()->findByPk($country->id);
		$this->assertTrue($recoverCountry instanceof Country);
		$this->assertEquals($code,$recoverCountry->code);
	}
		
	/**
	 * @author dmontoya
	 * @group delete 
	 * @group crud
	 * @small
	 * @test
	 */
	public function delete()
	{
		$country=$this->country('Country3');
		$countryId=$country->id;
		$this->assertTrue($country->delete());

		//RECOVER
		$recoverCountry=Country::model()->findByPk($countryId);
		$this->assertEquals(NULL,$recoverCountry);
	}
}