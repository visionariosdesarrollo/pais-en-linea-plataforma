<?php
class DepartmentTest extends CDbTestCase
{
	/**
	 * @var array a list of fixtures that should be loaded before each test method executes.
	 * The array keys are fixture names, and the array values are either AR class names
	 * or table names. If table names, they must begin with a colon character (e.g. 'Post'
	 * means an AR class, while ':post' means a table name).
	 */	
	public $fixtures=array(
		'country'=>'Country',
		'department'=>'Department',
	);
	
	/**
	 * @author dmontoya
	 * @group create 
	 * @group crud
	 * @small
	 * @test
	 */
	public function create()
	{
		$country=$this->country('Country1');
		$this->assertTrue($country instanceof Country);
		
		$name='Cundinamarca';
		$department=new Department;
		$department->setAttributes(array(
			'countryId'=>$country->id,
			'name'=>$name
		));
		$this->assertTrue($department->save(false));

		//RECOVER
		$recoverDepartment=Department::model()->findByPk($department->id);
		$this->assertTrue($recoverDepartment instanceof Department);
		$this->assertEquals($name,$recoverDepartment->name);
	}
	
	/**
	 * @author dmontoya
	 * @group read 
	 * @group crud
	 * @small
	 * @test
	 */
	public function read()
	{	
		$name='Risaralda';
		$department=$this->department('Department1');
		$this->assertTrue($department instanceof Department);
		$this->assertEquals($name,$department->name);
	}

	/**
	 * @author dmontoya
	 * @group update 
	 * @group crud
	 * @small
	 * @test
	 */
	public function update()
	{
		$name='Dosquebradas';
		$department=$this->department('Department1');
		$department->name=$name;
		$this->assertTrue($department->save(false));

		//RECOVER
		$recoverDepartment=Department::model()->findByPk($department->id);
		$this->assertTrue($recoverDepartment instanceof Department);
		$this->assertEquals($name,$recoverDepartment->name);
	}
	
	/**
	 * @author dmontoya
	 * @group delete 
	 * @group crud
	 * @small
	 * @test
	 */
	public function delete()
	{
		$department=$this->department('Department1');
		$departmentId=$department->id;
		$this->assertTrue($department->delete());

		//RECOVER
		$recoverDepartment=Department::model()->findByPk($departmentId);
		$this->assertEquals(NULL,$recoverDepartment);
	}
}