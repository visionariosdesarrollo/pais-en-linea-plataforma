<?php
class AuditTest extends CDbTestCase
{
	/**
	 * @var array a list of fixtures that should be loaded before each test method executes.
	 * The array keys are fixture names, and the array values are either AR class names
	 * or table names. If table names, they must begin with a colon character (e.g. 'Post'
	 * means an AR class, while ':post' means a table name).
	 */	
	public $fixtures=array(
		'user'=>'User',
		'audit'=>'Audit',
	);
	
	/**
	 * @author dmontoya
	 * @group create 
	 * @group crud
	 * @small
	 * @test
	 */
	public function create()
	{
		$user=$this->user('User1');
		$this->assertTrue($user instanceof User);
		
		$audit=new Audit;
		$audit->setAttributes(array(
			'username'=>$user->username,
			'accessType'=>AuditAbstract::getAccessType(),
			'ip'=>Yii::app()->request->getUserHostAddress(),
			'table'=>Country::model()->tableName(),
			'tableId'=>1,
			'type'=>AuditAbstract::TYPE_INSERCION,			
			'data'=>serialize(array(
				'id'=>1,
				'name'=>'Colombia',
				'code'=>'OC'
			))
		));
		$this->assertTrue($audit->save(false));

		//RECOVER
		$recoverAudit=Audit::model()->findByPk($audit->id);
		$this->assertTrue($recoverAudit instanceof Audit);
		$this->assertEquals($user->username,$recoverAudit->username);
	}
	
	/**
	 * @author dmontoya
	 * @group reade 
	 * @group crud
	 * @small
	 * @test
	 */
	public function read()
	{	
		$user=$this->user('User1');
		$this->assertTrue($user instanceof User);
		
		$audit=$this->audit('Audit1');
		$this->assertTrue($audit instanceof Audit);
		$this->assertEquals($user->username,$audit->username);
	}

	/**
	 * @author dmontoya
	 * @group update 
	 * @group crud
	 * @small
	 * @test
	 */
	public function update()
	{
		$user=$this->user('User2');
		$this->assertTrue($user instanceof User);
		
		$audit=$this->audit('Audit1');
		$audit->username=$user->username;
		$this->assertTrue($audit->save(false));
		
		//RECOVER
		$recoverAudit=Audit::model()->findByPk($audit->id);
		$this->assertTrue($recoverAudit instanceof Audit);
		$this->assertEquals($user->username,$recoverAudit->username);
	}

	/**
	 * @author dmontoya
	 * @group delete 
	 * @group crud
	 * @small
	 * @test
	 */
	public function delete()
	{
		$audit=$this->audit('Audit1');
		$auditId=$audit->id;
		$this->assertTrue($audit->delete());

		//RECOVER
		$recoverAudit=Audit::model()->findByPk($auditId);
		$this->assertEquals(NULL,$recoverAudit);
	}
}