<?php
return array (
		'Audit1'=>array(
			'username'=>'dmontoya',
			'accessType'=>AuditAbstract::getAccessType(),
			'ip'=>Yii::app()->request->getUserHostAddress(),
			'table'=>Event::model()->tableName(),
			'tableId'=>2,
			'type'=>AuditAbstract::TYPE_INSERCION,			
			'data'=>serialize(array(
				'id'=>2,
				'name'=>'Danzas',
				'cityId'=>3,
				'type'=>'Musica,Cultura,Arte',
				'eventDate'=>'2015-05-19 15:12:14',
				'site'=>'Parque central',
				'body'=>'La danza es la acci�n o manera de bailar. Se trata de la ejecuci�n de movimientos al ritmo de la m�sica que permite expresar sentimientos y emociones.'
			)),
		),
		'Audit2'=>array(
				'username'=>'mipajara',
				'accessType'=>AuditAbstract::getAccessType(),
				'ip'=>Yii::app()->request->getUserHostAddress(),
				'table'=>User::model()->tableName(),
				'tableId'=>3,
				'type'=>AuditAbstract::TYPE_ELIMINACION,
				'data'=>serialize(array(
					'id'=>3,
					'name'=>'Miller Ramirez',
					'documentType'=>UserAbstract::DOCUMENT_TYPE_NIT,				
					'document'=>8647421,
					'cityId'=>1,
					'email'=>'telecomunicaciones@netha.com.co',
					'username'=>'millergiga',
					'password'=>User::model()->hashPassword('millerNH'),
					'dataLastAccess'=>'2015-05-21 16:01:40'
				)),
		),
		'Audit3'=>array(
				'username'=>'dmontoya',
				'accessType'=>AuditAbstract::getAccessType(),
				'ip'=>Yii::app()->request->getUserHostAddress(),
				'table'=>Country::model()->tableName(),
				'tableId'=>1,
				'type'=>AuditAbstract::TYPE_ACTUALIZACION,
				'data'=>serialize(array(
					'name'=>'Colombia',
					'code'=>'OC' 
				)),
		),
);