<?php
return array(
		'Multimedia1'=>array(
			'table'=>Event::model()->tableName(),
			'tableId'=>4,
			'type'=>MultimediaAbstract::TYPE_IMAGEN,
			'mimeType'=>CFileHelper::getMimeType('waltdisney.jpg'),
			'title'=>'Walt Disney',
			'path'=>'waltdisney.jpg',
			'description'=>'Tus vacaciones a tu manera: elige entre m�s de 20 Hoteles Walt Disney World Resort, personaliza tus boletos para parques y reserva.',	
		),
		'Multimedia2'=>array(
			'table'=>User::model()->tableName(),
			'tableId'=>2,
			'type'=>MultimediaAbstract::TYPE_VIDEO,
			'mimeType'=>CFileHelper::getMimeType('salsa_choke.avi'),
			'title'=>'Salsa choke',
			'path'=>'salsa_choke.avi',
			'description'=>'La salsa choke es un g�nero musical, de tipo urbano que naci� en las costas del pacifico colombiano, inspirada en la salsa tradicional a�adi�ndole el son urbano y sonidos de origen afro, lo cual la hace un ritmo �pegajoso� y perfecto para amenizar fiestas y celebraciones.',
		),
		'Multimedia3'=>array(
			'table'=>Event::model()->tableName(),
			'tableId'=>3,
			'type'=>MultimediaAbstract::TYPE_ENLACE,
			'title'=>'NetHa Soluciones IT',
			'path'=>'http://www.netha.com.co',
			'description'=>'NetHa Soluciones IT, empresa creada en al a�o 2010 con un equipo de personas altamente capacitado, con experiencia y orientaci�n investigativa en el mejoramiento y creaci�n de nuevos productos y servicios en el sector de las tecnolog�as de la informaci�n y las comunicaciones.',
		)
);