<?php
return array(
		'Event1'=>array(
			'name'=>'Concierto Santiagro Cruz',
			'cityId'=>1,
			'type'=>'Musica',
			'eventDate'=>new CDbExpression(sprintf('DATE_ADD(CURRENT_TIMESTAMP, INTERVAL %d DAY)',rand(0, 5))),
			'site'=>'Plaza de ferias',
			'body'=>'Uno de los artistas más influyentes del país Santiago Cruz por primera vez en la ciudad.'
		),
		'Event2'=>array(
			'name'=>'Danzas',
			'cityId'=>3,
			'type'=>'Musica,Cultura,Arte',
			'eventDate'=>new CDbExpression(sprintf('DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -%d DAY)',rand(0, 5))),
			'site'=>'Parque central',
			'body'=>'La danza es la acción o manera de bailar. Se trata de la ejecución de movimientos al ritmo de la música que permite expresar sentimientos y emociones.'
		),
		'Event3'=>array(
			'name'=>'Cultura India',
			'cityId'=>2,
			'type'=>'Cultura',
			'eventDate'=>new CDbExpression(sprintf('DATE_ADD(CURRENT_TIMESTAMP, INTERVAL %d DAY)',rand(0, 5))),
			'site'=>'Parque de la cultura',
			'body'=>'Guía sobre historia, lengua y cultura en India, incluyendo frases útiles, consejos sobre costumbres locales e información sobre religión y cultura.'
		),
		'Event4'=>array(
			'name'=>'Campeonato de tejo',
			'cityId'=>1,
			'type'=>'Cultura,Deporte',
			'eventDate'=>new CDbExpression(sprintf('DATE_ADD(CURRENT_TIMESTAMP, INTERVAL %d DAY)',rand(0, 5))),
			'site'=>'Federacion Bonaerense de Tejo',
			'body'=>'Campeonato provincuial de Tejo. 1° Torneo de Tejo en la Ciudad de San Nicolás de los Arroyos'
		),		
);