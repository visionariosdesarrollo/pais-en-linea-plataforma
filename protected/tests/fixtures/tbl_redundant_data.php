<?php
return array(
		'RedundantData1'=>array(
			'table'=>Country::model()->tableName(),
			'tableId'=>1,
			'status'=>RedundantData::STATUS_ACTIVO,
			'order'=>rand(0,5),
			'creationUserId'=>1,
			'modifiedUserId'=>2,
			'creationDate'=>new CDbExpression(sprintf('DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -%d DAY)',rand(0, 5))),
			'modifiedDate'=>new CDbExpression(sprintf('DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -%d DAY)',rand(0, 31))),
		),
		'RedundantData2'=>array(
			'table'=>Department::model()->tableName(),
			'tableId'=>2,
			'status'=>RedundantData::STATUS_INACTIVO,
			'order'=>rand(0,5),
			'creationUserId'=>1,
		),
		'RedundantData3'=>array(
			'table'=>City::model()->tableName(),
			'tableId'=>2,
			'status'=>RedundantData::STATUS_ACTIVO,
			'order'=>rand(0,5),
			'creationUserId'=>1,
			'modifiedUserId'=>1,
			'creationDate'=>new CDbExpression('CURRENT_TIMESTAMP'),
			'modifiedDate'=>new CDbExpression(sprintf('DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -%d DAY)',rand(0, 31))),
		)
);