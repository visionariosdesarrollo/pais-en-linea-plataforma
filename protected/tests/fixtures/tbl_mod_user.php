<?php
return array(
		'User1'=>array(
			'name'=>'Diego Montoya',
			'documentType'=>UserAbstract::DOCUMENT_TYPE_CC,				
			'document'=>9682761,
			'cityId'=>1,
			'email'=>'desarrollo@netha.com.co',
			'username'=>'dmontoya',
			'password'=>User::model()->hashPassword('dmontoyaNH'),
			'dataLastAccess'=>new CDbExpression('CURRENT_TIMESTAMP')
		),
		'User2'=>array(
			'name'=>'Michael Parra',
			'documentType'=>UserAbstract::DOCUMENT_TYPE_CC,				
			'document'=>6547329,
			'cityId'=>1,
			'email'=>'gerencia@netha.com.co',
			'username'=>'mipajara',
			'password'=>User::model()->hashPassword('mipajaraNH'),
		),
		'User3'=>array(
			'name'=>'Miller Ramirez',
			'documentType'=>UserAbstract::DOCUMENT_TYPE_NIT,				
			'document'=>8647421,
			'cityId'=>1,
			'email'=>'telecomunicaciones@netha.com.co',
			'username'=>'millergiga',
			'password'=>User::model()->hashPassword('millerNH'),
			'dataLastAccess'=>new CDbExpression('CURRENT_TIMESTAMP')
		),
);