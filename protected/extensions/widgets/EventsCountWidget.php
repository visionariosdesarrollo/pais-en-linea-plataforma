<?php
class EventsCountWidget extends CWidget {
	const COLOR_DEFAULT = "default";
	const COLOR_YELLOW = "yellow";
	const COLOR_YELLOW_TWO = "yellow-two";
	const COLOR_GREEN = "green";
	const COLOR_RED = "red";
	const COLOR_RED_TWO = "red-two";
	const COLOR_BLUE = "blue";
	public $color = self::COLOR_YELLOW;
	public $icon = BsHtml::GLYPHICON_THUMBS_UP;
	private $count = 0;
	public $status;
	public $url = '#';
	public $label;

	private $template = <<<EOF
<div class="panel panel-{COLOR}">
	<div class="panel-heading">
		<div class="row">
			<div class="col-xs-3 col-lg-4">
				{ICON}
			</div>
            <div class="col-lg-8 text-right">
            	<div class="huge">{COUNT}</div>
                <div>{STATUS}!</div>
			</div>
		</div>
	</div>
	<a href="{URL}">
    	<div class="panel-footer">
        	<span class="pull-left">{LABEL}</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
		</div>
	</a>
</div>
EOF;
	public function init() {
		parent::init ();

		if (empty ( $this->label ))
			$this->label = Yii::t ( "system", 'Ver' );

		$this->count ();
		$this->render (null);
	}
	private function count() {
		$criteria = new CDbCriteria ();
		$user = Yii::app ()->user->getState ( 'user' );
		switch ($user->role) {
			case UserAbstract::USUARIO_FINAl : // Redactor
				$criteria->compare ( 'redundantDataOfEvent.creationUserId', $user->id );
				break;
			case UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES : // Secretaria
				$criteria->compare ( 'entityEvent.cityId', $user->roleCityId );
				break;
			case UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS : // Secreatrio de despacho
				$criteria->compare ( 'entityId', $user->entityId );
				break;
			case UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO : // Alcalde
				$criteria->with = 'entityEvent';
				$criteria->compare ( 'entityEvent.cityId', $user->roleCityId );
				break;
			case UserAbstract::ADMINISTRADOR_GOBERNADOR : // Gobernador
				$criteria->with = array (
						'entityEvent' => array (
								'with' => array (
										'cityOfEntity'
								)
						)
				);
				$criteria->compare ( 'cityOfEntity.departmentId', $user->roleDepartmentId );
				break;
			case UserAbstract::ADMINISTRADOR_PRESIDENTE : // Presidente
				$criteria->with = array (
						'entityEvent' => array (
								'with' => array (
										'cityOfEntity' => array (
												'with' => array (
														'departmentOfCity'
												)
										)
								)
						)
				);
				$criteria->compare ( 'departmentOfCity.countryId', Yii::app ()->user->getState ( 'user' )->roleCountryId );
				break;
			case UserAbstract::ADMINISTRADOR_SUPER_USUARIO : // Root
				break;
		}

		if ($this->status) {
			switch ($this->status) {
				case RedundantDataAbstract::STATUS_CUMPLIDO : // El d�a de finalizaci�n es igual o menor que el d�a actual y la hora de finalizaci�n es menor que la hora actual.
					$criteriaAux = new CDbCriteria();
					$criteriaAux->addCondition ( new CDbExpression('CONCAT(DATE_FORMAT(eventDateEnd, "%Y-%m-%d")," ",DATE_FORMAT(eventTimeEnd, "%H:%i")) < DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")'));
					//$criteriaAux->addCondition ( new CDbExpression('DATE_FORMAT(NOW(), "%H:%i") > DATE_FORMAT(eventTimeEnd, "%H:%i")'));

					$status = array (
							RedundantDataAbstract::STATUS_INACTIVO,
							RedundantDataAbstract::STATUS_ACTIVO,
							RedundantDataAbstract::STATUS_CUMPLIDO,
							RedundantDataAbstract::STATUS_CANCELADO,
							RedundantDataAbstract::STATUS_PENDIENTE
					);

					/*
					if($user->role != UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS)
						$status[] = RedundantDataAbstract::STATUS_PENDIENTE;
					*/

					$criteriaAux->addInCondition ( 'redundantDataOfEvent.status', $status);
					$criteria->mergeWith($criteriaAux);
					$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
					break;
				case RedundantDataAbstract::STATUS_ACTIVO : // Puede ser pendiente "Pendiente por revisar" o activa. El d�a de finalizaci�n debe de ser menor o igual que el d�a actual y la hora de finalizaci�n debe ser menor que la hora actual.
					$criteriaAux = new CDbCriteria();
					$criteriaAux->addCondition ( new CDbExpression('CONCAT(DATE_FORMAT(eventDateEnd, "%Y-%m-%d")," ",DATE_FORMAT(eventTimeEnd, "%H:%i")) > DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")'));

					$status = array (
							RedundantDataAbstract::STATUS_INACTIVO,
							RedundantDataAbstract::STATUS_ACTIVO,
							RedundantDataAbstract::STATUS_CUMPLIDO,
							RedundantDataAbstract::STATUS_CANCELADO,
					);

					if($user->role != UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS)
						$status[] = RedundantDataAbstract::STATUS_PENDIENTE;

					$criteriaAux->addInCondition ( 'redundantDataOfEvent.status', $status);
					$criteria->mergeWith($criteriaAux);
					$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
					break;
				case RedundantDataAbstract::STATUS_ELIMINADO:
					$criteriaAux = new CDbCriteria();
					$criteria->compare ( 'redundantDataOfEvent.status', RedundantDataAbstract::STATUS_ELIMINADO);
					$criteria->mergeWith($criteriaAux);
					break;
				default :
					$criteria->compare ( 'redundantDataOfEvent.status', $this->status);
					$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
			}
		} else
			$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));

		$this->count = Event::model ()->count ( $criteria );
	}
	public function render($view,$data=null,$return=false) {
		$template =  str_replace ( array (
				'{COLOR}',
				'{ICON}',
				'{COUNT}',
				'{STATUS}',
				'{URL}',
				'{LABEL}'
		), array (
				$this->color,
				BsHtml::icon ( $this->icon, array (
						'style' => 'font-size:70px;'
				) ),
				$this->count,
				$this->status ? RedundantDataAbstract::getStatusLabel ( $this->status ) : $this->status = Yii::t ( "system", 'Todos los estados' ),
				$this->url,
				$this->label
		), $this->template );

		echo $template;
	}
}
