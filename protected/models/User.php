<?php
/**
 * This is the model class for table "tbl_mod_user".
 *
 * The followings are the available columns in table 'tbl_mod_user':
 * @property integer $id
 * @property integer $entityId
 * @property string $role
 * @property string $name
 * @property string $documentType
 * @property string $document
 * @property string $dependence
 * @property string $position  
 * @property integer $cityId
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $dataLastAccess
 * @property string $documentDeliveryDate
 * @property string $image 
 * @property integer $creationUserId
 * @property integer $modifiedUserId 
 *
 * The followings are the available model relations:
 * @property City $city
 * @property Entity $entity
 * @property UserRole[] $userRoles 
 * @property RedundantData $redundantDataUser
 */
class User extends UserAbstract
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_mod_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, role, email, username, password, status, document', 'required'),
			array('entityId, roleCountryId, roleDepartmentId, roleCityId, creationUserId, modifiedUserId, countryId, departmentId, resetPassword', 'numerical', 'integerOnly'=>true),				
			array('name, email', 'length', 'max'=>50),
			array('document', 'length', 'max'=>7, 'max'=>20),
			array('dependence, position', 'length', 'max'=>250),
			array('username', 'length', 'min'=>5, 'max'=>20),
			array('documentDeliveryDate', 'length', 'min'=>10, 'max'=>10),
			array('roleCountryId, roleDepartmentId, roleCityId, entityId', 'requiredEntityOfRole'),
			array('roleCountryId', 'uniqueUserInRoleCountry'),
			array('roleDepartmentId', 'uniqueUserInRoleDepartment'),
			array('roleCityId', 'uniqueUserInRoleCity'),
			array('documentType', 'length', 'max'=>9),
			array('username', 'length', 'min'=>self::MINIMUM_USERNAME_LENGTH, 'max'=>20),
			array('password', 'length', 'min'=>self::MINIMUM_PASSWORD_LENGTH, 'max'=>60),
			array('username, document, email', 'unique'),
			array('email', 'email'),
			//array('image', 'required'),
			array('image', 'length', 'max'=>250),
			array('image', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true),
			array('dataLastAccess', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, role, name, documentType, document, email, username, password, dataLastAccess, creationUserId, modifiedUserId, creationDate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated dsadbelow.
		return array(
			'roleCountryOfUser' => array(self::BELONGS_TO, 'Country', 'roleCountryId'),
			'roleDepartmentOfUser' => array(self::BELONGS_TO, 'Department', 'roleDepartmentId'),
			'roleCityOfUser' => array(self::BELONGS_TO, 'City', 'roleCityId'),
			'entityOfUser' => array(self::BELONGS_TO, 'Entity', 'entityId'),
			'userRoles' => array(self::HAS_MANY, 'UserRole', 'userId'),
			'creationUserUser' => array(self::HAS_MANY, 'RedundantData', 'creationUserId'),
      		'modifiedUserUser' => array(self::HAS_MANY, 'RedundantData', 'modifiedUserId'),
			'redundantDataOfUser' => array(self::HAS_ONE, 'RedundantData', 'tableId',
				'on' => '`redundantDataOfUser`.`table` = :tableUser',
				'params' => array(':tableUser' => $this->tableName()),
			),
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Identificador',
			'entityId' => 'Entidad',
			'role' => 'Perfil',
			'roleCountryId' => 'País',
			'roleDepartmentId' => 'Departamento',
			'roleCityId' => 'Ciudad',				
			'name' => 'Nombre y apellidos de usuario',
			'documentType' => 'Tipo de documento',
			'document' => 'Documento de identidad',
			'dependence' => 'Dependencia donde trabaja',
			'position' => 'Cargo',
			'countryId' => 'País de nacimiento',
			'departmentId' => 'Departamento de nacimiento',
			'email' => 'Correo electrónico',
			'username' => 'Usuario',
			'password' => 'Contraseña',
			'dataLastAccess' => 'Fecha de último acceso',
			'image' => 'Imagen "jpg, gif, png"',
			'status' => 'Estado',
			'order' => 'Orden',
			'creationUser' => 'Usuario de creación',
			'creationUserId' => 'Usuario de creación',
			'modifiedUser' => 'Usuario de modificación',				
			'modifiedUserId' => 'Usuario de modificación',
			'creationDate' => 'Fecha de creación',
			'modifiedDate' => 'Fecha de modificación',			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$user = Yii::app ()->user->getState ( 'user' );
		$criteria=new CDbCriteria;
		
		switch ($user->role) {
			case self::ADMINISTRADOR_SUPER_USUARIO :
				break;			
			case self::ADMINISTRADOR_PRESIDENTE :
				$criteria->addNotInCondition ( 'role', array (
						self::ADMINISTRADOR_SUPER_USUARIO
				) );
				
				$criteria->compare('roleCountryId',$user->roleCountryId);
				break;
			case self::ADMINISTRADOR_GOBERNADOR :
				$criteria->addNotInCondition ( 'role', array (
						self::ADMINISTRADOR_SUPER_USUARIO,
						self::ADMINISTRADOR_PRESIDENTE
				) );
				$criteria->compare('roleCountryId',$user->roleCountryId);
				$criteria->compare('roleDepartmentId',$user->roleDepartmentId);
				break;
			case self::SERVIDOR_PUBLICO_NUMERO_UNO :
				$criteria->addNotInCondition ( 'role', array (
						self::ADMINISTRADOR_SUPER_USUARIO,
						self::ADMINISTRADOR_PRESIDENTE,
						self::ADMINISTRADOR_GOBERNADOR
				) );
				$criteria->compare('roleCountryId',$user->roleCountryId);
				$criteria->compare('roleDepartmentId',$user->roleDepartmentId);
				$criteria->compare('roleCityId',$user->roleCityId);
				break;
			case self::SERVIDOR_PUBLICO_NUMERO_DOS :
				$criteria->addNotInCondition ( 'role', array (
					self::ADMINISTRADOR_SUPER_USUARIO,
					self::ADMINISTRADOR_PRESIDENTE,
					self::ADMINISTRADOR_GOBERNADOR,
					self::USUARIO_USUARIO
				) );
				$criteria->compare('roleCountryId',$user->roleCountryId);
				$criteria->compare('roleDepartmentId',$user->roleDepartmentId);
				$criteria->compare('roleCityId',$user->roleCityId);
				$criteria->compare('entityId',$user->entityId);
				break;
			case self::SERVIDOR_PUBLICO_NUMERO_TRES :
				$criteria->addNotInCondition ( 'role', array (
					self::ADMINISTRADOR_SUPER_USUARIO,
					self::ADMINISTRADOR_PRESIDENTE,
					self::ADMINISTRADOR_GOBERNADOR,
					self::SERVIDOR_PUBLICO_NUMERO_UNO,
					self::SERVIDOR_PUBLICO_NUMERO_TRES,
					self::USUARIO_USUARIO
				) );
				$criteria->compare('roleCountryId',$user->roleCountryId);
				$criteria->compare('roleDepartmentId',$user->roleDepartmentId);
				$criteria->compare('roleCityId',$user->roleCityId);
				break;
			default: // REDACTOR
				$criteria->addNotInCondition ( 'role', array (
					self::ADMINISTRADOR_SUPER_USUARIO,
					self::ADMINISTRADOR_PRESIDENTE,
					self::ADMINISTRADOR_GOBERNADOR,
					self::SERVIDOR_PUBLICO_NUMERO_UNO,
					self::SERVIDOR_PUBLICO_NUMERO_DOS,
					elf::SERVIDOR_PUBLICO_NUMERO_TRES,
				) );
				$criteria->compare('roleCountryId',$user->roleCountryId);
				$criteria->compare('roleDepartmentId',$user->roleDepartmentId);
				$criteria->compare('roleCityId',$user->roleCityId);
				$criteria->compare('entityId',$user->entityId);				
		}
	
		$criteria->addNotInCondition ( '`t`.id', array (
				Yii::app ()->user->getId()
		) );
		
		$criteria->compare('`t`.id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('role',$this->role);
		$criteria->compare('documentType',$this->documentType,true);
		$criteria->compare('document',$this->document,true);
		$criteria->compare('roleCityId',$this->roleCityId);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('dataLastAccess',$this->dataLastAccess,true);
		$criteria->compare('redundantDataOfUser.status',$this->status,true);

		if($user->role != UserAbstract::ADMINISTRADOR_SUPER_USUARIO)
			$criteria->addNotInCondition('redundantDataOfUser.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
		
		if($this->creationDate)
		{
			$criteria->addCondition(new CDbExpression('DATE_FORMAT(redundantDataOfUser.creationDate,"%Y-%m-%d")=:creationDate'));
			$timestamp=CDateTimeParser::parse($this->creationDate,Yii::app()->locale->dateFormat);
			$criteria->params[':creationDate']=date('Y-m-d',$timestamp);
		}		

		return new CActiveDataProvider($this, array(				
			'criteria'=>$criteria,
			'sort'=>array(
				//'defaultOrder'=>'`t`.name ASC, redundantDataCountry.creationDate DESC',
				'attributes'=>array(
					'role' => array(
						'asc' => 'role ASC',
						'desc' => 'role DESC',
					),
					'roleCityId' => array(
						'asc' => 'roleCityId ASC',
						'desc' => 'roleCityId DESC',
					),						
					'name' => array(
						'asc' => 'name ASC',
						'desc' => 'name DESC',
					),
					'status' => array(
						'asc' => 'redundantDataOfUser.status ASC',
						'desc' => 'redundantDataOfUser.status DESC',
					),						
					'email' => array(
						'asc' => 'email ASC',
						'desc' => 'email DESC',
					),					
					'creationDate' => array(
						'asc' => 'redundantDataOfUser.creationDate ASC',
						'desc' => 'redundantDataOfUser.creationDate DESC',
					),
				),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}