<?php
/**
 * This is the model class for table "tbl_sys_config_country_department_city".
 *
 * The followings are the available columns in table 'tbl_sys_config_country_department_city':
 * @property integer $id
 * @property integer $departmentId
 * @property string $name
 * @property string $image 
 * @property integer $creationUserId
 * @property integer $modifiedUserId
 *
 * The followings are the available model relations:
 * @property Entity[] $entities
 * @property Event[] $events
 * @property User[] $users
 * @property Department $department
 * @property RedundantData $redundantDataCity 
 */
class City extends CityAbstract
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_sys_config_country_department_city';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('departmentId, name, status', 'required'),
			array('departmentId, creationUserId, modifiedUserId', 'numerical', 'integerOnly'=>true),				
			array('name', 'length', 'max'=>100),
			array('name', 'uniqueCity'),
			array('image', 'required', 'on'=>'insert'),
			array('image', 'length', 'max'=>250, 'on'=>'insert,update'),
			array('image', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, departmentId, name, creationUserId, modifiedUserId, creationDate', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.		
		return array(
			'entitiesOfCity' => array(self::HAS_MANY, 'Entity', 'cityId'),
			'usersOfCity' => array(self::HAS_MANY, 'User', 'cityId'),
			'usersOfRoleCity' => array(self::HAS_MANY, 'User', 'roleCityId'),
			'departmentOfCity' => array(self::BELONGS_TO, 'Department', 'departmentId'),
			'redundantDataOfCity' => array(self::HAS_ONE, 'RedundantData', 'tableId',
				'on' => '`redundantDataOfCity`.`table` = :tableCity',
				'params' => array(':tableCity' => $this->tableName()))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Identificador',
			'departmentId' => 'Departamento',
			'name' => 'Nombre de municipio',
			'image' => 'Imagen "jpg, gif, png"',
			'status' => 'Estado',
			'order' => 'Orden',
			'creationUser' => 'Usuario de creación',
			'creationUserId' => 'Usuario de creación',
			'modifiedUser' => 'Usuario de modificación',				
			'modifiedUserId' => 'Usuario de modificación',
			'creationDate' => 'Fecha de creación',
			'modifiedDate' => 'Fecha de modificación',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		
		$criteria=new CDbCriteria;
		$user = Yii::app ()->user->getState ( 'user' );
		$criteria->compare('`t`.id',$this->id);
		$criteria->compare('`departmentId`',$this->departmentId);
		$criteria->compare('`t`.name',$this->name,true);
		$criteria->compare('status',$this->status,true);
		
		if($this->creationDate)
		{
			$criteria->addCondition(new CDbExpression('DATE_FORMAT(`redundantDataOfCity`.`creationDate`,"%Y-%m-%d")=:creationDate'));
			$timestamp=CDateTimeParser::parse($this->creationDate,Yii::app()->locale->dateFormat);
			$criteria->params[':creationDate']=date('Y-m-d',$timestamp);
		}		
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'`t`.`name` ASC, `redundantDataOfCity`.`creationDate` DESC',
				'attributes'=>array(
					'departmentId' => array(
						'asc' => '`t`.`departmentId` ASC',
						'desc' => '`t`.`departmentId` DESC',
					),
					'name' => array(
						'asc' => '`t`.`name` ASC',
						'desc' => '`t`.`name` DESC',
					),
					'status' => array (
						'asc' => 'redundantDataOfEvent.status ASC',
						'desc' => 'redundantDataOfEvent.status DESC'
					),						
					'creationDate' => array(
						'asc' => '`redundantDataOfCity`.`creationDate` ASC',
						'desc' => '`redundantDataOfCity`.`creationDate` DESC',
					),
				),
			),				
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return City the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}