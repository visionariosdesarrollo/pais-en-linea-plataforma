<?php
/**
 * This is the model class for table "tbl_mod_event".
 *
 * The followings are the available columns in table 'tbl_mod_event':
 * @property integer $id
 * @property integer $entityId
 * @property string $name
 * @property string $type
 * @property string $eventDateStart
 * @property string $eventTimeStart
 * @property string $eventDateEnd
 * @property string $eventTimeEnd
 * @property string $site 
 * @property string $body
 *
 * The followings are the available model relations:
 * @property CountryDepartmentCity $city
 */
class Event extends EventAbstract {
	/**
	 *
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'tbl_mod_event';
	}
	
	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array (
				array ( 'name, entityId, eventDateStart, eventTimeStart, eventDateEnd, eventTimeEnd, site, body, status, type', 'required'),
				array ( 'entityId', 'numerical', 'integerOnly' => true),
				array ( 'name, site', 'length', 'max' => 250 ),
				array ( 'name', 'uniqueEvent' ),
				array ( 'type', 'type', 'type' => 'array','message' => Yii::t ( 'yii', '{attribute} is invalid.' ) ),
				/*
				array (
						'status',
						'rangeInStatus' 
				),
				*/
				array ( 'type', 'rangeInEventType' ),
				array ( 'eventDateStart', 'dateEvent' ),
				array ( 'eventDateEnd', 'dateEvent' ),
				array ( 'eventTimeStart', 'timeEvent' ),
				array ( 'eventTimeEnd', 'timeEvent' ),
				array ( 'eventDateEnd', 'dateTimeStartEventEqualOrMinorDateTimeEndEvent'),
				//array('eventDateEnd', 'compareDateTimeStartAndDateTimeEnd'),
				// The following rule is used by search().
				// @todo Please remove those attributes that should not be searched.
				array ( 'id, name, entityId, type, eventDateStart, eventTimeStart, eventDateEnd, eventTimeEnd, site, body, creationDate, creationUserId', 'safe', 'on' => 'search' ) 
		);
	}
	
	/**
	 *
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array (
				'entityEvent' => array (
						self::BELONGS_TO,
						'Entity',
						'entityId' 
				),
				'redundantDataOfEvent' => array (
						self::HAS_ONE, 'RedundantData', 'tableId',
						'on' => 'redundantDataOfEvent.table = :table',
						'params' => array (
								':table' => $this->tableName () 
						) 
				) 
		);
	}
	
	/**
	 *
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array (
				'id' => 'Identificador',
				'entityId' => 'Entidad',
				'name' => 'Nombre de actividad',
				'type' => 'Tipo de actividad',
				'eventDateStart' => 'Fecha inicio de actividad',
				'eventTimeStart' => 'Hora inicio de actividad',
				'eventDateEnd' => 'Fecha finalización de actividad',
				'eventTimeEnd' => 'Hora finalización de actividad',
				'site' => 'Lugar y dirección de actividad',
				'body' => 'Descripción de actividad',
				'status' => 'Estado',
				'order' => 'Orden',
				'creationUserId' => 'Usuario de creación',
				'modifiedUserId' => 'Usuario de modificación',
				'creationDate' => 'Fecha de creación',
				'modifiedDate' => 'Fecha de modificación' 
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 *         based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria = new CDbCriteria ();
		$user = Yii::app ()->user->getState ( 'user' );
		
		$criteria->compare ( '`t`.id', $this->id );
		$criteria->compare ( 't.name', $this->name, true );
		$criteria->compare ( 'entityId', $this->entityId );
		$criteria->compare ( 'type', $this->type, true );
		$criteria->compare ( 'eventTimeStart', $this->eventTimeStart, true );
		$criteria->compare ( 'eventTimeEnd', $this->eventTimeEnd, true );
		$criteria->compare ( 'site', $this->site, true );
		$criteria->compare ( 'body', $this->body, true );
		$criteria->compare ( 'redundantDataOfEvent.creationUserId', $this->creationUserId );

		if ($this->eventDateStart) {
			$criteria->addCondition ( new CDbExpression ( 'DATE_FORMAT(eventDateStart,"%Y-%m-%d")=:eventDateStart' ) );
			$timestamp = CDateTimeParser::parse ( $this->eventDateStart, Yii::app ()->locale->dateFormat );
			$criteria->params [':eventDateStart'] = date ( 'Y-m-d', $timestamp );
		}
		
		if ($this->eventDateEnd) {
			$criteria->addCondition ( new CDbExpression ( 'DATE_FORMAT(eventDateEnd,"%Y-%m-%d")=:eventDateEnd' ) );
			$timestamp = CDateTimeParser::parse ( $this->eventDateEnd, Yii::app ()->locale->dateFormat );
			$criteria->params [':eventDateEnd'] = date ( 'Y-m-d', $timestamp );
		}
		
		if ($this->creationDate) {
			$criteria->addCondition ( new CDbExpression ( 'DATE_FORMAT(redundantDataOfEvent.creationDate,"%Y-%m-%d")=:creationDate' ) );
			$timestamp = CDateTimeParser::parse ( $this->creationDate, Yii::app ()->locale->dateFormat );
			$criteria->params [':creationDate'] = date ( 'Y-m-d', $timestamp );
		}

		if ($this->status) {
			switch ($this->status) {
				case RedundantDataAbstract::STATUS_CUMPLIDO : // El d�a de finalizaci�n es igual o menor que el d�a actual y la hora de finalizaci�n es menor que la hora actual.
					$criteriaAux = new CDbCriteria();
					$criteriaAux->addCondition ( new CDbExpression('CONCAT(DATE_FORMAT(eventDateEnd, "%Y-%m-%d")," ",DATE_FORMAT(eventTimeEnd, "%H:%i")) < DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")'));
					//$criteriaAux->addCondition ( new CDbExpression('DATE_FORMAT(NOW(), "%H:%i") > DATE_FORMAT(eventTimeEnd, "%H:%i")'));
					
					$status = array (
							RedundantDataAbstract::STATUS_INACTIVO,
							RedundantDataAbstract::STATUS_ACTIVO,
							RedundantDataAbstract::STATUS_CUMPLIDO,
							RedundantDataAbstract::STATUS_CANCELADO,
							RedundantDataAbstract::STATUS_PENDIENTE
					);
					
					/*
					if($user->role != UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS)
						$status[] = RedundantDataAbstract::STATUS_PENDIENTE;
					*/
						
					$criteriaAux->addInCondition ( 'redundantDataOfEvent.status', $status);
					$criteria->mergeWith($criteriaAux);
					$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
					break;
				case RedundantDataAbstract::STATUS_ACTIVO : // Puede ser pendiente "Pendiente por revisar" o activa. El d�a de finalizaci�n debe de ser menor o igual que el d�a actual y la hora de finalizaci�n debe ser menor que la hora actual.
					$criteriaAux = new CDbCriteria();
					$criteriaAux->addCondition ( new CDbExpression('CONCAT(DATE_FORMAT(eventDateEnd, "%Y-%m-%d")," ",DATE_FORMAT(eventTimeEnd, "%H:%i")) > DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")'));
					
					$status = array (
							RedundantDataAbstract::STATUS_INACTIVO,
							RedundantDataAbstract::STATUS_ACTIVO,
							RedundantDataAbstract::STATUS_CUMPLIDO,
							RedundantDataAbstract::STATUS_CANCELADO,
					);
					
					if($user->role != UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS)
						$status[] = RedundantDataAbstract::STATUS_PENDIENTE;
					
					$criteriaAux->addInCondition ( 'redundantDataOfEvent.status', $status);
					$criteria->mergeWith($criteriaAux);
					$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
					break;
				case RedundantDataAbstract::STATUS_ELIMINADO:
					$criteriaAux = new CDbCriteria();
					$criteria->compare ( 'redundantDataOfEvent.status', RedundantDataAbstract::STATUS_ELIMINADO);
					$criteria->mergeWith($criteriaAux);
					break;					
				default :
					$criteria->compare ( 'redundantDataOfEvent.status', $this->status);
					$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
			}
		} else
			$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));

		return new CActiveDataProvider ( $this, array (
				'criteria' => $criteria,
				'sort' => array (
						'defaultOrder' => 't.name ASC, redundantDataOfEvent.creationDate DESC',
						'attributes' => array (
								'eventDateStart' => array (
										'asc' => 'eventDateStart ASC',
										'desc' => 'eventDateStart DESC' 
								),
								'name' => array (
										'asc' => 't.name ASC',
										'desc' => 't.name DESC' 
								),
								'status' => array (
										'asc' => 'redundantDataOfEvent.status ASC',
										'desc' => 'redundantDataOfEvent.status DESC'
								),
								'creationUserId' => array (
										'asc' => 'redundantDataOfEvent.creationUserId ASC',
										'desc' => 'redundantDataOfEvent.creationUserId DESC'
								),								
								'entityId' => array (
										'asc' => 'entityEvent.name ASC',
										'desc' => 'entityEvent.name DESC' 
								),
								'type' => array (
										'asc' => 'type ASC',
										'desc' => 'type DESC' 
								),
								'creationDate' => array (
										'asc' => 'redundantDataOfEvent.creationDate ASC',
										'desc' => 'redundantDataOfEvent.creationDate DESC' 
								) 
						) 
				) 
		) );
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className
	 *        	active record class name.
	 * @return Event the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model ( $className );
	}
}