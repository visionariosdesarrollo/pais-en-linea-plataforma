<?php

class Error{
	public function tableName(){
		return 'quickly_error';
	}

	public $attribute;
	public $message;

	public function Error($attribute, $message){
		$this->attribute = $attribute;
		$this->message = $message;
	}
	
	public static function model($className=__CLASS__)
	{
		return new $className(null, null);
	}
}