<?php
/**
 * This is the parent model class for table "tbl_mod_event".
 *
 */
abstract class EventAbstract extends NHCActiveRecord {
	const LIMIT_COMMENT = 8;
	const LIMIT_CREATION = 8;
	const LIMIT_PUBLICATION = 8;
	const STATUS_ADMINISTRATOR = 'administrator';
	public $eventTimeStampStart;
	public $eventTimeStampEnd;
	public $eventTimeStampEndLimit;
	public function behaviors() {
		return array (
				'RedundantDataBehavior' => array (
						'class' => 'RedundantDataBehavior' 
				),
				'EventAppBehavior' => array (
						'class' => 'EventAppBehavior' 
				) 
		);
	}
	public function defaultScope() {
		$criteria = new CDbCriteria ();
		if (! Yii::app ()->user->isGuest) {
			$user = Yii::app ()->user->getState ( 'user' );
			switch ($user->role) {
				case UserAbstract::USUARIO_FINAl : // Redactor
					$criteria->compare ( 'redundantDataOfEvent.creationUserId', $user->id );
					break;
				case UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS : // Secreatrio de despacho
					$criteria->compare ( 'entityId', $user->entityId );
					break;
				case UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES : // Secretaria
				case UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO : // Alcalde
					$criteria->with = array (
							'entityEvent' 
					);
					$criteria->compare ( 'entityEvent.cityId', $user->roleCityId );
					break;
				case UserAbstract::ADMINISTRADOR_GOBERNADOR : // Gobernador
					$criteria->with = array (
							'entityEvent' => array (
									'with' => array (
											'cityOfEntity' 
									) 
							) 
					);
					$criteria->compare ( 'cityOfEntity.departmentId', $user->roleDepartmentId );
					break;
				case UserAbstract::ADMINISTRADOR_PRESIDENTE : // Presidente
					$criteria->with = array (
							'entityEvent' => array (
									'with' => array (
											'cityOfEntity' => array (
													'with' => array (
															'departmentOfCity' 
													) 
											) 
									) 
							) 
					);
					$criteria->compare ( 'departmentOfCity.countryId', $user->roleCountryId );
					break;
				case UserAbstract::ADMINISTRADOR_SUPER_USUARIO : // Root
					break;
			}
		}
		
		if (is_array ( $criteria->with ) && count ( $criteria->with )) {
			array_push ( $criteria->with, 'redundantDataOfEvent' );
		} else
			$criteria->with = 'redundantDataOfEvent';
		return $criteria->toArray ();
	}
	private static function _status() {
		return array (
				RedundantDataAbstract::STATUS_INACTIVO => Yii::t ( 'redundantData', 'Inactivo' ),
				RedundantDataAbstract::STATUS_ACTIVO => Yii::t ( 'redundantData', 'Activo' ),
				RedundantDataAbstract::STATUS_CUMPLIDO => Yii::t ( 'redundantData', 'Cumplido' ),
				RedundantDataAbstract::STATUS_CANCELADO => Yii::t ( 'redundantData', 'Cancelado' ) 
		);
	}
	private static function _statusList() {
		return array (
				RedundantDataAbstract::STATUS_PENDIENTE => Yii::t ( 'redundantData', 'Pendiente' ),
				RedundantDataAbstract::STATUS_ACTIVO => Yii::t ( 'redundantData', 'Activo' ),
				RedundantDataAbstract::STATUS_CUMPLIDO => Yii::t ( 'redundantData', 'Cumplido' ),
				RedundantDataAbstract::STATUS_CANCELADO => Yii::t ( 'redundantData', 'Cancelado' ) 
		);
	}
	private static function _statusAdmin() {
		return array (
				RedundantDataAbstract::STATUS_INACTIVO => Yii::t ( 'redundantData', 'Inactivo' ),
				RedundantDataAbstract::STATUS_PENDIENTE => Yii::t ( 'redundantData', 'Pendiente' ),
				RedundantDataAbstract::STATUS_ACTIVO => Yii::t ( 'redundantData', 'Activo' ),
				RedundantDataAbstract::STATUS_CUMPLIDO => Yii::t ( 'redundantData', 'Cumplido' ),
				RedundantDataAbstract::STATUS_CANCELADO => Yii::t ( 'redundantData', 'Cancelado' ) 
		);
	}
	private static function _statusListSuperAdmin() {
		return array (
				RedundantDataAbstract::STATUS_INACTIVO => Yii::t ( 'redundantData', 'Inactivo' ),
				RedundantDataAbstract::STATUS_PENDIENTE => Yii::t ( 'redundantData', 'Pendiente' ),
				RedundantDataAbstract::STATUS_ACTIVO => Yii::t ( 'redundantData', 'Activo' ),
				RedundantDataAbstract::STATUS_CUMPLIDO => Yii::t ( 'redundantData', 'Cumplido' ),
				RedundantDataAbstract::STATUS_CANCELADO => Yii::t ( 'redundantData', 'Cancelado' ),
				RedundantDataAbstract::STATUS_ELIMINADO => Yii::t ( 'redundantData', 'Eliminado' ) 
		);
	}
	/**
	 *
	 * @return string label of status.
	 */
	public static function getStatuLabel($status, $type = null) {
		if ($type == self::STATUS_ADMINISTRATOR)
			$_status = self::_statusAdmin ();
		else
			$_status = self::_status ();
		
		$label = null;
		if (isset ( $_status [$status] ))
			$label = $_status [$status];
		return $label;
	}
	
	/**
	 *
	 * @return array status.
	 */
	public static function getStatus($type = null) {
		if ($type == self::STATUS_ADMINISTRATOR)
			$_status = self::_statusAdmin ();
		else
			$_status = self::_status ();
		
		return $_status;
	}
	public static function getStatusList() {
		$user = Yii::app ()->user->getState ( 'user' );
		if ($user->role == UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS)
			$_status = self::_statusList ();
		else if ($user->role == UserAbstract::ADMINISTRADOR_SUPER_USUARIO)
			$_status = self::_statusListSuperAdmin ();
		else
			$_status = self::_statusAdmin ();
		
		return $_status;
	}
	public function uniqueEvent($attribute, $params) {
		CValidator::createValidator ( 'unique', $this, $attribute, array (
				'criteria' => array (
						'condition' => '`t`.`entityId`=:entityId',
						'params' => array (
								':entityId' => Yii::app ()->user->getState ( 'user' )->entityId 
						) 
				) 
		) )->validate ( $this );
	}
	public function rangeInStatus($attribute, $params) {
		$status = RedundantDataAbstract::getStatus ();
		if (! in_array ( $this->attributes, $status ))
			$this->addError ( $attribute, Yii::t ( 'yii', '{attribute} is not in the list.', array (
					'{attribute}' => $this->getAttributeLabel ( $attribute ) 
			) ) );
	}
	public function rangeInEventType($attribute, $params) {
		if (is_array ( $this->$attribute ) && count ( $this->$attribute )) {
			$eventsTypes = EventType::model ()->getEventsTypesForName ();
			foreach ( $this->$attribute as $eventType )
				if (! in_array ( $eventType, $eventsTypes ))
					$this->addError ( $attribute, Yii::t ( 'yii', '{attribute} is not in the list.', array (
							'{attribute}' => $this->getAttributeLabel ( $attribute ) 
					) ) );
		}
	}
	public function dateEvent($attribute, $params) {
		$locale = Yii::app ()->locale;
		$dateFormat = $locale->getDateFormat ();
		CValidator::createValidator ( 'date', $this, $attribute, array (
				'format' => $dateFormat 
		) )->validate ( $this );
	}
	public function timeEvent($attribute, $params) {
		$locale = Yii::app ()->locale;
		$timeFormat = $locale->getTimeFormat ( 'short' ) . ' a';
		$timeArray = explode ( ':', $this->$attribute );
		if (is_array ( $timeArray ))
			$this->$attribute = ($timeArray [0] < 10 ? '0' . $timeArray [0] : $timeArray [0]) . ':' . $timeArray [1];
		CValidator::createValidator ( 'date', $this, $attribute, array (
				'format' => $timeFormat 
		) )->validate ( $this );
	}
	public function dateTimeStartEventEqualOrMinorDateTimeEndEvent($attribute, $params) {
		$locale = Yii::app ()->locale;
		$dateTimeFormat = $locale->getDateFormat () . ' ' . $locale->getTimeFormat ( 'short' ) . ' a';
		
		$dateTimeStart = $this->eventDateStart . ' ' . $this->eventTimeStart;
		$dateTimeEnd = $this->$attribute . ' ' . $this->eventTimeEnd;
		
		$startDateTime = strtotime ( '+' . EventAbstract::LIMIT_CREATION . ' hour', time () );
		$dateTimeParser = new CDateTimeParser ();
		$dateTimeStartParser = $dateTimeParser->parse ( $dateTimeStart, $dateTimeFormat );
		$dateTimeEndParser = $dateTimeParser->parse ( $dateTimeEnd, $dateTimeFormat );
		
		if ($dateTimeStartParser < $startDateTime) {
			$dateTimeFormatNoPaddingHour0To12 = str_replace ( 'HH', 'h', $dateTimeFormat );
			$this->addError ( 'eventDateStart', Yii::t ( 'yii', '{attribute} must be greater than "{compareValue}".', array (
					'{attribute}' => $this->getAttributeLabel ( 'eventDateStart' ) . '+' . $this->getAttributeLabel ( 'eventTimeStart' ),
					'{compareValue}' => $locale->getDateFormatter ()->format ( $dateTimeFormatNoPaddingHour0To12, $startDateTime ) 
			) ) );
		} else if ($dateTimeEndParser < $dateTimeStartParser)
			$this->addError ( $attribute, Yii::t ( 'yii', '{attribute} must be greater than "{compareValue}".', array (
					'{attribute}' => $this->getAttributeLabel ( $attribute ) . '+' . $this->getAttributeLabel ( 'eventTimeEnd' ),
					'{compareValue}' => $this->getAttributeLabel ( 'eventDateStart' ) . '+' . $this->getAttributeLabel ( 'eventTimeStart' ) 
			) ) );
		else if ($dateTimeEndParser == $dateTimeStartParser)
			$this->addError ( $attribute, Yii::t ( 'yii', '{attribute} must not be equal to "{compareValue}".', array (
					'{attribute}' => $this->getAttributeLabel ( $attribute ) . '+' . $this->getAttributeLabel ( 'eventTimeEnd' ),
					'{compareValue}' => $this->getAttributeLabel ( 'eventDateStart' ) . '+' . $this->getAttributeLabel ( 'eventTimeStart' ) 
			) ) );
	}
	public function getStatusPendingMinimum() {
		return array (
				RedundantDataAbstract::STATUS_PENDIENTE => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_PENDIENTE ) 
		);
	}
	public function getStatusMinimum() {
		return array (
				RedundantDataAbstract::STATUS_ACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_ACTIVO ),
				RedundantDataAbstract::STATUS_INACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_INACTIVO ) 
		);
	}
}