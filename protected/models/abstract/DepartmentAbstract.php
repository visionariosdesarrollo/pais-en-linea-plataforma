<?php
/**
 * This is the parent model class for table "tbl_sys_config_country_department".
 *
 */
abstract class DepartmentAbstract extends NHCActiveRecord {
	public static $counterOfRelations = '';
	public function behaviors() {
		return array (
				'RedundantDataBehavior' => array (
						'class' => 'RedundantDataBehavior' 
				) 
		);
	}
	public function defaultScope() {
		return array (
				'with' => array (
						'redundantDataOfDepartment' 
				) 
		);
	}
	
	/**
	 *
	 * @param CModel $attribute
	 *        	the attribute name to validate.
	 * @param array $params
	 *        	initial values to be applied to the validator properties.
	 * @return bool True if the department is unique.
	 */
	public function uniqueDepartment($attribute, $params) {
		CValidator::createValidator ( 'unique', $this, $attribute, array (
				'criteria' => array (
						'condition' => '`t`.`countryId`=:countryId',
						'params' => array (
								':countryId' => $this->countryId 
						) 
				) 
		) )->validate ( $this );
	}
	
	/**
	 * Departments grouped for country.
	 *
	 * @param
	 *        	string department status.
	 * @return array of departments.
	 */
	public function getDepartmentsOfCountry($status = RedundantDataAbstract::STATUS_ACTIVO, $countryId = null, $group = true) {
		$criteria = new CDbCriteria ();
		
		if (is_numeric ( $countryId )) {
			$criteria->compare ( 'countryId', $countryId );
		}
		if ($group) {
			$departments = CHtml::listData ( $this->resetScope ()->with ( array (
					'redundantDataOfDepartment' => array (
							'condition' => '`redundantDataOfDepartment`.`status`=:status',
							'params' => array (
									':status' => $status 
							) 
					),
					'countryOfDepartment' 
			) )->findAll ( $criteria ), 'id', 'name', 'countryOfDepartment.name' );
		} else {
			$departments = CHtml::listData ( $this->resetScope ()->with ( array (
					'redundantDataOfDepartment' => array (
							'condition' => '`redundantDataOfDepartment`.`status`=:status',
							'params' => array (
									':status' => $status 
							) 
					) 
			) )->findAll ( $criteria ), 'id', 'name' );
		}
		return $departments;
	}
	public function getStatus() {
		return array (
				RedundantDataAbstract::STATUS_ACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_ACTIVO ),
				RedundantDataAbstract::STATUS_INACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_INACTIVO ) 
		);
	}
}