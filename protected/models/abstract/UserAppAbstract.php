<?php
/**
 * This is the parent model class for table "tbl_mod_user".
 *
 * @property string $_type The type of user of the application.
 */
abstract class UserAppAbstract extends NHCActiveRecord {
	public function behaviors() {
		return array (
				'RedundantDataBehavior' => array (
						'class' => 'RedundantDataBehavior'
				)
		);
	}
}