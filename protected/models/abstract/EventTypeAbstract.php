<?php
/**
 * This is the parent model class for table "tbl_mod_event_type".
 *
 */
abstract class EventTypeAbstract extends NHCActiveRecord {
	public function behaviors() {
		return array (
				'RedundantDataBehavior' => array (
						'class' => 'RedundantDataBehavior' 
				) 
		);
	}
	public function defaultScope() {
		return array (
				'with' => array (
						'redundantDataOfEventType' 
				) 
		);
	}
	
	/**
	 *
	 * @param
	 *        	string events types status.
	 * @return array list of countries.
	 */
	public function getEventsTypesForName($status = RedundantDataAbstract::STATUS_ACTIVO) {
		return CHtml::listData ( $this->with ( array (
				'redundantDataOfEventType' => array (
						'condition' => '`redundantDataOfEventType`.`status`=:status',
						'params' => array (
								':status' => $status 
						) 
				) 
		) )->findAll (), 'name', 'name' );
	}
	public function getStatus() {
		return array (
				RedundantDataAbstract::STATUS_ACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_ACTIVO ),
				RedundantDataAbstract::STATUS_INACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_INACTIVO ) 
		);
	}
	public function getStatusList() {
		return array (
				RedundantDataAbstract::STATUS_ACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_ACTIVO ),
				RedundantDataAbstract::STATUS_INACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_INACTIVO ) 
		);
	}
}