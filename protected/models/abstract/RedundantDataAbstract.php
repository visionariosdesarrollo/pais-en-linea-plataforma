<?php
/**
 * This is the parent model class for table "tbl_redundant_data".
 *
 */
abstract class RedundantDataAbstract extends NHCActiveRecord {
	const STATUS_TODO = 'todo';
	const STATUS_INACTIVO = 'inactivo';
	const STATUS_PENDIENTE = 'pendiente';
	const STATUS_ACTIVO = 'activo';
	const STATUS_ELIMINADO = 'eliminado';
	const STATUS_CANCELADO = 'cancelado';
	const STATUS_CUMPLIDO = 'cumplido';
	const STATUS_AGENDADA = 'agendada';
	const STATUS_DESAGENDADA = 'desagendada';
	private static function _status() {
		return array (
				self::STATUS_INACTIVO => Yii::t ( 'redundantData', 'Inactivo' ),
				self::STATUS_PENDIENTE => Yii::t ( 'redundantData', 'Pendiente' ),
				self::STATUS_ACTIVO => Yii::t ( 'redundantData', 'Activo' ),
				self::STATUS_ELIMINADO => Yii::t ( 'redundantData', 'Eliminado' ),
				self::STATUS_CANCELADO => Yii::t ( 'redundantData', 'Cancelado' ),
				self::STATUS_CUMPLIDO => Yii::t ( 'redundantData', 'Cumplido' ) 
		);
	}
	
	/**
	 *
	 * @return string label of status.
	 */
	public static function getStatusLabel($status) {
		$_status = self::_status ();
		$label = null;
		if (isset ( $_status [$status] ))
			$label = $_status [$status];
		return $label;
	}
	
	/**
	 *
	 * @return array status.
	 */
	public static function getStatus() {
		return self::_status ();
	}
}