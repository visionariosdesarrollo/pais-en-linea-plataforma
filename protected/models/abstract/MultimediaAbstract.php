<?php
/**
 * This is the parent model class for table "tbl_mod_multimedia".
 *
 */
abstract class MultimediaAbstract extends CActiveRecord {
	const TYPE_DOCUMENTO = 'documento';
	const TYPE_ENLACE = 'enlace';
	const TYPE_IMAGEN = 'imagen';
	const TYPE_SONIDO = 'sonido';
	const TYPE_VIDEO = 'video';
	private $_types = array ();
	public function init() {
		parent::init ();
		$this->_types = array (
				self::TYPE_DOCUMENTO => Yii::t ( 'multimedia', 'Documento' ),
				self::TYPE_ENLACE => Yii::t ( 'multimedia', 'Enlace' ),
				self::TYPE_IMAGEN => Yii::t ( 'multimedia', 'Imagen' ),
				self::TYPE_SONIDO => Yii::t ( 'multimedia', 'Sonido' ),
				self::TYPE_VIDEO => Yii::t ( 'multimedia', 'Video' ) 
		);
	}
	
	/**
	 *
	 * @return string label of type.
	 */
	public function getTypeLabel($type) {
		$label = null;
		if (isset ( $this->_types [$type] ))
			$label = $this->_types [$type];
		return $label;
	}
	
	/**
	 *
	 * @return array types of documents.
	 */
	public function getDocumentTypes() {
		return $this->_types;
	}
}