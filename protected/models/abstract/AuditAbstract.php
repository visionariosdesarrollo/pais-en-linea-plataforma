<?php
/**
 * This is the parent model class for table "tbl_redundant_data".
 *
 */
abstract class AuditAbstract extends CActiveRecord {
	const ACCESS_TYPE_ESCRITORIO = 'escritorio';
	const ACCESS_TYPE_TABLETA = 'tableta';
	const ACCESS_TYPE_CELULAR = 'celular';
	const ACCESS_TYPE_SERVICIO_WEB = 'servicio web';
	const TYPE_INSERCION = 'insercion';
	const TYPE_ACTUALIZACION = 'actualizacion';
	const TYPE_ELIMINACION = 'eliminacion';
	const TYPE_CONSULTA = 'consulta';
	const TYPE_OTRO = 'otro';
	
	/*
	 * @return string label of access type.
	 */
	public static function getAccessType() {
		$detect = Yii::app ()->mobileDetect;
		if ($detect->isTablet ())
			return self::ACCESS_TYPE_TABLETA;
		else if ($detect->isMobile ())
			return self::ACCESS_TYPE_CELULAR;
		else
			return self::ACCESS_TYPE_ESCRITORIO;
	}
}