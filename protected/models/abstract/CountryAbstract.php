<?php
/**
 * This is the parent model class for table "tbl_sys_config_country".
 *
 */
abstract class CountryAbstract extends NHCActiveRecord {
	public function behaviors() {
		return array (
				'RedundantDataBehavior' => array (
						'class' => 'RedundantDataBehavior' 
				) 
		);
	}
	public function defaultScope() {
		return array (
				'with' => array (
						'redundantDataOfCountry' 
				) 
		);
	}
	
	/**
	 *
	 * @param CModel $attribute
	 *        	the attribute name to validate.
	 * @param array $params
	 *        	initial values to be applied to the validator properties.
	 * @return bool True if the country is unique.
	 */
	public function uniqueCountry($attribute, $params) {
		CValidator::createValidator ( 'unique', $this, $attribute, array (
				'criteria' => array (
						'condition' => '`t`.`name`=:name',
						'params' => array (
								':name' => $this->name 
						) 
				) 
		) )->validate ( $this );
	}
	
	/**
	 *
	 * @param
	 *        	string country status.
	 * @param
	 *        	boolean join code.
	 * @return array list of countries.
	 */
	public function getCountries($status = RedundantDataAbstract::STATUS_ACTIVO, $code = false) {
		$countries = $this->with ( array (
				'redundantDataOfCountry' => array (
						'condition' => '`redundantDataOfCountry`.`status`=:status',
						'params' => array (
								':status' => $status 
						) 
				) 
		) )->findAll ();
		
		if ($code)
			$countries = $this->_countriesAndCode ( $countries );
		
		return CHtml::listData ( $countries, 'id', 'name' );
	}
	
	/**
	 *
	 * @param
	 *        	array countries.
	 * @return array of countries and code.
	 */
	private function _countriesAndCode($countries) {
		$newCountries = array ();
		foreach ( $countries as $model ) {
			$model->name = sprintf ( '%s (%s)', $model->name, $model->code );
			array_push ( $newCountries, $model );
		}
		return $newCountries;
	}
	public function getStatus() {
		return array (
				RedundantDataAbstract::STATUS_ACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_ACTIVO ),
				RedundantDataAbstract::STATUS_INACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_INACTIVO ) 
		);
	}
}