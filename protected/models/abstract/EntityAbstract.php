<?php
/**
 * This is the parent model class for table "tbl_mod_entity".
 *
 */
abstract class EntityAbstract extends NHCActiveRecord {
	const IMAGE_WIDTH = 80;
	const IMAGE_EXTENSION = 'png';
	public static function getImagePath() {
		return Yii::app ()->request->baseUrl . '/public/images/entities/';
	}
	public function behaviors() {
		return array (
				'RedundantDataBehavior' => array (
						'class' => 'RedundantDataBehavior' 
				),
				'EntityBehavior' => array (
						'class' => 'EntityAppBehavior' 
				) 
		);
	}
	public function defaultScope() {
		return array (
				'with' => array (
						'redundantDataOfEntity' 
				) 
		);
	}
	
	/**
	 *
	 * @param CModel $attribute
	 *        	the attribute name to validate.
	 * @param array $params
	 *        	initial values to be applied to the validator properties.
	 * @return bool True if the entity is unique.
	 */
	public function uniqueEntity($attribute, $params) {
		CValidator::createValidator ( 'unique', $this, $attribute, array (
				'criteria' => array (
						'condition' => '`t`.`departmentId`=:departmentId',
						'params' => array (
								':departmentId' => $this->departmentId 
						) 
				) 
		) )->validate ( $this );
	}
	public function uniqueUserInEntity($attribute, $params) {
		CValidator::createValidator ( 'unique', $this, $attribute, array (
				'criteria' => array (
						'condition' => '`t`.`cityId`=:cityId',
						'params' => array (
								':cityId' => $this->cityId 
						) 
				) 
		) )->validate ( $this );
	}
	
	/**
	 * Entities.
	 *
	 * @param
	 *        	string entity status.
	 * @return array of entities.
	 */
	public function getEntities($status = RedundantDataAbstract::STATUS_ACTIVO, $cityId = null, $group = false) {
		$criteria = new CDbCriteria ();
		
		if (is_numeric ( $cityId ))
			$criteria->compare ( 'cityId', $cityId );
		
		if ($group) {
			$entities = CHtml::listData ( $this->with ( array (
					'redundantDataOfEntity' => array (
							'condition' => '`redundantDataOfEntity`.`status`=:status',
							'params' => array (
									':status' => $status 
							) 
					),
					'cityOfEntity' 
			) )->findAll ( $criteria ), 'id', 'name', 'city.name' );
		} else {
			$entities = CHtml::listData ( $this->with ( array (
					'redundantDataOfEntity' => array (
							'condition' => '`redundantDataOfEntity`.`status`=:status',
							'params' => array (
									':status' => $status 
							) 
					) 
			) )->findAll ( $criteria ), 'id', 'name' );
		}
		
		return $entities;
	}
	private static function _status() {
		return array (
				RedundantDataAbstract::STATUS_INACTIVO => Yii::t ( 'redundantData', 'Inactivo' ),
				RedundantDataAbstract::STATUS_ACTIVO => Yii::t ( 'redundantData', 'Activo' ) 
		);
	}
	
	/**
	 *
	 * @return string label of status.
	 */
	public static function getStatusLabel($status) {
		$_status = self::_status ();
		$label = null;
		if (isset ( $_status [$status] ))
			$label = $_status [$status];
		return $label;
	}
	
	/**
	 *
	 * @return array status.
	 */
	public static function getStatus() {
		return self::_status ();
	}
}