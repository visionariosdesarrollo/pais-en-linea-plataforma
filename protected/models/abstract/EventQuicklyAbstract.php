<?php
abstract class EventQuicklyAbstract extends NHCActiveRecord {
	public function behaviors() {
		return array (
				'RedundantDataBehavior' => array (
						'class' => 'RedundantDataBehavior' 
				) 
		);
	}
	public function defaultScope() {
		return array (
				'with' => array (
						'redundantDataOfEventQuickly' 
				) 
		);
	}
}