<?php
abstract class RoleAbstract extends CComponent {
	public static function getRolesDropDownListControlGroup() {
		$user = Yii::app ()->user->getState ( 'user' );
		if (! $role = $user->role)
			throw new CHttpException ( 404, Yii::t ( 'configuration', 'La página solicitada no existe.' ) );
		
		$roles = array (
				UserAbstract::ADMINISTRADOR_SUPER_USUARIO => array (
						UserAbstract::ADMINISTRADOR_PRESIDENTE => UserAbstract::getRoleLabel ( UserAbstract::ADMINISTRADOR_PRESIDENTE ),
						UserAbstract::ADMINISTRADOR_GOBERNADOR => UserAbstract::getRoleLabel ( UserAbstract::ADMINISTRADOR_GOBERNADOR ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
						UserAbstract::USUARIO_FINAl => UserAbstract::getRoleLabel ( UserAbstract::USUARIO_FINAl ) 
				),
				UserAbstract::ADMINISTRADOR_PRESIDENTE => array (
						UserAbstract::ADMINISTRADOR_GOBERNADOR => UserAbstract::getRoleLabel ( UserAbstract::ADMINISTRADOR_GOBERNADOR ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
						UserAbstract::USUARIO_FINAl => UserAbstract::getRoleLabel ( UserAbstract::USUARIO_FINAl ) 
				),
				UserAbstract::ADMINISTRADOR_GOBERNADOR => array (
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
						UserAbstract::USUARIO_FINAl => UserAbstract::getRoleLabel ( UserAbstract::USUARIO_FINAl ) 
				),
				UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => array ( // Alcalde
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS ),
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES ),
						UserAbstract::USUARIO_FINAl => UserAbstract::getRoleLabel ( UserAbstract::USUARIO_FINAl ) 
				),
				UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => array (), // Secretario de despacho
				UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES => array ( // Secretaria
						UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS ),
						UserAbstract::USUARIO_FINAl => UserAbstract::getRoleLabel ( UserAbstract::USUARIO_FINAl ) 
				),
				UserAbstract::USUARIO_FINAl => array (), // Redactor
				UserAbstract::USUARIO_USUARIO => array () 
		);
		
		return $roles [$user->role];
	}
	
	/*
	 * public static function getRolesSearchDropDownListControlGroup() {
	 * $user = Yii::app ()->user->getState ( 'user' );
	 * if (! $role = $user->role)
	 * throw new CHttpException ( 404, Yii::t ( 'configuration', 'La página solicitada no existe.' ) );
	 *
	 * $roles = array (
	 * UserAbstract::ADMINISTRADOR_SUPER_USUARIO => array (
	 * UserAbstract::ADMINISTRADOR_PRESIDENTE => UserAbstract::getRoleLabel ( UserAbstract::ADMINISTRADOR_PRESIDENTE ),
	 * UserAbstract::ADMINISTRADOR_GOBERNADOR => UserAbstract::getRoleLabel ( UserAbstract::ADMINISTRADOR_GOBERNADOR ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
	 * UserAbstract::USUARIO_FINAl => UserAbstract::getRoleLabel ( UserAbstract::USUARIO_FINAl )
	 * ),
	 * UserAbstract::ADMINISTRADOR_PRESIDENTE => array (
	 * UserAbstract::ADMINISTRADOR_GOBERNADOR => UserAbstract::getRoleLabel ( UserAbstract::ADMINISTRADOR_GOBERNADOR ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
	 * UserAbstract::USUARIO_FINAl => UserAbstract::getRoleLabel ( UserAbstract::USUARIO_FINAl )
	 * ),
	 * UserAbstract::ADMINISTRADOR_GOBERNADOR => array (
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
	 * UserAbstract::USUARIO_FINAl => UserAbstract::getRoleLabel ( UserAbstract::USUARIO_FINAl )
	 * ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => array ( // Alcalde
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO ),
	 * UserAbstract::USUARIO_FINAl => UserAbstract::getRoleLabel ( UserAbstract::USUARIO_FINAl )
	 * ),
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => array (), // Secretario de despacho
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES => array ( // Secretaria
	 * UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS => UserAbstract::getRoleLabel ( UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS ),
	 * UserAbstract::USUARIO_FINAl => UserAbstract::getRoleLabel ( UserAbstract::USUARIO_FINAl )
	 * ),
	 * UserAbstract::USUARIO_FINAl => array () // Redactor
	 * ,
	 * UserAbstract::USUARIO_USUARIO => array ()
	 * );
	 *
	 * return $roles [$user->role];
	 * }
	 */
}