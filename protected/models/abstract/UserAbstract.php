<?php
/**
 * This is the parent model class for table "tbl_mod_user".
 *
 * @property string $_type The type of user of the application.
 */
abstract class UserAbstract extends NHCActiveRecord {
	public $resetPassword;
	public $countryId;
	public $departmentId;
	public $dataLastAccessAux;
	const ADMINISTRADOR_SUPER_USUARIO = "root";
	const ADMINISTRADOR_PRESIDENTE = "presidente";
	const ADMINISTRADOR_GOBERNADOR = "gobernador";
	const SERVIDOR_PUBLICO_NUMERO_UNO = "alcalde";
	const SERVIDOR_PUBLICO_NUMERO_DOS = "secretario de despacho";
	const SERVIDOR_PUBLICO_NUMERO_TRES = "secretaria";
	const USUARIO_FINAl = "redactor";
	const USUARIO_USUARIO = "usuario";
	const MINIMUM_USERNAME_LENGTH = 5;
	const MINIMUM_PASSWORD_LENGTH = 5;
	const TYPE_PUBLIC = 'public';
	const TYPE_ENTITY = 'entity';
	const DOCUMENT_TYPE_CC = 'cc';
	const DOCUMENT_TYPE_PASAPORTE = 'pasaporte';
	const DOCUMENT_TYPE_NIT = 'nit';
	const PASSWORD_MASK = '0123456789';
	public static $counterOfRelations = 0;
	public $type = self::TYPE_PUBLIC;
	private $_documentTypes = array ();
	const IMAGE_WIDTH = 200;
	const IMAGE_EXTENSION = 'png';
	public static function getImagePath() {
		return Yii::app ()->request->baseUrl . '/public/images/users/';
	}
	public function behaviors() {
		return array (
				'RedundantDataBehavior' => array (
						'class' => 'RedundantDataBehavior'
				),				
				'UserBehavior' => array (
						'class' => 'UserAppBehavior' 
				) 
		);
	}
	private static function _roles() {
		return array (
				self::ADMINISTRADOR_SUPER_USUARIO => Yii::t ( 'redundantData', 'Super usuario' ),
				self::ADMINISTRADOR_PRESIDENTE => Yii::t ( 'redundantData', 'Presidente' ),
				self::ADMINISTRADOR_GOBERNADOR => Yii::t ( 'redundantData', 'Gobernador' ),
				self::SERVIDOR_PUBLICO_NUMERO_UNO => Yii::t ( 'redundantData', 'Alcalde' ),
				self::SERVIDOR_PUBLICO_NUMERO_DOS => Yii::t ( 'redundantData', 'Secretario de despacho' ),
				self::SERVIDOR_PUBLICO_NUMERO_TRES => Yii::t ( 'redundantData', 'Secretaria' ),
				self::USUARIO_FINAl => Yii::t ( 'redundantData', 'Redactor' ),
				self::USUARIO_USUARIO => Yii::t ( 'redundantData', 'Usuario' ) 
		);
	}
	private static function _rolesAdmin() {
		return array (
				self::ADMINISTRADOR_SUPER_USUARIO => Yii::t ( 'redundantData', 'Super usuario' ),
				self::ADMINISTRADOR_PRESIDENTE => Yii::t ( 'redundantData', 'Presidente' ),
				self::ADMINISTRADOR_GOBERNADOR => Yii::t ( 'redundantData', 'Gobernador' ),
				self::SERVIDOR_PUBLICO_NUMERO_UNO => Yii::t ( 'redundantData', 'Alcalde' ),
				self::SERVIDOR_PUBLICO_NUMERO_DOS => Yii::t ( 'redundantData', 'Secretario de despacho' ),
				self::SERVIDOR_PUBLICO_NUMERO_TRES => Yii::t ( 'redundantData', 'Secretaria' ),
				self::USUARIO_FINAl => Yii::t ( 'redundantData', 'Redactor' ),
				self::USUARIO_USUARIO => Yii::t ( 'redundantData', 'Usuario' ) 
		);
	}
	
	/**
	 *
	 * @return string label of roles.
	 */
	public static function getRoleLabel($role) {
		$_roles = self::_roles ();
		$label = null;
		if (isset ( $_roles [$role] ))
			$label = $_roles [$role];
		return $label;
	}
	
	/**
	 *
	 * @return array roles.
	 */
	public static function getRoles() {
		return self::_roles ();
	}
	private static $_access;
	public static function getAccess() {
		if (self::$_access == null) {
			self::$_access = require Yii::getPathOfAlias ( 'application.config' ) . '/roles.php';
		}
		
		$user = Yii::app ()->user->getState ( 'user' );
		return self::$_access [$user->role];
	}
	public function getStatus() {
		return array (
				RedundantDataAbstract::STATUS_ACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_ACTIVO ),
				RedundantDataAbstract::STATUS_PENDIENTE => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_PENDIENTE ),
				RedundantDataAbstract::STATUS_INACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_INACTIVO ) 
		);
	}
	public function getStatusPending() {
		return array (
				RedundantDataAbstract::STATUS_PENDIENTE => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_PENDIENTE ) 
		);
	}
	/**
	 *
	 * @return bool access.
	 */
	public static function checkAccess($controller, $action = null) {
		$user = Yii::app ()->user->getState ( 'user' );
		if (! $role = $user->role)
			throw new CHttpException ( 404, Yii::t ( 'configuration', 'La p�gina solicitada no existe.' ) );
		$access = false;
		$roles = self::getAccess ();
		if ($access = array_key_exists ( $controller, $roles )) {
			if ($action) {
				$exists = array_search ( $action, $roles [$controller] );
				$access = is_numeric ( $exists ) ? true : false;
			}
		}
		return $access;
	}
	
	/*
	 * public function defaultScope()
	 * {
	 * $alias=self::$counterOfRelations++;
	 * return array(
	 * 'with'=>array(
	 * 'redundantDataOfUser' => array(
	 * 'alias' => 'redundantDataOfUser'.$alias,
	 * 'on' => 'redundantDataOfUser'.$alias.'.table = :tableUser',
	 * 'params' => array(':tableUser' => $this->tableName()),
	 * ),
	 * 'entityOfUser',
	 * 'cityOfUser'
	 * ),
	 * );
	 * }
	 */
	public function defaultScope() {
		$criteria = new CDbCriteria ();
		$criteria->with = array (
				'redundantDataOfUser' 
		);
		
		return $criteria;
	}
	public function init() {
		parent::init ();
		$this->_documentTypes = array (
				self::DOCUMENT_TYPE_CC => Yii::t ( 'user', 'CC' ),
				self::DOCUMENT_TYPE_PASAPORTE => Yii::t ( 'user', 'Pasaporte' ),
				self::DOCUMENT_TYPE_NIT => Yii::t ( 'user', 'NIT' ) 
		);
	}
	
	/**
	 *
	 * @return string label of type document.
	 */
	public function getDocumentTypeLabel($type) {
		$label = null;
		if (isset ( $this->_documentTypes [$type] ))
			$label = $this->_documentTypes [$type];
		return $label;
	}
	
	/**
	 *
	 * @return array types of documents.
	 */
	public function getDocumentTypes() {
		return $this->_documentTypes;
	}
	
	/**
	 *
	 * @param string $password
	 *        	The password to be hashed.
	 * @return string the encrypted password data.
	 */
	public function hashPassword($password) {
		return CPasswordHelper::hashPassword ( $password );
	}
	
	/**
	 *
	 * @param string $password
	 *        	The password to verify. If password is empty or not a string, method will return false.
	 * @return bool True if the password matches the parent.
	 */
	public function validatePassword($password) {
		return CPasswordHelper::verifyPassword ( $password, $this->password );
	}
	public function requiredEntityOfRole($attribute, $params) {
		if (! $this->role)
			return CValidator::createValidator ( 'required', $this, 'role', array () )->validate ( $this );
		
		$roles = $this->selectsRole ( $this->role );
		
		switch ($this->role) {
			case self::ADMINISTRADOR_PRESIDENTE :
				if (in_array ( 'roleCountryId', $roles )) {
					CValidator::createValidator ( 'required', $this, 'roleCountryId', array () )->validate ( $this );
				}
				break;
			case self::ADMINISTRADOR_GOBERNADOR :
				if (in_array ( 'roleDepartmentId', $roles )) {
					CValidator::createValidator ( 'required', $this, 'roleDepartmentId', array () )->validate ( $this );
				}
				break;
			case self::SERVIDOR_PUBLICO_NUMERO_UNO :
				if (in_array ( 'roleCityId', $roles )) {
					CValidator::createValidator ( 'required', $this, 'roleCityId', array () )->validate ( $this );
				}
				break;
			case self::SERVIDOR_PUBLICO_NUMERO_DOS :
				if (in_array ( $this->role, $roles )) {
					CValidator::createValidator ( 'required', $this, $attribute, array () )->validate ( $this );
				}
				
				if (! $this->entityId) {
					CValidator::createValidator ( 'required', $this, 'entityId', array () )->validate ( $this );
				}
				break;
			case self::SERVIDOR_PUBLICO_NUMERO_TRES :
				if (in_array ( 'roleCityId', $roles )) {
					CValidator::createValidator ( 'required', $this, 'roleCityId', array () )->validate ( $this );
				}
				break;
			case self::USUARIO_FINAl :
				if (in_array ( $this->role, $roles )) {
					CValidator::createValidator ( 'required', $this, $attribute, array () )->validate ( $this );
				}
				
				if (! $this->entityId) {
					CValidator::createValidator ( 'required', $this, 'entityId', array () )->validate ( $this );
				}
		}
	}
	public function uniqueUserInRoleCountry($attribute, $params) {
		if ($this->role == self::ADMINISTRADOR_PRESIDENTE)
			CValidator::createValidator ( 'unique', $this, $attribute, array (
					'criteria' => array (
							'condition' => '`t`.`role`=:role',
							'params' => array (
									':role' => $this->role 
							) 
					) 
			) )->validate ( $this );
	}
	public function uniqueUserInRoleDepartment($attribute, $params) {
		if ($this->role == self::ADMINISTRADOR_GOBERNADOR)
			CValidator::createValidator ( 'unique', $this, $attribute, array (
					'criteria' => array (
							'condition' => '`t`.`role`=:role',
							'params' => array (
									':role' => $this->role 
							) 
					) 
			) )->validate ( $this );
	}
	public function uniqueUserInRoleCity($attribute, $params) {
		if ($this->role == self::SERVIDOR_PUBLICO_NUMERO_UNO)
			CValidator::createValidator ( 'unique', $this, $attribute, array (
					'criteria' => array (
							'condition' => '`t`.`role`=:role',
							'params' => array (
									':role' => $this->role 
							) 
					) 
			) )->validate ( $this );
	}
	
	/*
	 * public function requiredEntityOfRole($attribute, $params) {
	 * $roles = array (
	 * self::SERVIDOR_PUBLICO_NUMERO_DOS,
	 * self::SERVIDOR_PUBLICO_NUMERO_TRES,
	 * self::USUARIO_FINAl
	 * );
	 *
	 * if (is_numeric ( array_search ( $this->role, $roles ) ) && ! $this->entityId) { // Entidad requerida para secretaria, secretario de despacho y redactor
	 * return CValidator::createValidator ( 'required', $this, $attribute, array () )->validate ( $this );
	 * }
	 * }
	 */
	/**
	 * Users editors grouped for department.
	 *
	 * @param
	 *        	array user status.
	 * @return array of users.
	 */
	public function getUsersEditorsOfEntities($status = array(RedundantDataAbstract::STATUS_ACTIVO)) {
		$criteria = new CDbCriteria ();
		$user = Yii::app ()->user->getState ( 'user' );
		
		$group = $user->role !== self::SERVIDOR_PUBLICO_NUMERO_DOS;
		$criteria->compare ( 'role', self::USUARIO_FINAl );
		
		if (count ( $status )) {
			$criteria->addInCondition ( 'redundantDataOfUser.`status`', $status ); // Por defecto solo visualizo usuarios redactor activos
		}
		
		if ($user->role == self::SERVIDOR_PUBLICO_NUMERO_DOS) {
			$criteria->compare ( 'entityId', $user->entityId );
		} else {
			$criteria->compare ( 'roleCityId', $user->roleCityId );
		}
		
		if ($group) {
			$usersEditors = CHtml::listData ( $this->with ( array (
					'entityOfUser' 
			) )->findAll ( $criteria ), 'id', 'name', 'entityOfUser.name' );
		} else {
			$usersEditors = CHtml::listData ( $this->with ( array (
					'entityOfUser' 
			) )->findAll ( $criteria ), 'id', 'name' );
		}
		
		return $usersEditors;
	}
	/**
	 * List users editors.
	 *
	 * @param
	 *        	array user status.
	 * @return array of users.
	 */
	public function getListUsersEditorsOfEntities($status = array(RedundantDataAbstract::STATUS_ACTIVO)) {
		$criteria = new CDbCriteria ();
		$user = Yii::app ()->user->getState ( 'user' );
		
		$criteria->compare ( 'role', self::USUARIO_FINAl );
		
		if (count ( $status )) {
			$criteria->addInCondition ( 'redundantDataOfUser.`status`', $status ); // Por defecto solo visualizo usuarios redactor activos
		}
		
		if ($user->role == self::SERVIDOR_PUBLICO_NUMERO_DOS) {
			$criteria->compare ( 'entityId', $user->entityId );
		} else {
			$criteria->compare ( 'roleCityId', $user->roleCityId );
		}
		
		$usersEditors = CHtml::listData ( $this->with ( array (
				'entityOfUser' 
		) )->findAll ( $criteria ), 'id', 'name' );
		
		return $usersEditors;
	}
	public function getListUsersStatistics($roleCountryId, $roleDepartmentId, $roleCityId, $entityId) {
		$usersEditors = array ();
		$criteria = new CDbCriteria ();
		$criteria->compare ( 'role', self::USUARIO_FINAl );
		
		if ($roleCountryId) {
			$criteria->compare ( 'roleCountryId', $roleCountryId );
			
			$usersEditors = CHtml::listData ( $this->with ( array (
					'roleDepartmentOfUser' 
			) )->findAll ( $criteria ), 'id', 'name', 'roleDepartmentOfUser.name' );
		}
		
		if ($roleDepartmentId) {
			$criteria->compare ( 'roleDepartmentId', $roleDepartmentId );
			
			$usersEditors = CHtml::listData ( $this->with ( array (
					'roleCityOfUser' 
			) )->findAll ( $criteria ), 'id', 'name', 'roleCityOfUser.name' );
		}
		
		if ($roleCityId) {
			$criteria->compare ( 'roleCityId', $roleCityId );
			
			$usersEditors = CHtml::listData ( $this->with ( array (
					'entityOfUser' 
			) )->findAll ( $criteria ), 'id', 'name', 'entityOfUser.name' );
		}
		
		if ($entityId) {
			$criteria->compare ( 'entityId', $entityId );
			
			$usersEditors = CHtml::listData ( $this->with ( array () )->findAll ( $criteria ), 'id', 'name' );
		}
		
		return $usersEditors;
	}
	public function selectsRole($role) {
		$selectsRoles = array ();
		$user = Yii::app ()->user->isGuest ? $this : Yii::app ()->user->getState ( 'user' );
		
		if ($user->role == self::ADMINISTRADOR_SUPER_USUARIO) {
			switch ($role) {
				case self::ADMINISTRADOR_SUPER_USUARIO :
					$selectsRoles [] = 'roleCountryId';
					$selectsRoles [] = 'roleDepartmentId';
					$selectsRoles [] = 'roleCityId';
					$selectsRoles [] = 'entityId';
					break;
				case self::ADMINISTRADOR_PRESIDENTE :
					$selectsRoles [] = 'roleCountryId';
					break;
				case self::ADMINISTRADOR_GOBERNADOR :
					$selectsRoles [] = 'roleCountryId';
					$selectsRoles [] = 'roleDepartmentId';
					break;
				case self::SERVIDOR_PUBLICO_NUMERO_UNO :
					$selectsRoles [] = 'roleCountryId';
					$selectsRoles [] = 'roleDepartmentId';
					$selectsRoles [] = 'roleCityId';
					break;
				case self::SERVIDOR_PUBLICO_NUMERO_DOS :
					$selectsRoles [] = 'roleCountryId';
					$selectsRoles [] = 'roleDepartmentId';
					$selectsRoles [] = 'roleCityId';
					$selectsRoles [] = 'entityId';
					break;
				case self::SERVIDOR_PUBLICO_NUMERO_TRES :
					$selectsRoles [] = 'roleCountryId';
					$selectsRoles [] = 'roleDepartmentId';
					$selectsRoles [] = 'roleCityId';
					break;
				case self::USUARIO_FINAl :
					$selectsRoles [] = 'roleCountryId';
					$selectsRoles [] = 'roleDepartmentId';
					$selectsRoles [] = 'roleCityId';
					$selectsRoles [] = 'entityId';
			}
		} else {
			switch ($role) {
				case self::ADMINISTRADOR_PRESIDENTE :
					$selectsRoles [] = 'roleCountryId';
					break;
				case self::ADMINISTRADOR_GOBERNADOR :
					$selectsRoles [] = 'roleDepartmentId';
					break;
				case self::SERVIDOR_PUBLICO_NUMERO_UNO :
					$selectsRoles [] = 'roleCityId';
					break;
				case self::SERVIDOR_PUBLICO_NUMERO_DOS :
					$selectsRoles [] = 'entityId';
					break;
				case self::SERVIDOR_PUBLICO_NUMERO_TRES :
					$selectsRoles [] = 'roleCityId';
					break;
				case self::USUARIO_FINAl :
					$selectsRoles [] = 'entityId';
			}
		}
		
		return $selectsRoles;
	}
}