<?php
/**
 * This is the parent model class for table "tbl_sys_config_country_department_city".
 *
 */
abstract class CityAbstract extends NHCActiveRecord {
	public static $counterOfRelations = '';
	const IMAGE_WIDTH = 800;
	const IMAGE_HEIGHT = 400;
	const IMAGE_EXTENSION = 'png';
	public static function getImagePath() {
		return Yii::app ()->request->baseUrl . '/public/images/cities/';
	}
	public function behaviors() {
		return array (
				'RedundantDataBehavior' => array (
						'class' => 'RedundantDataBehavior' 
				),
				'CityBehavior' => array (
						'class' => 'CityAppBehavior'
				)
		);
	}
	public function defaultScope() {
		return array (
				'with' => array (
						'redundantDataOfCity' 
				) 
		);
	}
	
	/**
	 *
	 * @param CModel $attribute
	 *        	the attribute name to validate.
	 * @param array $params
	 *        	initial values to be applied to the validator properties.
	 * @return bool True if the city is unique.
	 */
	public function uniqueCity($attribute, $params) {
		CValidator::createValidator ( 'unique', $this, $attribute, array (
				'criteria' => array (
						'condition' => '`t`.`departmentId`=:departmentId',
						'params' => array (
								':departmentId' => $this->departmentId 
						) 
				) 
		) )->validate ( $this );
	}
	
	/**
	 * Cities grouped for department.
	 *
	 * @param
	 *        	string city status.
	 * @return array of cities.
	 */
	public function getCityOfDepartments($status = RedundantDataAbstract::STATUS_ACTIVO, $departmentId = 0, $group = false) {
		$criteria = new CDbCriteria ();
		
		if (is_numeric ( $departmentId ))
			$criteria->compare ( 'departmentId', $departmentId );
		
		if ($group) {
			if (is_null ( $status )) {
				$cities = CHtml::listData ( $this->with ( array (
						'departmentOfCity' 
				) )->findAll ( $criteria ), 'id', 'name', 'departmentOfCity.name' );
			} else {
				$cities = CHtml::listData ( $this->with ( array (
						'redundantDataOfCity' => array (
								'condition' => '`redundantDataOfCity`.`status`=:status',
								'params' => array (
										':status' => $status 
								) 
						),
						'departmentOfCity' 
				) )->findAll ( $criteria ), 'id', 'name', 'departmentOfCity.name' );
			}
		} else {
			if (is_null ( $status )) {
				$cities = CHtml::listData ( $this->findAll ( $criteria ), 'id', 'name' );
			} else {
				$cities = CHtml::listData ( $this->with ( array (
						'redundantDataOfCity' => array (
								'condition' => '`redundantDataOfCity`.`status`=:status',
								'params' => array (
										':status' => $status 
								) 
						) 
				) )->findAll ( $criteria ), 'id', 'name' );
			}
		}
		
		return $cities;
	}
	public function getStatus() {
		return array (
				RedundantDataAbstract::STATUS_ACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_ACTIVO ),
				RedundantDataAbstract::STATUS_INACTIVO => RedundantDataAbstract::getStatusLabel ( RedundantDataAbstract::STATUS_INACTIVO ) 
		);
	}
}