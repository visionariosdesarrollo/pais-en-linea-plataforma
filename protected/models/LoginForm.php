<?php
/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $username;
	public $password;
	public $rememberMe;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password', 'required'),
			array('username', 'length', 'min'=>5, 'max'=>20),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'length', 'min'=>5, 'max'=>60),
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'username'=>Yii::t('user','Nombre de usuario'),
			'password'=>Yii::t('user','Contraseña'),
			'rememberMe'=>Yii::t('user','No cerrar sesión')
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new NHCUserIdentity($this->username,$this->password);
			switch($this->_identity->authenticate())
			{
				case NHCUserIdentity::ERROR_USERNAME_INVALID:
				case NHCUserIdentity::ERROR_PASSWORD_INVALID:
					$this->addError('password',Yii::t('user','Nombre de usuario o contraseña incorrecta.'));
					break;
				case NHCUserIdentity::ERROR_STATUS_PENDING:
					$this->addError('password',Yii::t('user','Usuario pendiente de activación! Por favor póngase en contacto con el administrador del sistema.'));
					break;					
				case NHCUserIdentity::ERROR_STATUS_INACTIVE:
					$this->addError('password',Yii::t('user','Usuario inactivo! Por favor póngase en contacto con el administrador del sistema.'));
					break;
			}
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new NHCUserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->getIsAuthenticated())
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}
}