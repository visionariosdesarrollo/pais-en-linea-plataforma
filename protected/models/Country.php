<?php
/**
 * This is the model class for table "tbl_sys_config_country".
 *
 * The followings are the available columns in table 'tbl_sys_config_country':
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $creationUserId
 * @property integer $modifiedUserId
 *
 * The followings are the available model relations:
 * @property User $modifiedUser
 * @property User $creationUser
 * @property Department[] $departments
 * @property RedundantData $redundantDataCountry
 */
class Country extends CountryAbstract
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_sys_config_country';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, code, status', 'required'),
			array('creationUserId, modifiedUserId', 'numerical', 'integerOnly'=>true),				
			array('name', 'length', 'max'=>100),
			array('code', 'length', 'min'=>2, 'max'=>2),	
			array('name', 'unique'),
			array('code', 'unique'),
			//array('code', 'uniqueCountry'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, code, creationUserId, modifiedUserId, creationDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'departmentsOfCountry' => array(self::HAS_MANY, 'Department', 'countryId'),								
			'redundantDataOfCountry' => array(self::HAS_ONE, 'RedundantData', 'tableId',
				'on' => '`redundantDataOfCountry`.`table` = :tableCountry',
				'params' => array(':tableCountry' => $this->tableName()))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Identificador',
			'name' => 'Nombre de país',
			'code' => 'Código de 2 letras',
			'status' => 'Estado',
			'order' => 'Orden',
			'creationUser' => 'Usuario de creación',
			'creationUserId' => 'Usuario de creación',
			'modifiedUser' => 'Usuario de modificación',				
			'modifiedUserId' => 'Usuario de modificación',
			'creationDate' => 'Fecha de creación',
			'modifiedDate' => 'Fecha de modificación',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$user = Yii::app ()->user->getState ( 'user' );
		$criteria->compare('`t`.id',$this->id);
		$criteria->compare('`t`.`name`',$this->name,true);
		$criteria->compare('`code`',$this->code,true);
		$criteria->compare('status',$this->status,true);
		
		if($this->creationDate)
		{
			$criteria->addCondition(new CDbExpression('DATE_FORMAT(`redundantDataOfCountry`.`creationDate`,"%Y-%m-%d")=:creationDate'));
			$timestamp=CDateTimeParser::parse($this->creationDate,Yii::app()->locale->dateFormat);
			$criteria->params[':creationDate']=date('Y-m-d',$timestamp);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'`t`.`name` ASC, `redundantDataOfCountry`.`creationDate` DESC',
				'attributes'=>array(
					'name' => array(
            			'asc' => '`t`.`name` ASC',
						'desc' => '`t`.`name` DESC',
					),
					'code' => array(
						'asc' => '`code` ASC',
						'desc' => '`code` DESC',
					),
					'status' => array (
						'asc' => 'redundantDataOfEvent.status ASC',
						'desc' => 'redundantDataOfEvent.status DESC'
					),						
					'creationDate' => array(
						'asc' => '`redundantDataOfCountry`.`creationDate` ASC',
						'desc' => '`redundantDataOfCountry`.`creationDate` DESC',
					),						
				),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Country the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}