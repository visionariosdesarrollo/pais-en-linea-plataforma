<?php
/**
 * This is the model class for table "tbl_sys_config_country_department".
 *
 * The followings are the available columns in table 'tbl_sys_config_country_department':
 * @property integer $id
 * @property integer $countryId
 * @property string $name
 * @property integer $creationUserId
 * @property integer $modifiedUserId
 *
 * The followings are the available model relations:
 * @property Country $country
 * @property City[] $cities
 * @property RedundantData $redundantDataDepartment 
 */
class Department extends DepartmentAbstract
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_sys_config_country_department';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('countryId, name, status','required'),
			array('countryId, creationUserId, modifiedUserId', 'numerical', 'integerOnly'=>true),				
			array('name','length','max'=>100),
			array('name','uniqueDepartment'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, countryId, name, creationUserId, modifiedUserId, creationDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'countryOfDepartment' => array(self::BELONGS_TO, 'Country', 'countryId'),
			'citiesOfDepartment' => array(self::HAS_MANY, 'City', 'departmentId'),
			'redundantDataOfDepartment' => array(self::HAS_ONE, 'RedundantData', 'tableId',
				'on' => '`redundantDataOfDepartment`.`table` = :tableDepartment',
				'params' => array(':tableDepartment' => $this->tableName())
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Identificador',
			'countryId' => 'País',
			'name' => 'Nombre de departamento',
			'status' => 'Estado',
			'order' => 'Orden',
			'creationUser' => 'Usuario de creación',
			'creationUserId' => 'Usuario de creación',
			'modifiedUser' => 'Usuario de modificación',				
			'modifiedUserId' => 'Usuario de modificación',
			'creationDate' => 'Fecha de creación',
			'modifiedDate' => 'Fecha de modificación',		
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$user = Yii::app ()->user->getState ( 'user' );
		$criteria->compare('`t`.id',$this->id);
		$criteria->compare('`countryId`',$this->countryId);
		$criteria->compare('`t`.name',$this->name,true);
		$criteria->compare('status',$this->status,true);

		if($this->creationDate)
		{
			$criteria->addCondition(new CDbExpression('DATE_FORMAT(`redundantDataOfDepartment`.`creationDate`,"%Y-%m-%d")=:creationDate'));
			$timestamp=CDateTimeParser::parse($this->creationDate,Yii::app()->locale->dateFormat);
			$criteria->params[':creationDate']=date('Y-m-d',$timestamp);
		}		
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'`t`.`name` ASC, `redundantDataOfDepartment`.`creationDate` DESC',
				'attributes'=>array(
					'countryId' => array(
						'asc' => '`countryId` ASC',
						'desc' => '`countryId` DESC',
					),						
					'name' => array(
						'asc' => '`t`.`name` ASC',
						'desc' => '`t`.`name` DESC',
					),
					'status' => array (
						'asc' => 'redundantDataOfEvent.status ASC',
						'desc' => 'redundantDataOfEvent.status DESC'
					),						
					'creationDate' => array(
						'asc' => '`redundantDataOfDepartment`.`creationDate` ASC',
						'desc' => '`redundantDataOfDepartment`.`creationDate` DESC',
					),
				),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Department the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
