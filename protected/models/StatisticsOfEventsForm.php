<?php

class StatisticsOfEventsForm extends CFormModel
{
	public $roleCountryId;
	public $roleDepartmentId;
	public $roleCityId;
	public $entityId;
	public $userId;
	public $type;
	
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array('roleCountryId, roleDepartmentId, roleCityId, entityId, userId', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'min'=>3, 'max'=>100),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'roleCountryId'=>Yii::t('user','País'),
			'roleDepartmentId'=>Yii::t('user','Departamento'),
			'roleCityId'=>Yii::t('user','Ciudad'),
			'entityId'=>Yii::t('user','Entidad'),
			'userId'=>Yii::t('user','Usuario'),
			'type'=>Yii::t('user','Tipo de actividad'),
		);
	}

	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria = new CDbCriteria ();
		
		
		$criteria->with = array(
				'entityEvent' => array(
					'with' => array(
						'cityOfEntity' => array(
							'with' => array(
								'departmentOfCity' => array(
									'countryOfDepartment'
								)
							)	
						)
					)
				)
		);
		
		
		$criteria->compare ( 'departmentOfCity.countryId', $this->roleCountryId, true );
		$criteria->compare ( 'cityOfEntity.departmentId', $this->roleDepartmentId, true );
		$criteria->compare ( 'entityEvent.cityId', $this->roleCityId, true );	
		$criteria->compare ( 't.entityId', $this->entityId, true );
		$criteria->compare ( 'redundantDataOfEvent.creationUserId', $this->userId, true );
		$criteria->compare ( 'type', $this->type);
		
/*
		$criteria->group = 'departmentOfCity.countryId';
		
		if($this->roleDepartmentId)
			$criteria->group = 'cityOfEntity.departmentId';
		
		if($this->roleCityId)
			$criteria->group = 'entityEvent.cityId';

		if($this->entityId)
			$criteria->group = 't.entityId';
		
		if($this->userId)
			$criteria->group = 'MONTH(t.eventDateStart)';
		*/
			
		$events = array();
		$modelsEvents = Event::model()->findAll($criteria);
		
		if(is_array($modelsEvents) && count($modelsEvents)) {
			$locale = Yii::app ()->locale;
			$dateTimeParser = new CDateTimeParser ();
			$mysqlDateTimeFormat = NHCActiveRecord::MYSQL_DATE_FORMAT . ' ' . NHCActiveRecord::MYSQL_TIME_FORMAT;
			foreach ($modelsEvents as $model) {
				$dateTimeCreationDate = $dateTimeParser->parse ( $model->redundantDataOfEvent->creationDate, $mysqlDateTimeFormat );
				$month = date('n',$dateTimeCreationDate);
				$year = date('Y',$dateTimeCreationDate);
				$key = $locale->getMonthName($month).' '.$year;
				$eventsAux[$key][] = $model->name;
			}

			foreach ($eventsAux as $key=>$value) {
				$events[$key]=count($eventsAux[$key]);
			}
		}

		return $events;
		
		/*
		//$criteria->compare ( 'redundantDataOfEvent.creationUserId', $this->creationUserId );
	
		if ($this->eventDateStart) {
			$criteria->addCondition ( new CDbExpression ( 'DATE_FORMAT(eventDateStart,"%Y-%m-%d")=:eventDateStart' ) );
			$timestamp = CDateTimeParser::parse ( $this->eventDateStart, Yii::app ()->locale->dateFormat );
			$criteria->params [':eventDateStart'] = date ( 'Y-m-d', $timestamp );
		}
	
		if ($this->eventDateEnd) {
			$criteria->addCondition ( new CDbExpression ( 'DATE_FORMAT(eventDateEnd,"%Y-%m-%d")=:eventDateEnd' ) );
			$timestamp = CDateTimeParser::parse ( $this->eventDateEnd, Yii::app ()->locale->dateFormat );
			$criteria->params [':eventDateEnd'] = date ( 'Y-m-d', $timestamp );
		}
	
		if ($this->creationDate) {
			$criteria->addCondition ( new CDbExpression ( 'DATE_FORMAT(redundantDataOfEvent.creationDate,"%Y-%m-%d")=:creationDate' ) );
			$timestamp = CDateTimeParser::parse ( $this->creationDate, Yii::app ()->locale->dateFormat );
			$criteria->params [':creationDate'] = date ( 'Y-m-d', $timestamp );
		}
	
		if ($this->status) {
			switch ($this->status) {
				case RedundantDataAbstract::STATUS_CUMPLIDO : // El d�a de finalizaci�n es igual o menor que el d�a actual y la hora de finalizaci�n es menor que la hora actual.
					$criteriaAux = new CDbCriteria();
					$criteriaAux->addCondition ( new CDbExpression('CONCAT(DATE_FORMAT(eventDateEnd, "%Y-%m-%d")," ",DATE_FORMAT(eventTimeEnd, "%H:%i")) < DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")'));
					//$criteriaAux->addCondition ( new CDbExpression('DATE_FORMAT(NOW(), "%H:%i") > DATE_FORMAT(eventTimeEnd, "%H:%i")'));
						
					$status = array (
							RedundantDataAbstract::STATUS_INACTIVO,
							RedundantDataAbstract::STATUS_ACTIVO,
							RedundantDataAbstract::STATUS_CUMPLIDO,
							RedundantDataAbstract::STATUS_CANCELADO,
							RedundantDataAbstract::STATUS_PENDIENTE
					);
						
					
						//if($user->role != UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS)
							//$status[] = RedundantDataAbstract::STATUS_PENDIENTE;
					
	
						$criteriaAux->addInCondition ( 'redundantDataOfEvent.status', $status);
						$criteria->mergeWith($criteriaAux);
						$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
						break;
				case RedundantDataAbstract::STATUS_ACTIVO : // Puede ser pendiente "Pendiente por revisar" o activa. El d�a de finalizaci�n debe de ser menor o igual que el d�a actual y la hora de finalizaci�n debe ser menor que la hora actual.
					$criteriaAux = new CDbCriteria();
					$criteriaAux->addCondition ( new CDbExpression('CONCAT(DATE_FORMAT(eventDateEnd, "%Y-%m-%d")," ",DATE_FORMAT(eventTimeEnd, "%H:%i")) > DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")'));
						
					$status = array (
							RedundantDataAbstract::STATUS_INACTIVO,
							RedundantDataAbstract::STATUS_ACTIVO,
							RedundantDataAbstract::STATUS_CUMPLIDO,
							RedundantDataAbstract::STATUS_CANCELADO,
					);
						
					if($user->role != UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS)
						$status[] = RedundantDataAbstract::STATUS_PENDIENTE;
							
						$criteriaAux->addInCondition ( 'redundantDataOfEvent.status', $status);
						$criteria->mergeWith($criteriaAux);
						$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
						break;
				case RedundantDataAbstract::STATUS_ELIMINADO:
					$criteriaAux = new CDbCriteria();
					$criteria->compare ( 'redundantDataOfEvent.status', RedundantDataAbstract::STATUS_ELIMINADO);
					$criteria->mergeWith($criteriaAux);
					break;
				default :
					$criteria->compare ( 'redundantDataOfEvent.status', $this->status);
					$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
			}
		} else
			$criteria->addNotInCondition('redundantDataOfEvent.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
	
			return new CActiveDataProvider ( $this, array (
					'criteria' => $criteria,
					'sort' => array (
							'defaultOrder' => 't.name ASC, redundantDataOfEvent.creationDate DESC',
							'attributes' => array (
									'eventDateStart' => array (
											'asc' => 'eventDateStart ASC',
											'desc' => 'eventDateStart DESC'
									),
									'name' => array (
											'asc' => 't.name ASC',
											'desc' => 't.name DESC'
									),
									'status' => array (
											'asc' => 'redundantDataOfEvent.status ASC',
											'desc' => 'redundantDataOfEvent.status DESC'
									),
									'creationUserId' => array (
											'asc' => 'redundantDataOfEvent.creationUserId ASC',
											'desc' => 'redundantDataOfEvent.creationUserId DESC'
									),
									'entityId' => array (
											'asc' => 'entityEvent.name ASC',
											'desc' => 'entityEvent.name DESC'
									),
									'type' => array (
											'asc' => 'type ASC',
											'desc' => 'type DESC'
									),
									'creationDate' => array (
											'asc' => 'redundantDataOfEvent.creationDate ASC',
											'desc' => 'redundantDataOfEvent.creationDate DESC'
									)
							)
					)
			) );
		*/
		

	}
}