<?php
/**
 * This is the model class for table "tbl_mod_user".
 *
 * The followings are the available columns in table 'tbl_mod_user':
 * @property integer $id
 * @property string $role 
 * @property string $name
 * @property string $documentType  
 * @property string $document
 * @property string $email
 * @property string $dataLastAccess
 * @property integer $creationUserId
 * @property integer $modifiedUserId
 *
 * The followings are the available model relations:
 * @property City $city
 * @property Entity $entity
 * @property UserRole[] $userRoles 
 * @property RedundantData $redundantDataUser
 */
class UserApp extends UserAppAbstract
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_mod_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, role, email, roleCountryId, roleDepartmentId, roleCityId, sex, territory, documentType, document, status, documentDeliveryDate', 'required'),
			array('roleCountryId, roleDepartmentId, roleCityId', 'numerical', 'integerOnly'=>true),				
			array('name, email, documentDeliveryDate', 'length', 'max'=>50),
			array('document', 'length', 'max'=>7, 'max'=>20),
			array('documentType, territory', 'length', 'max'=>9),
			array('email', 'email'),
			array('document', 'unique', 'criteria' => array('condition' => 't.role=:role', 'params' => array(':role' => User::USUARIO_USUARIO))),
			array('dataLastAccess', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated dsadbelow.
		return array(
			'roleCountryOfUser' => array(self::BELONGS_TO, 'Country', 'roleCountryId'),
			'roleDepartmentOfUser' => array(self::BELONGS_TO, 'Department', 'roleDepartmentId'),
			'roleCityOfUser' => array(self::BELONGS_TO, 'City', 'roleCityId'),
			'creationUserUser' => array(self::HAS_MANY, 'RedundantData', 'creationUserId'),
      		'modifiedUserUser' => array(self::HAS_MANY, 'RedundantData', 'modifiedUserId'),
			'redundantDataOfUserApp' => array(self::HAS_ONE, 'RedundantData', 'tableId',
				'on' => '`redundantDataOfUserApp`.`table` = :tableUser',
				'params' => array(':tableUser' => $this->tableName()),
			),
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Identificador',
			'role' => 'Perfil',
			'roleCountryId' => 'País',
			'roleDepartmentId' => 'Departamento',
			'roleCityId' => 'Ciudad',				
			'name' => 'Nombre y apellidos de usuario',
			'documentType' => 'Tipo de documento',
			'email' => 'Correo electrónico',
			'dataLastAccess' => 'Fecha de último acceso',
			'documentDeliveryDate' => 'Fecha de expedición',
			'status' => 'Estado',
			'creationUser' => 'Usuario de creación',
			'creationUserId' => 'Usuario de creación',
			'modifiedUser' => 'Usuario de modificación',				
			'modifiedUserId' => 'Usuario de modificación',
			'creationDate' => 'Fecha de creación',
			'modifiedDate' => 'Fecha de modificación',			
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}