<?php
/**
 * UserBehavior class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

/**
 * EventBehavior is the base class for behaviors that can be attached to {@link CActiveRecord} model class Event.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @since 1.0
 */
class ReleaseAppBehavior extends NHCActiveRecordBehavior {
	/**
	 * Responds to {@link CActiveRecord::onBeforeSave} event.
	 * Override this method and make it public if you want to handle the corresponding
	 * event of the {@link CBehavior::owner owner}.
	 * You may set {@link CModelEvent::isValid} to be false to quit the saving process.
	 * 
	 * @param CModelEvent $event
	 *        	event parameter
	 */
	public function beforeSave($event) {
		$this->owner->status = RedundantDataAbstract::STATUS_ACTIVO;
		
		if($this->owner->releaseDateStart) {
			$releaseDateStart = explode('/', $this->owner->releaseDateStart);
			if(is_array($releaseDateStart) && count($releaseDateStart))
				$this->owner->releaseDateStart = sprintf('%s-%s-%s', $releaseDateStart[2], $releaseDateStart[1], $releaseDateStart[0]);
		}

		if($this->owner->releaseDateEnd) {
			$releaseDateEnd = explode('/', $this->owner->releaseDateEnd);
			if(is_array($releaseDateEnd) && count($releaseDateEnd))
				$this->owner->releaseDateEnd = sprintf('%s-%s-%s', $releaseDateEnd[2], $releaseDateEnd[1], $releaseDateEnd[0]);
		}
	}
	
	/**
	 * Responds to {@link CActiveRecord::onAfterFind} event.
	 * Override this method and make it public if you want to handle the corresponding event
	 * of the {@link CBehavior::owner owner}.
	 * 
	 * @param CEvent $event
	 *        	event parameter
	 */
	public function afterFind($event) {
		$locale = Yii::app ()->locale;
		$timeFormat = str_replace ( 'HH', 'H', $locale->getTimeFormat ( 'short' ) ) . ' a';
		$dateFormat = $locale->getDateFormat ();
		$mysqlDateTimeFormat = NHCActiveRecord::MYSQL_DATE_FORMAT . ' ' . NHCActiveRecord::MYSQL_TIME_FORMAT;
		
		$dateTimeStart = $this->owner->releaseDateStart . ' 00:00:01';
		$dateTimeEnd = $this->owner->releaseDateEnd . ' 23:59:59';
		
		$dateTimeParser = new CDateTimeParser ();
		$this->owner->releaseTimeStampStart = $dateTimeStartParser = $dateTimeParser->parse ( $dateTimeStart, $mysqlDateTimeFormat );
		$this->owner->releaseTimeStampEnd = $dateTimeEndParser = $dateTimeParser->parse ( $dateTimeEnd, $mysqlDateTimeFormat );
		
		if(isset($this->owner->userOfRelease)){
			$this->owner->cityId = $this->owner->userOfRelease->roleCityId;
			$this->owner->userName = $this->owner->userOfRelease->name;
			$this->owner->userImage = $this->owner->userOfRelease->image;
			$this->owner->userRole = $this->owner->userOfRelease->role;
			if(isset($this->owner->userOfRelease->roleDepartmentOfUser))
				$this->owner->userDepartmentName = $this->owner->userOfRelease->roleDepartmentOfUser->name;
			if(isset($this->owner->userOfRelease->roleCityOfUser))
				$this->owner->userCityName = $this->owner->userOfRelease->roleCityOfUser->name;
		}
		
		/*
		$this->owner->releaseDateStart = $locale->getDateFormatter ()->format ( $dateFormat, $dateTimeStartParser );
		// $this->owner->eventTimeStart = date('g:m a', $dateTimeStartParser ); // $this->owner->eventTimeStart = $locale->getDateFormatter ()->format ( $timeFormat, $dateTimeStartParser );
		$this->owner->releaseTimeStart = $locale->getDateFormatter ()->format ( $timeFormat, $dateTimeStartParser );
		$this->owner->releaseDateEnd = $locale->getDateFormatter ()->format ( $dateFormat, $dateTimeEndParser );
		// $this->owner->eventTimeEnd = date('g:m a', $dateTimeEndParser ); // $this->owner->eventTimeEnd = $locale->getDateFormatter ()->format ( $timeFormat, $dateTimeEndParser );
		$this->owner->releaseTimeEnd = $locale->getDateFormatter ()->format ( $timeFormat, $dateTimeEndParser );
		*/
		if($this->owner->releaseDateStart) {
			$releaseDateStart = explode('-', $this->owner->releaseDateStart );
			if(is_array($releaseDateStart) && count($releaseDateStart))
				$this->owner->releaseDateStart = sprintf('%s/%s/%s', $releaseDateStart[2], $releaseDateStart[1], $releaseDateStart[0]);
		}
		
		if($this->owner->releaseDateEnd) {
			$releaseDateEnd = explode('-', $this->owner->releaseDateEnd );
			if(is_array($releaseDateEnd) && count($releaseDateEnd))
				$this->owner->releaseDateEnd = sprintf('%s/%s/%s', $releaseDateEnd[2], $releaseDateEnd[1], $releaseDateEnd[0]);
		}
	}
}