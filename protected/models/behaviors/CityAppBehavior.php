<?php
/**
 * CityAppBehavior class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

/**
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @since 1.0
 */
class CityAppBehavior extends NHCActiveRecordBehavior {
	private function image() {
		if (! $this->owner->image || ! file_exists ( '.' . CityAbstract::getImagePath () . $this->owner->image ))
			$this->owner->image = '';
	}
	
	/**
	 * Responds to {@link CActiveRecord::onAfterFind} event.
	 * Override this method and make it public if you want to handle the corresponding event
	 * of the {@link CBehavior::owner owner}.
	 *
	 * @param CEvent $event
	 *        	event parameter
	 */
	public function afterFind($event) {
		$this->image ();
	}
}