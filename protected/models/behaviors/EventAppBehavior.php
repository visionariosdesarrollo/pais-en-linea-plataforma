<?php
/**
 * UserBehavior class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

/**
 * EventBehavior is the base class for behaviors that can be attached to {@link CActiveRecord} model class Event.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @since 1.0
 */
class EventAppBehavior extends NHCActiveRecordBehavior {
	/**
	 * Responds to {@link CActiveRecord::onBeforeSave} event.
	 * Override this method and make it public if you want to handle the corresponding
	 * event of the {@link CBehavior::owner owner}.
	 * You may set {@link CModelEvent::isValid} to be false to quit the saving process.
	 *
	 * @param CModelEvent $event
	 *        	event parameter
	 */
	public function beforeSave($event) {
		$locale = Yii::app ()->locale;
		$timeFormat = $locale->getTimeFormat ( 'short' ) . ' a';
		$dateTimeFormat = $locale->getDateFormat () . ' ' . $timeFormat;
		
		if ($this->owner->isNewRecord) { // Desface de cero al inicio de hora en formato de 1 a 12
			$timeArray = explode ( ':', $this->owner->eventTimeStart );
			if (is_array ( $timeArray ))
				$this->owner->eventTimeStart = ($timeArray [0] < 10 ? '0' . $timeArray [0] : $timeArray [0]) . ':' . $timeArray [1];
		}
		$dateTimeStart = $this->owner->eventDateStart . ' ' . $this->owner->eventTimeStart;
		
		if ($this->owner->isNewRecord) { // Desface de cero al inicio de hora en formato de 1 a 12
			$timeArray = explode ( ':', $this->owner->eventTimeEnd );
			if (is_array ( $timeArray ))
				$this->owner->eventTimeEnd = ($timeArray [0] < 10 ? '0' . $timeArray [0] : $timeArray [0]) . ':' . $timeArray [1];
		}
		$dateTimeEnd = $this->owner->eventDateEnd . ' ' . $this->owner->eventTimeEnd;
		
		$dateTimeParser = new CDateTimeParser ();
		$dateTimeStartParser = $dateTimeParser->parse ( $dateTimeStart, $dateTimeFormat );
		$dateTimeEndParser = $dateTimeParser->parse ( $dateTimeEnd, $dateTimeFormat );
		
		$this->owner->eventDateStart = $locale->getDateFormatter ()->format ( NHCActiveRecord::MYSQL_DATE_FORMAT, $dateTimeStartParser );
		$this->owner->eventTimeStart = $locale->getDateFormatter ()->format ( NHCActiveRecord::MYSQL_TIME_FORMAT, $dateTimeStartParser );
		$this->owner->eventDateEnd = $locale->getDateFormatter ()->format ( NHCActiveRecord::MYSQL_DATE_FORMAT, $dateTimeEndParser );
		$this->owner->eventTimeEnd = $locale->getDateFormatter ()->format ( NHCActiveRecord::MYSQL_TIME_FORMAT, $dateTimeEndParser );
		
		if (is_array ( $this->owner->type ))
			$this->owner->type = implode ( ',', $this->owner->type );
	}
	
	/**
	 * Responds to {@link CActiveRecord::onAfterFind} event.
	 * Override this method and make it public if you want to handle the corresponding event
	 * of the {@link CBehavior::owner owner}.
	 *
	 * @param CEvent $event
	 *        	event parameter
	 */
	public function afterFind($event) {
		$locale = Yii::app ()->locale;
		$timeFormat = str_replace ( 'HH', 'H', $locale->getTimeFormat ( 'short' ) ) . ' a';
		$dateFormat = $locale->getDateFormat ();
		$mysqlDateTimeFormat = NHCActiveRecord::MYSQL_DATE_FORMAT . ' ' . NHCActiveRecord::MYSQL_TIME_FORMAT;
		
		$dateTimeStart = $this->owner->eventDateStart . ' ' . $this->owner->eventTimeStart;
		$dateTimeEnd = $this->owner->eventDateEnd . ' ' . $this->owner->eventTimeEnd;
		
		$dateTimeParser = new CDateTimeParser ();
		$this->owner->eventTimeStampStart = $dateTimeStartParser = $dateTimeParser->parse ( $dateTimeStart, $mysqlDateTimeFormat );
		$this->owner->eventTimeStampEnd = $dateTimeEndParser = $dateTimeParser->parse ( $dateTimeEnd, $mysqlDateTimeFormat );
		$this->owner->eventTimeStampEndLimit = strtotime('+ ' . EventAbstract::LIMIT_COMMENT . ' hours', $this->owner->eventTimeStampEnd);
		
		$this->owner->eventDateStart = $locale->getDateFormatter ()->format ( $dateFormat, $dateTimeStartParser );
		// $this->owner->eventTimeStart = date('g:m a', $dateTimeStartParser ); // $this->owner->eventTimeStart = $locale->getDateFormatter ()->format ( $timeFormat, $dateTimeStartParser );
		$this->owner->eventTimeStart = $locale->getDateFormatter ()->format ( $timeFormat, $dateTimeStartParser );
		$this->owner->eventDateEnd = $locale->getDateFormatter ()->format ( $dateFormat, $dateTimeEndParser );
		// $this->owner->eventTimeEnd = date('g:m a', $dateTimeEndParser ); // $this->owner->eventTimeEnd = $locale->getDateFormatter ()->format ( $timeFormat, $dateTimeEndParser );
		$this->owner->eventTimeEnd = $locale->getDateFormatter ()->format ( $timeFormat, $dateTimeEndParser );
		
		if (is_string ( $this->owner->type ))
			$this->owner->type = explode ( ',', $this->owner->type );
		else if (! is_array ( $this->owner->type ))
			$this->owner->type = array ();
	}
}