<?php
/**
 * UserBehavior class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

/**
 * UserBehavior is the base class for behaviors that can be attached to {@link CActiveRecord} model class for table "tbl_redundant_data"..
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @since 1.0
 */
class UserAppBehavior extends NHCActiveRecordBehavior {
	private function findRedundantData() {
		if ($this->owner->entityOfUser !== null) // Set type of user
			$this->owner->type = UserAbstract::TYPE_ENTITY;
	}
	
	/**
	 * Responds to {@link CActiveRecord::onAfterFind} event.
	 * Override this method and make it public if you want to handle the corresponding event
	 * of the {@link CBehavior::owner owner}.
	 * 
	 * @param CEvent $event
	 *        	event parameter
	 */
	public function afterFind($event) {
		$this->findRedundantData ();
	}
}