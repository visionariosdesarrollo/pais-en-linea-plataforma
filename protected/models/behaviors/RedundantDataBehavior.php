<?php
/**
 * RedundantDataBehavior class file.
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @link http://www.netha.com.co/
 * @copyright 2015-2018 NH Software LLC
 * @license http://www.netha.com.co/license/
 */

/**
 * RedundantDataBehavior is the base class for behaviors that can be attached to {@link CActiveRecord} model class for table "tbl_redundant_data".
 *
 * @author Diego Montoya <desarrollo@netha.com.co>
 * @since 1.0
 */
class RedundantDataBehavior extends NHCActiveRecordBehavior {
	private function loadModel() {
		$model = RedundantData::model ()->findByAttributes ( array (
				'table' => $this->owner->tableName (),
				'tableId' => $this->owner->getPrimaryKey () 
		) );
		return $model;
	}
	private function findRedundantData() {
		$propertyName = 'redundantDataOf' . get_class ( $this->owner );
		$redundantData = $this->owner->$propertyName;
		if ($redundantData == ! null) {
			$this->owner->status = $redundantData->status;
			$this->owner->order = $redundantData->order;
			//$this->owner->creationUser = $redundantData->creationUserRedundantData;
			//$this->owner->modifiedUser = $redundantData->modifiedUserRedundantData;
			$this->owner->creationUserId = $redundantData->creationUserId;
			$this->owner->modifiedUserId = $redundantData->modifiedUserId;
			$this->owner->creationDate = $redundantData->creationDate;
			$this->owner->modifiedDate = $redundantData->modifiedDate;
		}
	}
	private function createRedundantData() {
		if(!$modelRedundantData = $this->loadModel ()) {
			$modelRedundantData = new RedundantData ();
			$modelRedundantData->setAttributes ( array (
					'table' => $this->owner->tableName (),
					'tableId' => $this->owner->getPrimaryKey (),
					'status' => $this->owner->status,
					'order' => $this->owner->order,
					'creationUserId' => Yii::app ()->user->getId () 
			) );
		} else
			$this->updateRedundantData();
		
		$modelRedundantData->save ( false );
	}
	private function updateRedundantData() {
		$modelRedundantData = $this->loadModel ();
		if ($modelRedundantData == ! null) {
			$modelRedundantData->setAttributes ( array (
					'status' => $this->owner->status,
					'order' => $this->owner->order,
					'modifiedUserId' => Yii::app ()->user->getId (),
					'modifiedDate' => new CDbExpression ( 'CURRENT_TIMESTAMP' ) 
			) );
			$modelRedundantData->save ( false );
		} else
			$this->createRedundantData ();
	}
	private function deleteRedundantData() {
		$modelRedundantData = $this->loadModel ();
		if ($modelRedundantData == ! null)
			$modelRedundantData->delete ();
	}
	
	/**
	 * Responds to {@link CActiveRecord::onAfterFind} event.
	 * Override this method and make it public if you want to handle the corresponding event
	 * of the {@link CBehavior::owner owner}.
	 *
	 * @param CEvent $event
	 *        	event parameter
	 */
	public function afterFind($event) {
		$this->findRedundantData ();
	}
	
	/**
	 * Responds to {@link CActiveRecord::onAfterSave} event.
	 * Override this method and make it public if you want to handle the corresponding event
	 * of the {@link CBehavior::owner owner}.
	 *
	 * @param CEvent $event
	 *        	event parameter
	 */
	public function afterSave($event) {
		if ($this->owner->isNewRecord)
			$this->createRedundantData ();
		else
			$this->updateRedundantData ();
	}
	
	/**
	 * Responds to {@link CActiveRecord::onAfterDelete} event.
	 * Override this method and make it public if you want to handle the corresponding event
	 * of the {@link CBehavior::owner owner}.
	 *
	 * @param CEvent $event
	 *        	event parameter
	 */
	public function afterDelete($event) {
		$this->deleteRedundantData ();
	}
}