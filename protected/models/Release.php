<?php

/**
 * This is the model class for table "tbl_mod_release".
 *
 * The followings are the available columns in table 'tbl_mod_release':
 * @property integer $id
 * @property string $name
 * @property integer $userId 
 * @property string $releaseDateStart
 * @property string $releaseDateEnd
 * @property string $body
 */
class Release extends ReleaseAbstract
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_mod_release';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userId, name, releaseDateStart, releaseDateEnd, body', 'required'),
			array('name', 'length', 'max'=>250),
			//array('releaseDateEnd', 'dateStartEventMigherOrEqualDateEndEvent'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, releaseDateStart, releaseDateEnd, body', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userOfRelease' => array(self::BELONGS_TO, 'User', 'userId'),
			'redundantDataOfRelease' => array (self::HAS_ONE, 'RedundantData', 'tableId',
					'on' => 'redundantDataOfRelease.table = :table',
					'params' => array (
							':table' => $this->tableName ()
					)
			)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Identificador',
			'name' => 'Nombre del comunicado',
			'releaseDateStart' => 'Fecha inicio del comunicado',
			'releaseDateEnd' => 'Fecha finalización del comunicado',
			'body' => 'Descripción del comunicado',
				'status' => 'Estado',
				'order' => 'Orden',
				'creationUser' => 'Usuario de creación',
				'creationUserId' => 'Usuario de creación',
				'modifiedUser' => 'Usuario de modificación',
				'modifiedUserId' => 'Usuario de modificación',
				'creationDate' => 'Fecha de creación',
				'modifiedDate' => 'Fecha de modificación',				
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$user = Yii::app ()->user->getState ( 'user' );
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('releaseDateStart',$this->releaseDateStart,true);
		$criteria->compare('releaseDateEnd',$this->releaseDateEnd,true);
		$criteria->compare('body',$this->body,true);

		if ($this->releaseDateStart) {
			$criteria->addCondition ( new CDbExpression ( 'DATE_FORMAT(eventDateStart,"%Y-%m-%d")=:releaseDateStart' ) );
			$timestamp = CDateTimeParser::parse ( $this->releaseDateStart, Yii::app ()->locale->dateFormat );
			$criteria->params [':releaseDateStart'] = date ( 'Y-m-d', $timestamp );
		}
		
		if ($this->creationDate) {
			$criteria->addCondition ( new CDbExpression ( 'DATE_FORMAT(redundantDataOfRelease.creationDate,"%Y-%m-%d")=:creationDate' ) );
			$timestamp = CDateTimeParser::parse ( $this->creationDate, Yii::app ()->locale->dateFormat );
			$criteria->params [':creationDate'] = date ( 'Y-m-d', $timestamp );
		}
		
		if($user->role != UserAbstract::ADMINISTRADOR_SUPER_USUARIO)
			$criteria->addNotInCondition('redundantDataOfRelease.status', array(RedundantDataAbstract::STATUS_ELIMINADO));
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
				'sort' => array (
						'defaultOrder' => 't.name ASC, redundantDataOfRelease.creationDate DESC',
						'attributes' => array (
								'releaseDateStart' => array (
										'asc' => 'releaseDateStart ASC',
										'desc' => 'releaseDateStart DESC'
								),
								'name' => array (
										'asc' => 't.name ASC',
										'desc' => 't.name DESC'
								),
								'status' => array (
										'asc' => 'redundantDataOfRelease.status ASC',
										'desc' => 'redundantDataOfRelease.status DESC'
								),
								'creationDate' => array (
										'asc' => 'redundantDataOfRelease.creationDate ASC',
										'desc' => 'redundantDataOfRelease.creationDate DESC'
								)
						)
				)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Release the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
