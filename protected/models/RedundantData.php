<?php
/**
 * This is the model class for table "tbl_redundant_data".
 *
 * The followings are the available columns in table 'tbl_redundant_data':
 * @property string $id
 * @property string $table
 * @property integer $tableId
 * @property string $status
 * @property integer $order
 * @property integer $creationUserId
 * @property integer $modifiedUserId
 * @property string $creationDate
 * @property string $modifiedDate
 *
 * The followings are the available model relations:
 */
class RedundantData extends RedundantDataAbstract
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_redundant_data';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('table, tableId, creationDate', 'required'),
			array('tableId, order, creationUserId, modifiedUserId', 'numerical', 'integerOnly'=>true),
			array('table', 'length', 'max'=>38),
			array('status', 'length', 'max'=>8),
			array('modifiedDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, table, tableId, status, order, creationUserId, modifiedUserId, creationDate, modifiedDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			/*
			'creationUserRedundantData' => array(self::BELONGS_TO, 'User', 'id',
				'on' => '`creationUserRedundantData`.`id`=`redundantDataOfUser`.`creationUserId`'
			),
			'modifiedUserRedundantData' => array(self::BELONGS_TO, 'User', 'id',
				'on' => '`modifiedUserRedundantData`.`id`=`redundantDataOfUser`.`modifiedUserId`'
			),
			*/
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Identificador',
			'table' => 'Nombre de tabla',
			'tableId' => 'Identificador de tabla',
			'status' => 'Estado',
			'order' => 'Orden',
			'creationUserId' => 'Usuario de creación',
			'modifiedUserId' => 'Usuario de modificación',
			'creationDate' => 'Fecha de creación',
			'modifiedDate' => 'Fecha de modificación',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('table',$this->table,true);
		$criteria->compare('tableId',$this->tableId);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('order',$this->order);
		$criteria->compare('creationUserId',$this->creationUserId);
		$criteria->compare('modifiedUserId',$this->modifiedUserId);
		$criteria->compare('creationDate',$this->creationDate,true);
		$criteria->compare('modifiedDate',$this->modifiedDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RedundantData the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}