<?php
/**
 * This is the model class for table "tbl_mod_entity".
 *
 * The followings are the available columns in table 'tbl_mod_entity':
 * @property integer $id
 * @property integer $cityId
 * @property string $name
 * @property string $description
 * @property string $email
 * @property string $phoneOne
 * @property string $phoneTwo
 * @property string $image 
 * @property integer $creationUserId
 * @property integer $modifiedUserId
 * 
 * The followings are the available model relations:
 * @property City $city
 * @property User[] $users
 * @property RedundantData $redundantDataEntity  
 */
class Entity extends EntityAbstract
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_mod_entity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cityId, name, email, phoneOne, status', 'required'),
			array('cityId, creationUserId, modifiedUserId', 'numerical', 'integerOnly'=>true),
			array('name', 'uniqueUserInEntity'),
			array('name, email', 'length', 'max'=>50),
			array('image', 'required'),
			array('image', 'length', 'max'=>250),
			array('image', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true),	
			array('phoneOne, phoneTwo', 'length', 'max'=>20),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cityId, name, description, email, phoneOne, phoneTwo, creationUserId, modifiedUserId, creationDate, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cityOfEntity' => array(self::BELONGS_TO, 'City', 'cityId'),
			'usersOfEntity' => array(self::HAS_MANY, 'User', 'entityId'),
			'eventsOfEntity' => array(self::HAS_MANY, 'Event', 'entityId'),
			'redundantDataOfEntity' => array(self::HAS_ONE, 'RedundantData', 'tableId',
				'on' => 'redundantDataOfEntity.table = :tableEntity',
				'params' => array(':tableEntity' => $this->tableName()),
			),				
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Identificador',
			'cityId' => 'Ciudad',
			'name' => 'Nombre de entidad',
			'description' => 'Descripción',
			'email' => 'Correo electrónico',
			'phoneOne' => 'Teléfono uno',
			'phoneTwo' => 'Teléfono dos',
			'status' => 'Estado',
			'order' => 'Orden',
			'image' => 'Imagen "jpg, gif, png"',
			'creationUser' => 'Usuario de creación',
			'creationUserId' => 'Usuario de creación',
			'modifiedUser' => 'Usuario de modificación',				
			'modifiedUserId' => 'Usuario de modificación',
			'creationDate' => 'Fecha de creación',
			'modifiedDate' => 'Fecha de modificación',			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$user = Yii::app ()->user->getState ( 'user' );
		$criteria=new CDbCriteria;
		$criteria->with = array('cityOfEntity' => array(
			'with' => array(
				'departmentOfCity' => array(
					'with' => array('countryOfDepartment')
				)	
			)
		));
		
		switch ($user->role) {
			case UserAbstract::ADMINISTRADOR_SUPER_USUARIO :
				break;			
			case UserAbstract::ADMINISTRADOR_PRESIDENTE :
				$criteria->compare('countryOfDepartment.id',$user->roleCountryId);
				break;
			case UserAbstract::ADMINISTRADOR_GOBERNADOR :
				$criteria->compare('departmentOfCity.id',$user->roleDepartmentId);
				break;
			case UserAbstract::SERVIDOR_PUBLICO_NUMERO_UNO :
				$criteria->compare('cityOfEntity.id',$user->roleCityId);
				break;
			case UserAbstract::SERVIDOR_PUBLICO_NUMERO_DOS :
				$criteria->compare('entityId',$user->entityId);
				break;
			case UserAbstract::SERVIDOR_PUBLICO_NUMERO_TRES :
				$criteria->compare('cityOfEntity.id',$user->roleCityId);
				break;
			default: // REDACTOR
				$criteria->compare('entityId',$user->entityId);				
		}
		
		$criteria->compare('`t`id',$this->id);
		$criteria->compare('cityId',$this->cityId);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phoneOne',$this->phoneOne,true);
		$criteria->compare('phoneTwo',$this->phoneTwo,true);
		$criteria->compare('status',$this->status,true);

		if($user->role != UserAbstract::ADMINISTRADOR_SUPER_USUARIO)
			$criteria->addNotInCondition('redundantDataOfEntity.status', array(RedundantDataAbstract::STATUS_ELIMINADO));

		if($this->creationDate)
		{
			$criteria->addCondition(new CDbExpression('DATE_FORMAT(`redundantDataOfEntity`.`creationDate`,"%Y-%m-%d")=:creationDate'));
			$timestamp=CDateTimeParser::parse($this->creationDate,Yii::app()->locale->dateFormat);
			$criteria->params[':creationDate']=date('Y-m-d',$timestamp);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'t.name ASC, `redundantDataOfEntity`.`creationDate` DESC',
				'attributes'=>array(
					'cityId' => array(
						'asc' => 'cityId ASC',
						'desc' => 'cityId DESC',
					),
					'name' => array(
						'asc' => 't.name ASC',
						'desc' => 't.name DESC',
					),
					'status' => array (
						'asc' => 'redundantDataOfEvent.status ASC',
						'desc' => 'redundantDataOfEvent.status DESC'
					),
					'email' => array(
						'asc' => 'email ASC',
						'desc' => 'email DESC',
					),
					'creationDate' => array(
						'asc' => '`redundantDataOfEntity`.`creationDate` ASC',
						'desc' => '`redundantDataOfEntity`.`creationDate` DESC',
					),
				),				
			)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Entity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}