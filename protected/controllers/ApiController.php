<?php
class ApiController extends NHCControllerPublicModule {
	const APPLICATION_ID = 'NHQ2016';
	const COUNTRY_ID = 1;
	private $request;
	private $user;
	private function getUser() {
		if ($this->request->getParam ( 'userId' )) {
			$criteriaUser = new CDbCriteria ();
			$criteriaUser->with = array ('redundantDataOfUser');
			$criteriaUser->addCondition ( '`t`.`id`=:userId' )->addCondition ( '`t`.`role`=:role' )->addCondition ( '`redundantDataOfUser`.`status`=:status' );
			$criteriaUser->params = array (':userId' => $this->request->getParam ( 'userId' ), ':role' => UserAbstract::USUARIO_USUARIO, ':status' => RedundantDataAbstract::STATUS_ACTIVO );
			$this->user = User::model ()->find ( $criteriaUser );
		}
	}
	private function getUserEvents($status = null) {
		if ($this->user) {
			$criteriaEventsQuickly = new CDbCriteria ();
			$criteriaEventsQuickly->with = array ('redundantDataOfEventQuickly');
			if ($status) {
				$criteriaEventsQuickly->addCondition ( '`redundantDataOfEventQuickly`.`status`=:status' );
				$criteriaEventsQuickly->params [':status'] = $status;
			}
			$criteriaEventsQuickly->addCondition ( '`userId`=:userId' );
			$criteriaEventsQuickly->params [':userId'] = $this->user->id;
		}
		return EventQuickly::model ()->findAll ( $criteriaEventsQuickly );
	}
	private function getEntitiesOfCountry($countryId) {
		$entities = $status = array ();
		$criteriaEntities = new CDbCriteria ();
		$modelEntities;
		$criteriaEntities->with = array (
				'cityOfEntity' => array (
						'with' => array (
								'redundantDataOfCity',
								'departmentOfCity' => array (
										'with' => array (
												'redundantDataOfDepartment',
												'countryOfDepartment' => array (
														'with' => array (
																'redundantDataOfCountry'
														)
												)
										)
								)
						)
				)
		);
		$criteriaEntities->addCondition ( '`countryOfDepartment`.`id`=:countryId' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' );
		$criteriaEntities->params = array (':countryId' => $countryId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaEntities->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `cityOfEntity`.`name`, `t`.name';
		if ($modelEntities = Entity::model ()->findAll ( $criteriaEntities ))
			foreach ( $modelEntities as $model )
				$entities [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'status'
				) ) );
		return $entities;
	}
	private function getEntitiesOfDepartment($departmentId) {
		$entities = $status = array ();
		$criteriaEntities = new CDbCriteria ();
		$modelEntities;
		$criteriaEntities->with = array (
				'cityOfEntity' => array (
						'with' => array (
								'redundantDataOfCity',
								'departmentOfCity' => array (
										'with' => array (
												'redundantDataOfDepartment',
												'countryOfDepartment' => array (
														'with' => array (
																'redundantDataOfCountry'
														)
												)
										)
								)
						)
				)
		);
		$criteriaEntities->addCondition ( '`cityOfEntity`.`departmentId`=:departmentId' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' );
		$criteriaEntities->params = array (':departmentId' => $departmentId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaEntities->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `cityOfEntity`.`name`, `t`.name';
		if ($modelEntities = Entity::model ()->findAll ( $criteriaEntities ))
			foreach ( $modelEntities as $model )
				$entities [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'status'
				) ) );
		return $entities;
	}
	private function getEntitiesOfCity($cityId) {
		$entities = $status = array ();
		$criteriaEntities = new CDbCriteria ();
		$modelEntities;
		$criteriaEntities->with = array (
				'cityOfEntity' => array (
						'with' => array (
								'redundantDataOfCity',
								'departmentOfCity' => array (
										'with' => array (
												'redundantDataOfDepartment',
												'countryOfDepartment' => array (
														'with' => array (
																'redundantDataOfCountry'
														)
												)
										)
								)
						)
				)
		);
		$criteriaEntities->addCondition ( '`t`.`cityId`=:cityId' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' );
		$criteriaEntities->params = array (':cityId' => $cityId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaEntities->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `cityOfEntity`.`name`, `t`.name';
		if ($modelEntities = Entity::model ()->findAll ( $criteriaEntities ))
			foreach ( $modelEntities as $model )
				$entities [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'status'
				) ) );
		return $entities;
	}	
	private function getEntities() {
		$entities = $status = array ();
		$criteriaEntities = new CDbCriteria ();
		$modelEntities;
		$criteriaEntities->with = array (
				'cityOfEntity' => array (
						'with' => array (
								'redundantDataOfCity',
								'departmentOfCity' => array (
										'with' => array (
												'redundantDataOfDepartment',
												'countryOfDepartment' => array (
														'with' => array (
																'redundantDataOfCountry'
														)
												)
										)
								)
						)
				)
		);
		$criteriaEntities->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' );
		$criteriaEntities->params [':status'] = RedundantDataAbstract::STATUS_ACTIVO;
		$criteriaEntities->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `cityOfEntity`.`name`, `t`.name';	
		if ($modelEntities = Entity::model ()->findAll ( $criteriaEntities ))
			foreach ( $modelEntities as $model )
				$entities [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'status'
				) ) );
		return $entities;
	}
	private function getEntity($entityId) {
		$entity = $status = array ();
		$criteriaEntity = new CDbCriteria ();
		$modelEntity;
		$criteriaEntity->with = array (
				'cityOfEntity' => array (
						'with' => array (
								'redundantDataOfCity',
								'departmentOfCity' => array (
										'with' => array (
												'redundantDataOfDepartment',
												'countryOfDepartment' => array (
														'with' => array (
																'redundantDataOfCountry'
														)
												)
										)
								)
						)
				)
		);
		$criteriaEntity->addCondition ( '`t`.`id`=:entityId' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' );
		$criteriaEntity->params = array (':entityId' => $entityId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaEntity->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `cityOfEntity`.`name`, `t`.name';
		if ($modelEntity = Entity::model ()->find ( $criteriaEntity ))
			$entity = CMap::mergeArray ( $modelEntity->attributes, $modelEntity->getAttributes ( array (
					'status'
			) ) );
		return $entity;
	}	
	private function getEventsOfCountry($countryId) {
		$events = $status = array ();
		$criteriaEvents = new CDbCriteria ();
		$modelEvents;
		$criteriaEvents->with = array (
				'entityEvent' => array (
						'with' => array (
								'redundantDataOfEntity',
								'cityOfEntity' => array (
										'with' => array (
												'redundantDataOfCity',
												'departmentOfCity' => array (
														'with' => array (
																'redundantDataOfDepartment',
																'countryOfDepartment' => array (
																		'with' => array (
																				'redundantDataOfCountry' 
																		) 
																) 
														) 
												) 
										) 
								) 
						) 
				) 
		);
		$criteriaEvents->addCondition ( '`departmentOfCity`.`countryId`=:countryId' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' );
		$criteriaEvents->params = array (':countryId' => $countryId,':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaEvents->addCondition ( new CDbExpression ( 'DATE_ADD(DATE_FORMAT(CONCAT(DATE_FORMAT(`eventDateEnd`, "%Y-%m-%d")," ",DATE_FORMAT(`eventTimeEnd`, "%H:%i")), "%Y-%m-%d %H:%i"), INTERVAL ' . EventAbstract::LIMIT_PUBLICATION . ' HOUR) > DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")' ) );
		$status = array (RedundantDataAbstract::STATUS_PENDIENTE, RedundantDataAbstract::STATUS_ACTIVO, RedundantDataAbstract::STATUS_CUMPLIDO, RedundantDataAbstract::STATUS_CANCELADO);
		$criteriaEvents->addInCondition ( '`redundantDataOfEvent`.`status`', $status )->addNotInCondition ( '`redundantDataOfEvent`.`status`', array (RedundantDataAbstract::STATUS_ELIMINADO) );
		$criteriaEvents->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `cityOfEntity`.`name`, `entityEvent`.`name`, `t`.`name`';
		if ($modelEvents = Event::model ()->findAll ( $criteriaEvents ))
			foreach ( $modelEvents as $model )
				$events [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'eventTimeStampStart',
						'eventTimeStampEnd',
						'eventTimeStampEndLimit',
						'status' 
				) ) );
		return $events;
	}
	private function getEventsOfDepartment($departmentId) {
		$events = $status = array ();
		$criteriaEvents = new CDbCriteria ();
		$modelEvents;
		$criteriaEvents->with = array (
				'entityEvent' => array (
						'with' => array (
								'redundantDataOfEntity',
								'cityOfEntity' => array (
										'with' => array (
												'redundantDataOfCity',
												'departmentOfCity' => array (
														'with' => array (
																'redundantDataOfDepartment',
																'countryOfDepartment' => array (
																		'with' => array (
																				'redundantDataOfCountry' 
																		) 
																) 
														) 
												) 
										) 
								) 
						) 
				) 
		);
		$criteriaEvents->addCondition ( '`cityOfEntity`.`departmentId`=:departmentId' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' );
		$criteriaEvents->params = array (':departmentId' => $departmentId,':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaEvents->addCondition ( new CDbExpression ( 'DATE_ADD(DATE_FORMAT(CONCAT(DATE_FORMAT(`eventDateEnd`, "%Y-%m-%d")," ",DATE_FORMAT(`eventTimeEnd`, "%H:%i")), "%Y-%m-%d %H:%i"), INTERVAL ' . EventAbstract::LIMIT_PUBLICATION . ' HOUR) > DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")' ) );
		$status = array (RedundantDataAbstract::STATUS_PENDIENTE, RedundantDataAbstract::STATUS_ACTIVO, RedundantDataAbstract::STATUS_CUMPLIDO, RedundantDataAbstract::STATUS_CANCELADO);
		$criteriaEvents->addInCondition ( '`redundantDataOfEvent`.`status`', $status )->addNotInCondition ( '`redundantDataOfEvent`.`status`', array (RedundantDataAbstract::STATUS_ELIMINADO) );
		$criteriaEvents->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `cityOfEntity`.`name`, `entityEvent`.`name`, `t`.`name`';
		if ($modelEvents = Event::model ()->findAll ( $criteriaEvents ))
			foreach ( $modelEvents as $model )
				$events [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'eventTimeStampStart',
						'eventTimeStampEnd',
						'eventTimeStampEndLimit',						
						'status' 
				) ) );
		return $events;
	}
	private function getEventsOfCity($cityId) {
		$events = $status = array ();
		$criteriaEvents = new CDbCriteria ();
		$modelEvents;
		$criteriaEvents->with = array (
				'entityEvent' => array (
						'with' => array (
								'redundantDataOfEntity',
								'cityOfEntity' => array (
										'with' => array (
												'redundantDataOfCity',
												'departmentOfCity' => array (
														'with' => array (
																'redundantDataOfDepartment',
																'countryOfDepartment' => array (
																		'with' => array (
																				'redundantDataOfCountry' 
																		) 
																) 
														) 
												) 
										) 
								) 
						) 
				) 
		);
		$criteriaEvents->addCondition ( '`entityEvent`.`cityId`=:cityId' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' );
		$criteriaEvents->params = array (':cityId' => $cityId,':status' => RedundantDataAbstract::STATUS_ACTIVO);

		$criteriaEvents->addCondition ( new CDbExpression ( 'DATE_ADD(DATE_FORMAT(CONCAT(DATE_FORMAT(`eventDateEnd`, "%Y-%m-%d")," ",DATE_FORMAT(`eventTimeEnd`, "%H:%i")), "%Y-%m-%d %H:%i"), INTERVAL ' . EventAbstract::LIMIT_PUBLICATION . ' HOUR) > DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")' ) );
		$status = array (RedundantDataAbstract::STATUS_PENDIENTE, RedundantDataAbstract::STATUS_ACTIVO, RedundantDataAbstract::STATUS_CUMPLIDO, RedundantDataAbstract::STATUS_CANCELADO );
		$criteriaEvents->addInCondition ( '`redundantDataOfEvent`.`status`', $status )->addNotInCondition ( '`redundantDataOfEvent`.`status`', array (RedundantDataAbstract::STATUS_ELIMINADO) );
		$criteriaEvents->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `cityOfEntity`.`name`, `entityEvent`.`name`, `t`.`name`';
		if ($modelEvents = Event::model ()->findAll ( $criteriaEvents ))
			foreach ( $modelEvents as $model )
				$events [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'eventTimeStampStart',
						'eventTimeStampEnd',
						'eventTimeStampEndLimit',						
						'status' 
				) ) );
		return $events;
	}
	private function getEventsOfEntity($entityId) {
		$events = $status = array ();
		$criteriaEvents = new CDbCriteria ();
		$modelEvents;
		$criteriaEvents->with = array (
				'entityEvent' => array (
						'with' => array (
								'redundantDataOfEntity',
								'cityOfEntity' => array (
										'with' => array (
												'redundantDataOfCity',
												'departmentOfCity' => array (
														'with' => array (
																'redundantDataOfDepartment',
																'countryOfDepartment' => array (
																		'with' => array (
																				'redundantDataOfCountry' 
																		) 
																) 
														) 
												) 
										) 
								) 
						) 
				) 
		);
		$criteriaEvents->addCondition ( '`t`.`entityId`=:entityId' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' );
		$criteriaEvents->params = array (
				':entityId' => $entityId,
				':status' => RedundantDataAbstract::STATUS_ACTIVO 
		);
		$criteriaEvents->addCondition ( new CDbExpression ( 'DATE_ADD(DATE_FORMAT(CONCAT(DATE_FORMAT(`eventDateEnd`, "%Y-%m-%d")," ",DATE_FORMAT(`eventTimeEnd`, "%H:%i")), "%Y-%m-%d %H:%i"), INTERVAL ' . EventAbstract::LIMIT_PUBLICATION . ' HOUR) > DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")' ) );
		$status = array (RedundantDataAbstract::STATUS_PENDIENTE, RedundantDataAbstract::STATUS_ACTIVO, RedundantDataAbstract::STATUS_CUMPLIDO, RedundantDataAbstract::STATUS_CANCELADO);
		$criteriaEvents->addInCondition ( '`redundantDataOfEvent`.`status`', $status )->addNotInCondition ( '`redundantDataOfEvent`.`status`', array (RedundantDataAbstract::STATUS_ELIMINADO) );
		$criteriaEvents->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `cityOfEntity`.`name`, `entityEvent`.`name`, `t`.`name`';
		if ($modelEvents = Event::model ()->findAll ( $criteriaEvents ))
			foreach ( $modelEvents as $model )
				$events [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'eventTimeStampStart',
						'eventTimeStampEnd',
						'eventTimeStampEndLimit',						
						'status' 
				) ) );
		return $events;
	}
	private function getEvents() {
		$events = $status = array ();
		$criteriaEvents = new CDbCriteria ();
		$modelEvents;
		$criteriaEvents->with = array (
				'entityEvent' => array (
						'with' => array (
								'redundantDataOfEntity',
								'cityOfEntity' => array (
										'with' => array (
												'redundantDataOfCity',
												'departmentOfCity' => array (
														'with' => array (
																'redundantDataOfDepartment',
																'countryOfDepartment' => array (
																		'with' => array (
																				'redundantDataOfCountry' 
																		) 
																) 
														) 
												) 
										) 
								) 
						) 
				) 
		);
		$criteriaEvents->addCondition ( '`redundantDataOfEntity`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' );
		$criteriaEvents->params [':status'] = RedundantDataAbstract::STATUS_ACTIVO;
		$criteriaEvents->addCondition ( new CDbExpression ( 'DATE_ADD(DATE_FORMAT(CONCAT(DATE_FORMAT(`eventDateEnd`, "%Y-%m-%d")," ",DATE_FORMAT(`eventTimeEnd`, "%H:%i")), "%Y-%m-%d %H:%i"), INTERVAL ' . EventAbstract::LIMIT_PUBLICATION . ' HOUR) > DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")' ) );
		$status = array (RedundantDataAbstract::STATUS_PENDIENTE, RedundantDataAbstract::STATUS_ACTIVO, RedundantDataAbstract::STATUS_CUMPLIDO, RedundantDataAbstract::STATUS_CANCELADO);
		$criteriaEvents->addInCondition ( '`redundantDataOfEvent`.`status`', $status )->addNotInCondition ( '`redundantDataOfEvent`.`status`', array (RedundantDataAbstract::STATUS_ELIMINADO) );
		$criteriaEvents->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `cityOfEntity`.`name`, `entityEvent`.`name`, `t`.`name`';
		if ($modelEvents = Event::model ()->findAll ( $criteriaEvents ))
			foreach ( $modelEvents as $model )
				$events [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'eventTimeStampStart',
						'eventTimeStampEnd',
						'eventTimeStampEndLimit',						
						'status' 
				) ) );
		return $events;
	}
	private function getEvent($eventId) {
		$event = $status = array ();
		$criteriaEvent = new CDbCriteria ();
		$modelEvent;
		$criteriaEvent->with = array (
				'entityEvent' => array (
						'with' => array (
								'redundantDataOfEntity',
								'cityOfEntity' => array (
										'with' => array (
												'redundantDataOfCity',
												'departmentOfCity' => array (
														'with' => array (
																'redundantDataOfDepartment',
																'countryOfDepartment' => array (
																		'with' => array (
																				'redundantDataOfCountry'
																		)
																)
														)
												)
										)
								)
						)
				)
		);
		$criteriaEvent->addCondition ( '`t`.`id`=:eventId' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' )->addCondition ( '`redundantDataOfEntity`.`status`=:status' );
		$criteriaEvent->params = array(
				':eventId' => $eventId,
				':status' => RedundantDataAbstract::STATUS_ACTIVO
		);
		$criteriaEvent->addCondition ( new CDbExpression ( 'DATE_ADD(DATE_FORMAT(CONCAT(DATE_FORMAT(`eventDateEnd`, "%Y-%m-%d")," ",DATE_FORMAT(`eventTimeEnd`, "%H:%i")), "%Y-%m-%d %H:%i"), INTERVAL ' . EventAbstract::LIMIT_PUBLICATION . ' HOUR) > DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i")' ) );
		$status = array (RedundantDataAbstract::STATUS_PENDIENTE, RedundantDataAbstract::STATUS_ACTIVO, RedundantDataAbstract::STATUS_CUMPLIDO, RedundantDataAbstract::STATUS_CANCELADO);
		$criteriaEvent->addInCondition ( '`redundantDataOfEvent`.`status`', $status )->addNotInCondition ( '`redundantDataOfEvent`.`status`', array (RedundantDataAbstract::STATUS_ELIMINADO) );
		$criteriaEvent->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `cityOfEntity`.`name`, `entityEvent`.`name`, `t`.`name`';
		if ($modelEvent = Event::model ()->find ( $criteriaEvent ))
			$event = CMap::mergeArray ( $modelEvent->attributes, $modelEvent->getAttributes ( array (
					'eventTimeStampStart',
					'eventTimeStampEnd',
					'eventTimeStampEndLimit',					
					'status'
			) ) );
		return $event;
	}
	private function getReleasesOfCountry($countryId) {
		$releases = $status = array ();
		$criteriaReleases = new CDbCriteria ();
		$modelReleases;
		$criteriaReleases->with = array (
			'redundantDataOfRelease',
			'userOfRelease' => array (
				'with' => array (
					'redundantDataOfUser',
					'roleCityOfUser' => array (
							'with' => array (
									'redundantDataOfCity',
									'departmentOfCity' => array (
											'with' => array (
													'redundantDataOfDepartment',
													'countryOfDepartment' => array (
															'with' => array (
																	'redundantDataOfCountry'
															)
													)
											)
									)
							)
					)
				)
			)
		);
		$criteriaReleases->addCondition ( '`departmentOfCity`.`countryId`=:countryId' )->addCondition ( '`redundantDataOfUser`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );		
		$criteriaReleases->params = array (':countryId' => $countryId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaReleases->addCondition ( new CDbExpression ( '(DATE_FORMAT(NOW(), "%Y-%m-%d") BETWEEN  `releaseDateStart` AND `releaseDateEnd`)' ) );
		$status = array (RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaReleases->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `t`.`name`';
		if ($modelReleases = Release::model ()->findAll ( $criteriaReleases ))
			foreach ( $modelReleases as $model )
				$releases [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'releaseTimeStampStart',
						'releaseTimeStampEnd',
						'cityId',
						'userName',
						'userImage',
						'userRole',
						'userDepartmentName',
						'userCityName',
						'status'
				) ) );
		return $releases;
	}
	private function getReleasesOfDepartment($departmentId) {
		$releases = $status = array ();
		$criteriaReleases = new CDbCriteria ();
		$modelReleases;
		$criteriaReleases->with = array (
				'redundantDataOfRelease',
				'userOfRelease' => array (
						'with' => array (
								'redundantDataOfUser',
								'roleCityOfUser' => array (
										'with' => array (
												'redundantDataOfCity',
												'departmentOfCity' => array (
														'with' => array (
																'redundantDataOfDepartment',
																'countryOfDepartment' => array (
																		'with' => array (
																				'redundantDataOfCountry'
																		)
																)
														)
												)
										)
								)
						)
				)
		);
		$criteriaReleases->addCondition ( '`roleCityOfUser`.`departmentId`=:departmentId' )->addCondition ( '`redundantDataOfUser`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );	
		$criteriaReleases->params = array (':departmentId' => $departmentId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaReleases->addCondition ( new CDbExpression ( '(DATE_FORMAT(NOW(), "%Y-%m-%d") BETWEEN  `releaseDateStart` AND `releaseDateEnd`)' ) );
		$status = array (RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaReleases->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `t`.`name`';
		if ($modelReleases = Release::model ()->findAll ( $criteriaReleases ))
			foreach ( $modelReleases as $model )
				$releases [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'releaseTimeStampStart',
						'releaseTimeStampEnd',
						'cityId',
						'userName',
						'userImage',
						'userRole',
						'userDepartmentName',
						'userCityName',
						'status'
				) ) );
		return $releases;
	}
	private function getReleasesOfCity($cityId) {
		$releases = $status = array ();
		$criteriaReleases = new CDbCriteria ();
		$modelReleases;
		$criteriaReleases->with = array (
				'redundantDataOfRelease',
				'userOfRelease' => array (
						'with' => array (
								'redundantDataOfUser',
								'roleCityOfUser' => array (
										'with' => array (
												'redundantDataOfCity',
												'departmentOfCity' => array (
														'with' => array (
																'redundantDataOfDepartment',
																'countryOfDepartment' => array (
																		'with' => array (
																				'redundantDataOfCountry'
																		)
																)
														)
												)
										)
								)
						)
				)
		);
		$criteriaReleases->addCondition ( '`userOfRelease`.`roleCityId`=:cityId' )->addCondition ( '`redundantDataOfUser`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );
		$criteriaReleases->params = array (':cityId' => $cityId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaReleases->addCondition ( new CDbExpression ( '(DATE_FORMAT(NOW(), "%Y-%m-%d") BETWEEN  `releaseDateStart` AND `releaseDateEnd`)' ) );
		$status = array (RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaReleases->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `t`.`name`';
		if ($modelReleases = Release::model ()->findAll ( $criteriaReleases ))
			foreach ( $modelReleases as $model )
				$releases [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'releaseTimeStampStart',
						'releaseTimeStampEnd',
						'cityId',
						'userName',
						'userImage',
						'userRole',
						'userDepartmentName',
						'userCityName',
						'status'
				) ) );
		return $releases;
	}
	private function getReleasesOfEntity($entityId) {
		$releases = $status = array ();
		$criteriaReleases = new CDbCriteria ();
		$modelReleases;
		$criteriaReleases->with = array (
				'redundantDataOfRelease',
				'userOfRelease' => array (
						'with' => array (
								'redundantDataOfUser',
								'roleCityOfUser' => array (
										'with' => array (
												'redundantDataOfCity',
												'departmentOfCity' => array (
														'with' => array (
																'redundantDataOfDepartment',
																'countryOfDepartment' => array (
																		'with' => array (
																				'redundantDataOfCountry'
																		)
																)
														)
												)
										)
								)
						)
				)
		);
		$criteriaReleases->addCondition ( '`userOfRelease`.`entityId`=:entityId' )->addCondition ( '`redundantDataOfUser`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );
		$criteriaReleases->params = array (':entityId' => $entityId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaReleases->addCondition ( new CDbExpression ( '(DATE_FORMAT(NOW(), "%Y-%m-%d") BETWEEN  `releaseDateStart` AND `releaseDateEnd`)' ) );
		$status = array (RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaReleases->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `t`.`name`';
		if ($modelReleases = Release::model ()->findAll ( $criteriaReleases ))
			foreach ( $modelReleases as $model )
				$releases [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'releaseTimeStampStart',
						'releaseTimeStampEnd',
						'cityId',
						'userName',
						'userImage',
						'userRole',
						'userDepartmentName',
						'userCityName',
						'status'
				) ) );
				return $releases;
	}	
	private function getReleases() {
		$releases = $status = array ();
		$criteriaReleases = new CDbCriteria ();
		$modelReleases;
		$criteriaReleases->with = array (
				'redundantDataOfRelease',
				'userOfRelease' => array (
						'with' => array (
								'redundantDataOfUser',
								'roleCityOfUser' => array (
										'with' => array (
												'redundantDataOfCity',
												'departmentOfCity' => array (
														'with' => array (
																'redundantDataOfDepartment',
																'countryOfDepartment' => array (
																		'with' => array (
																				'redundantDataOfCountry'
																		)
																)
														)
												)
										)
								)
						)
				)
		);
		$criteriaReleases->addCondition ( '`redundantDataOfUser`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );	
		$criteriaReleases->params = array (':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaReleases->addCondition ( new CDbExpression ( '(DATE_FORMAT(NOW(), "%Y-%m-%d") BETWEEN  `releaseDateStart` AND `releaseDateEnd`)' ) );
		$status = array (RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaReleases->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `t`.`name`';
		if ($modelReleases = Release::model ()->findAll ( $criteriaReleases ))
			foreach ( $modelReleases as $model )
				$releases [] = CMap::mergeArray ( $model->attributes, $model->getAttributes ( array (
						'releaseTimeStampStart',
						'releaseTimeStampEnd',
						'cityId',
						'userName',
						'userImage',
						'userRole',
						'userDepartmentName',
						'userCityName',
						'status'
				) ) );
		return $releases;
	}
	private function getRelease($releaseId) {
		$release = $status = array ();
		$criteriaRelease = new CDbCriteria ();
		$modelRelease;
		$criteriaRelease->with = array (
				'redundantDataOfRelease',
				'userOfRelease' => array (
						'with' => array (
								'redundantDataOfUser',
								'roleCityOfUser' => array (
										'with' => array (
												'redundantDataOfCity',
												'departmentOfCity' => array (
														'with' => array (
																'redundantDataOfDepartment',
																'countryOfDepartment' => array (
																		'with' => array (
																				'redundantDataOfCountry'
																		)
																)
														)
												)
										)
								)
						)
				)
		);
		$criteriaRelease->addCondition ( '`t`.`id`=:releaseId' )->addCondition ( '`redundantDataOfUser`.`status`=:status' )->addCondition ( '`redundantDataOfUser`.`status`=:status' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );	
		$criteriaRelease->params = array (':releaseId' => $releaseId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaRelease->addCondition ( new CDbExpression ( '(DATE_FORMAT(NOW(), "%Y-%m-%d") BETWEEN  `releaseDateStart` AND `releaseDateEnd`)' ) );
		$status = array (RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaRelease->order = '`countryOfDepartment`.`name`, `departmentOfCity`.`name`, `t`.`name`';
		if ($modelRelease = Release::model ()->find ( $criteriaRelease ))
			$release = CMap::mergeArray ( $modelRelease->attributes, $modelRelease->getAttributes ( array (
					'eventTimeStampStart',
					'eventTimeStampEnd',
					'eventTimeStampEndLimit',
					'status'
			) ) );
		return $release;
	}
	private function getCountries() {
		$criteriaCountries = new CDbCriteria ();
		$criteriaCountries->addCondition ( '`redundantDataOfCountry`.`status`=:status' );
		$criteriaCountries->params [':status'] = RedundantDataAbstract::STATUS_ACTIVO;
		$criteriaCountries->order = '`t`.name';
		return Country::model ()->findAll ( $criteriaCountries );
	}
	private function getCountry($countryId) {
		$criteriaCountry = new CDbCriteria ();
		$criteriaCountry->addCondition ( '`t`.`id`=:countryId' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );
		$criteriaCountry->params = array (
				':countryId' => $countryId,
				':status' => RedundantDataAbstract::STATUS_ACTIVO 
		);
		return Country::model ()->find ( $criteriaCountry );
	}
	private function getDepartmentsOfCountry($countryId) {
		$criteriaDepartments = new CDbCriteria ();
		$criteriaDepartments->with = array ('countryOfDepartment');
		$criteriaDepartments->addCondition ( '`t`.`countryId`=:countryId' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );
		$criteriaDepartments->params = array (':countryId' => $countryId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaDepartments->order = '`countryOfDepartment`.`name`, `t`.name';
		return Department::model ()->findAll ( $criteriaDepartments );
	}
	private function getDepartments() {
		$criteriaDepartments = new CDbCriteria ();
		$criteriaDepartments->with = array ('countryOfDepartment');
		$criteriaDepartments->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );
		$criteriaDepartments->params [':status'] = RedundantDataAbstract::STATUS_ACTIVO;
		$criteriaDepartments->order = '`countryOfDepartment`.`name`, `t`.name';
		return Department::model ()->findAll ( $criteriaDepartments );
	}
	private function getDepartment($departmentId) {
		$criteriaDepartment = new CDbCriteria ();
		$criteriaDepartment->with = array ('countryOfDepartment');
		$criteriaDepartment->addCondition ( '`t`.`id`=:departmentId' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );
		$criteriaDepartment->params = array (':departmentId' => $departmentId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		return Department::model ()->find ( $criteriaDepartment );
	}
	private function getCitiesOfCountry($countryId) {
		$criteriaCities = new CDbCriteria ();
		$criteriaCities->with = array (
				'departmentOfCity' => array (
						'with' => array (
								'redundantDataOfDepartment',
								'countryOfDepartment' 
						) 
				) 
		);		
		$criteriaCities->addCondition ( '`departmentOfCity`.`countryId`=:countryId' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );
		$criteriaCities->params = array (':countryId' => $countryId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaCities->order = '`t`.`name`';
		return City::model ()->findAll ( $criteriaCities );
	}
	private function getCitiesOfDepartment($departmentId) {
		$criteriaCities = new CDbCriteria ();
		$criteriaCities->with = array (
				'departmentOfCity' => array (
						'with' => array (
								'redundantDataOfDepartment',
								'countryOfDepartment' 
						) 
				) 
		);
		$criteriaCities->addCondition ( '`departmentId`=:departmentId' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );
		$criteriaCities->params = array (':departmentId' => $departmentId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		$criteriaCities->order = '`t`.`name`';		
		return City::model ()->findAll ( $criteriaCities );
	}
	private function getCities() {
		$criteriaCities = new CDbCriteria ();
		$criteriaCities->with = array (
				'departmentOfCity' => array (
						'with' => array (
								'redundantDataOfDepartment',
								'countryOfDepartment' 
						) 
				) 
		);
		$criteriaCities->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );
		$criteriaCities->params [':status'] = RedundantDataAbstract::STATUS_ACTIVO;
		$criteriaCities->order = '`t`.`name`';
		return City::model ()->findAll ( $criteriaCities );
	}
	private function getCity($cityId) {
		$criteriaCity = new CDbCriteria ();
		$criteriaCity->with = array (
				'departmentOfCity' => array (
						'with' => array (
								'redundantDataOfDepartment',
								'countryOfDepartment' 
						) 
				) 
		);
		$criteriaCity->addCondition ( '`t`.`id`=:cityId' )->addCondition ( '`redundantDataOfCity`.`status`=:status' )->addCondition ( '`redundantDataOfDepartment`.`status`=:status' )->addCondition ( '`redundantDataOfCountry`.`status`=:status' );
		$criteriaCity->params = array (':cityId' => $cityId, ':status' => RedundantDataAbstract::STATUS_ACTIVO);
		return City::model ()->find ( $criteriaCity );
	}
	private function quicklyOrUnquickly($eventId, $status) {
		$error = true;
		if ($this->user && ($event = Event::model ()->findByPk ( $eventId ))) {
			$criteria = new CDbCriteria ();
			$criteria->addCondition ( 'userId=:userId' )->addCondition ( 'eventId=:eventId' );
			$criteria->params = array (':userId' => $this->user->id, ':eventId' => $event->id);
			if (! ($eventQuicklyOrUnquickly = EventQuickly::model ()->find ( $criteria ))) {
				$eventQuicklyOrUnquickly = new EventQuickly ();
				$eventQuicklyOrUnquickly->userId = $this->user->id;
				$eventQuicklyOrUnquickly->eventId = $event->id;
			}
			$eventQuicklyOrUnquickly->status = $status;
			if ($eventQuicklyOrUnquickly->save ())
				$error = false;
		}
		return $error ? null : $this->getUserEvents ( RedundantDataAbstract::STATUS_AGENDADA );
	}
	private function sendResponse($data) {
		header ( 'HTTP/1.1 200 OK' );
		header ( 'Content-type: application/json' );
		echo $data;
		Yii::app ()->end ();
	}
	public function filters() {
		return array ('validateMethod');
	}
	public function init() {
		parent::init ();
		$this->request = Yii::app ()->request;
		$this->getUser ();
	}
	public function filterValidateMethod($filter) {
		$pathInfo = $this->request->pathInfo;
		$rules = Yii::app ()->urlManager->rules;
		foreach ( $rules as $key => $value )
			if (is_array ( $value ) && in_array ( $pathInfo, $value ))
				break;
		if (isset ( $rules [$key] ))
			isset ( $rules [$key] ['verb'] ) && (strtoupper ( $rules [$key] ['verb'] ) != $this->request->getRequestType ()) ? Yii::app ()->end (404) : false;
		$filter->run ();
	}
	public function actionCountries() {
		$data = array ();
		if ($this->request->getQuery ( 'countryId' ))
			$data = $this->getCountry ( $this->request->getParam ( 'countryId' ) );
		else
			$data = $this->getCountries ();
		$this->sendResponse ( CJSON::encode ( array (
				Country::model ()->tableName () => $data 
		) ) );
	}
	public function actionDepartments() {
		$data = array ();
		if ($this->request->getParam ( 'countryId' ))
			$data = $this->getDepartmentsOfCountry ( $this->request->getParam ( 'countryId' ) );
		else if ($this->request->getParam ( 'departmentId' ))
			$data = $this->getDepartment ( $this->request->getParam ( 'departmentId' ) );
		else
			$data = $this->getDepartments ();
		$this->sendResponse ( CJSON::encode ( array (
				Department::model ()->tableName () => $data 
		) ) );
	}
	public function actionCities() {
		$data = array ();
		if ($this->request->getParam ( 'countryId' ))
			$data = $this->getCitiesOfCountry ( $this->request->getParam ( 'countryId' ) );
		else if ($this->request->getParam ( 'departmentId' ))
			$data = $this->getCitiesOfDepartment ( $this->request->getParam ( 'departmentId' ) );
		else if ($this->request->getParam ( 'cityId' ))
			$data = $this->getCity ( $this->request->getParam ( 'cityId' ) );
		else
			$data = $this->getCities ();
		$this->sendResponse ( CJSON::encode ( array (
				City::model ()->tableName () => $data 
		) ) );
	}
	public function actionEntities() {
		$data = array ();
		if ($this->request->getParam ( 'countryId' ))
			$data = $this->getEntitiesOfCountry ( $this->request->getParam ( 'countryId' ) );
		else if ($this->request->getParam ( 'departmentId' ))
			$data = $this->getEntitiesOfDepartment ( $this->request->getParam ( 'departmentId' ) );
		else if ($this->request->getParam ( 'cityId' ))
			$data = $this->getEntitiesOfCity ( $this->request->getParam ( 'cityId' ) );
		else if ($this->request->getParam ( 'entityId' ))
			$data = $this->getEntity ( $this->request->getParam ( 'entityId' ) );
		else
			$data = $this->getEntities ();
		$this->sendResponse ( CJSON::encode ( array(Entity::model ()->tableName () => $data) ) );
	}	
	public function actionEvents() {
		$data = array ();
		if ($this->request->getQuery ( 'countryId' ))
			$data = $this->getEventsOfCountry ( $this->request->getParam ( 'countryId' ) );
		else if ($this->request->getQuery ( 'departmentId' ))
			$data = $this->getEventsOfDepartment ( $this->request->getParam ( 'departmentId' ) );
		else if ($this->request->getQuery ( 'cityId' ))
			$data = $this->getEventsOfCity ( $this->request->getParam ( 'cityId' ) );
		else if ($this->request->getQuery ( 'entityId' ))
			$data = $this->getEventsOfEntity ( $this->request->getParam ( 'entityId' ) );
		else if ($this->request->getQuery ( 'eventId' ))
			$data = $this->getEvent ( $this->request->getParam ( 'eventId' ) );		
		else
			$data = $this->getEvents ();
		$this->sendResponse ( CJSON::encode ( array(Event::model ()->tableName () => $data ) ));
	}
	public function actionReleases() {
		$data = array ();
		if ($this->request->getParam ( 'countryId' ))
			$data = $this->getReleasesOfCountry ( $this->request->getParam ( 'countryId' ) );
		else if ($this->request->getParam ( 'departmentId' ))
			$data = $this->getReleasesOfDepartment ( $this->request->getParam ( 'departmentId' ) );
		else if ($this->request->getParam ( 'cityId' ))
			$data = $this->getReleasesOfCity ( $this->request->getParam ( 'cityId' ) );
		else if ($this->request->getParam ( 'entityId' ))
			$data = $this->getReleasesOfEntity ( $this->request->getParam ( 'entityId' ) );		
		else if ($this->request->getQuery ( 'releaseId' ))
			$data = $this->getRelease ( $this->request->getParam ( 'releaseId' ) );
		else
			$data = $this->getReleases ();
		$this->sendResponse ( CJSON::encode ( array(Release::model ()->tableName () => $data) ) );
	}	
	public function actionUserregister() {
		$userData = $errorsAux =  array ();
		$_POST = array_merge($_POST, $_GET);
		if ($city = $this->getCity ( $this->request->getPost ( 'cityId' ) )) {	
			$criteria = new CDbCriteria ();
			$criteria->addCondition ( '`document`=:document' )->addCondition ( '`role`=:role' );
			$criteria->params = array (
					':document' => $this->request->getPost ( 'document' ),
					':role' => User::USUARIO_USUARIO,
			);
			$this->user = UserApp::model ()->find ( $criteria );
			if (! $this->user) {
				$this->user = new UserApp ();
				$this->user->role = UserAbstract::USUARIO_USUARIO;
				$this->user->status = RedundantDataAbstract::STATUS_ACTIVO;
			} else {
				$criteriaEmail = new CDbCriteria ();
				$criteriaEmail->addCondition ( '`email`=:email' )->addCondition ( '`role`=:role' );
				$criteriaEmail->params = array (
						':email|' => $this->request->getPost ( 'email' ),
						':role' => $this->request->getPost ( 'role' )
				);
				if($userForEmail = UserApp::model ()->find ( $criteria )) {
					if($this->user->id ==  $userForEmail->id)
						$this->user->email = $this->request->getPost ( 'email' );
				}
			}
			$_POST ['roleCountryId'] = $city->departmentOfCity->countryId;
			$_POST ['roleDepartmentId'] = $city->departmentId;
			$_POST ['roleCityId'] = $city->id;
			foreach ( $_POST as $key => $value )
				if ($this->user->hasAttribute ( $key ))
					$this->user->$key = $value;
			$isNew = $this->user->isNewRecord;		
			if ($this->user->save ()) {
				$error = false;
				$userParams = $this->user->getAttributes ( array (
						'status' 
				) );
				$userParams ['isNew'] = $isNew;
				$userData = CMap::mergeArray ( $this->user->attributes, $userParams );
			} else {
				if($errors = $this->user->getErrors()){
					foreach($errors as $attribute => $messages)
						foreach($messages as $message)
							$errorsAux[] = new Error($attribute, $message);
					$this->sendResponse ( CJSON::encode ( array (
							Error::model ()->tableName () => $errorsAux
					) ) );
				}
			}
		}
		$this->sendResponse ( CJSON::encode ( array (
				Error::model ()->tableName () => $errorsAux,
				User::model ()->tableName () => array($userData)
		) ) );
	}
	public function actionEventsofuser() {
		$this->sendResponse ( CJSON::encode ( array (
				EventQuickly::model ()->tableName () => $this->getUserEvents ( $this->request->getParam ( 'status' ) ) 
		) ) );
	}
	public function actionQuickly() {
		$this->sendResponse ( CJSON::encode ( array (
				EventQuickly::model ()->tableName () => $this->quicklyOrUnquickly ( $this->request->getParam ( 'eventId' ), RedundantDataAbstract::STATUS_AGENDADA ) 
		) ) );
	}
	public function actionUnquickly() {
		$this->sendResponse ( CJSON::encode ( array (
				EventQuickly::model ()->tableName () => $this->quicklyOrUnquickly ( $this->request->getParam( 'eventId' ), RedundantDataAbstract::STATUS_DESAGENDADA ) 
		) ) );
	}
}