<?php
return array (
		self::ADMINISTRADOR_SUPER_USUARIO => array (
				'url' => array (
						'default' => 'default/dashboard' 
				),
				'default' => array (
						'dashboard',
						'resetPassword'
				),
				'entity' => array (	//	Crea entidades
						'admin',
						'create',
						'update',
						'delete',
						'view' 
				),
				'user' => array (	// Crea presidente, gobernadores o alcaldes
						'admin',
						'create',
						'update',
						'delete',
						'view',
						'reset'
				),
				'event' => array (
						'admin',
						'update',
						'delete',
						'view' 
				),
				'release' => array (
						'admin',
						'update',
						'delete',
						'view'
				),
				'configuration' => array (
						'createCountry',
						'updateCountry',
						'deleteCountry',
						'adminCountry',
						'createDepartment',
						'updateDepartment',
						'deleteDepartment',
						'adminDepartment',
						'createCity',
						'updateCity',
						'deleteCity',
						'adminCity',
						'createEventType',
						'updateEventType',
						'deleteEventType' ,
						'adminEventType'
				) 
		)
		,
		self::ADMINISTRADOR_PRESIDENTE => array (
				'url' => array (
						'default' => 'default/dashboard' 
				),
				'default' => array (
						'dashboard',
						'resetPassword'
				),
				'entity' => array (
						'admin',
						'view' 
				),
				'user' => array ( // Crea gobernadores
						'admin',
						'view' 
				),
				'event' => array (
						'admin',
						'view' 
				),
				'release' => array (
						'admin',
						'create',
						'update',
						'delete',
						'view'
				)		
		),
		self::ADMINISTRADOR_GOBERNADOR => array (
				'url' => array (
						'default' => 'default/dashboard' 
				),
				'default' => array (
						'dashboard',
						'resetPassword'
				),
				'entity' => array (
						'admin',
						'view' 
				),
				'user' => array (
						'admin',
						'view' 
				),
				'event' => array (
						'admin',
						'view' 
				),
				'release' => array (
						'admin',
						'create',
						'update',
						'delete',
						'view'
				)		
		),
		self::SERVIDOR_PUBLICO_NUMERO_UNO => array ( // Alcalde
				'url' => array (
						'default' => 'default/dashboard' 
				),
				'default' => array (
						'dashboard',
						'resetPassword'
				),
				'entity' => array ( // Crea Entidades=Secretarías
						'admin',
						'create',
						'delete',
						'update',
						'view' 
				),
				'user' => array ( // Crea Secretario de despacho y Secretaria
						'admin',
						'create',
						'update',
						'delete',
						'view',
						'reset'
				),
				'event' => array (
						'admin',
						'view'
				),
				'release' => array (
						'admin',
						'create',
						'update',
						'delete',
						'view'
				)				
		),
		self::SERVIDOR_PUBLICO_NUMERO_DOS => array ( // Secretario de despacho
				'url' => array (
						'default' => 'event/admin' 
				),
				'default' => array (
						'resetPassword'
				),
				'event' => array (
						'admin',
						'view',
						'activate',
				) 
		),
		self::SERVIDOR_PUBLICO_NUMERO_TRES => array ( // Secretaria
				'url' => array (
						'default' => 'user/admin' 
				),
				'default' => array (
						'resetPassword'
				),				
				'entity' => array ( // Crea Entidades=Secretarías
						'admin',
						'create',
						'update',
						'delete',
						'view'
				),				
				'user' => array ( // Crea Secretario de despacho y Redactor
						'admin',
						'create',
						'update',
						'delete',
						'view',
						'reset'
				),
				'configuration' => array (
						'createEventType',
						'updateEventType',
						'deleteEventType' ,
						'adminEventType'
				)				
		),
		self::USUARIO_FINAl => array ( // Redactor
				'url' => array (
						'default' => 'event/admin' 
				),
				'default' => array (
						'resetPassword'
				),				
				'event' => array (
						'admin',
						'create',
						'update',
						'delete',
						'cancel',
						'inactive',
						'view' 
				) 
		),
		self::USUARIO_USUARIO => array () 
);