<?php
$basePath = dirname(__FILE__).DIRECTORY_SEPARATOR.'..';

// This is the main Web application configuration.
// CWebApplication properties can be configured here.
return array(
	'basePath'=>$basePath,
	'name'=>'Quickly',
	'language'=>'es',
	'theme'=>'chia',

	// preloading 'log' component
	'preload'=>array('log'),

	'aliases' => array(
		'nh'=>dirname(dirname(dirname($basePath))).DIRECTORY_SEPARATOR.'nh-1.0.0'.DIRECTORY_SEPARATOR.'framework',			
		'bootstrap'=>'ext.yii-bootstrap-3-module',
	),		

	// autoloading model and component classes
	'import'=>array(
		'application.models.abstract.*',
		'application.models.behaviors.*',
		'application.models.*',
		'application.components.*',
		'nh.db.ar.*',
		'nh.web.*',
		'nh.web.auth.*',
		'bootstrap.behaviors.*',
		'bootstrap.helpers.*',
		'bootstrap.widgets.*',			
	),

	'modules'=>array(
		'admin',
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'generatorPaths' => array('bootstrap.gii'),
			'password'=>'nh2015',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
		'assetManager' => array(
			//'linkAssets' => true
		),

		'session' => array(
			'class'=>'nh.web.NHHttpSession',
		),
			
		'bootstrap' => array(
			'class' => 'bootstrap.components.BsApi'
		),

		'clientScript'=>array(
			'coreScriptPosition'=>CClientScript::POS_END
		),

		'mobileDetect' => array(
			'class'=>'ext.MobileDetect.MobileDetect.MobileDetect'
		),

		'securityManager'=>array(
			'encryptionKey'=>'N1Un%y]N.l=c1AH5',
		),

		'authManager'=>array(
			'class'=>'nh.web.auth.NHCDbAuthManager',
			'connectionID'=>'db',
		),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl'=>array('admin/default/login')
		),

		// enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				array('api/countries', 'verb'=>'GET'),
				array('api/departments', 'verb'=>'GET'),
				array('api/cities', 'verb'=>'GET'),					
				array('api/entities', 'verb'=>'GET'),
				array('api/events', 'verb'=>'GET'),
				array('api/releases', 'verb'=>'GET'),
				array('api/userregister', 'verb'=>'POST'),
				array('api/eventsofuser', 'verb'=>'GET'),
				array('api/quickly', 'verb'=>'POST'),
				array('api/unquickly', 'verb'=>'POST'),
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				
				array(
					'class'=>'CWebLogRoute',
				),
				
			),
		),
	),

	// application-level parameters that can be accessed using Yii::app()->params['paramName']
	'params'=>array(
		'applicationLogoWhiteSmall'=>"logo_white_small.png",
		'applicationLogoWhiteMedium'=>"logo_white_medium.png",
		'applicationLogoOrangeSmall'=>"logo_orange_small.png",
		'applicationLogoOrangeMedium'=>"logo_orange_medium.png",
		'applicationName'=>"Quickly",
		'autor'=>"NetHa Solucuiones IT",
		'description'=>"NetHa Solucuiones IT",
	),
);