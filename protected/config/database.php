<?php
// This is the database connection configuration.
return array(
	'connectionString' => 'mysql:host=localhost;dbname=nethaco1_quickly',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => 'root',
	'charset' => 'utf8',
);
