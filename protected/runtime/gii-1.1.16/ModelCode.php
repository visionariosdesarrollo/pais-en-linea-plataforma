<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'tbl_',
  'modelPath' => 'application.models',
  'baseClass' => 'DepartmentAbstract',
  'buildRelations' => '1',
  'commentsAsLabels' => '1',
);
